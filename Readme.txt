Project by: Joshua Lipstone

A few important notes:
1) This project consists of two parts.  The first (and more important part) is a library, the second part is a GUI based on that library.
2) There are a large number of classes in this project.  Most of them are short / not worth bothering with.  Use list of important classes below this list.
3) There are two GUI's in this project.  The one that I wrote for this assignment is in the newGUI package with ParserGUI as its main class.
4) I have updated 3 plugins to work with this library.  Their .jar files are in the repository.
5) Much of this rewrite is still in progress, especially for the more intricate features, so there are a decent number of hooks in the code for stuff that does not yet exist.
6) There are four classes that are /not/ in use: CAS1, Parser1, ConsCell, and ConsType.  They are old code that is being kept for logic reference.

The important classes:
	lipstone.joshua.parser:
		ParserCore
		Parser
		ParserPluginHandler
		.tokenizer:
			Tokenizer
			Token
		.util:
			History
			Equation
		ParserPlugin
	testHarnesses.newGUI:
		ParserGUI
		EquationListCell
		IOPane

A few structural notes:
	1) The GUI is on the small side for this project.  The model is the equation history, which is stored in the History class.  The History class extends ModifiableObservableListBase from JavaFx, so all of the model's update code is handled by JavaFx.  The Controller consists of the event listeners in IOPane and EquationListCell.  The View consists of ParserGUI (which sets up the layout and loads the CSS), IOPane (which displays the current equation / answer data), and EquationListCell, which, in conjunction with a ListView (declared in ParserGUI) displays the input history.
	2) This project uses 5 of my other libraries in addition to the apfloat library ([http://www.apfloat.org/](http://www.apfloat.org/)), which is used for handling of imaginary numbers.

Features that were successfully implemented:
	1) The first view for the GUI (aka a basic calculator interface)
	2) The basic parsing / evaluating parts of the library
	3) The basic plugin-related parts of the library (loading of preprocessing filters, postprocessing filters, operations, commands, keywords, and property change listeners)

Features that were not successfully implemented:
	1) The second view for the GUI (aka the help dialog)