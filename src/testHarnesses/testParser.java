package testHarnesses;

import java.util.logging.Level;
import java.util.logging.Logger;

import lipstone.joshua.parser.ParserCore;

public class testParser {
	
	public static void main(String[] args) {
		ParserCore core = new ParserCore();
		Logger.getLogger("toberumono.parser").setLevel(Level.WARNING);
		System.out.println(core.parse("x^2+2x=-1").resultString());
		/*final JFrame frame = new JFrame("Equation Parser");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(new ParserGUI(core));
		
		frame.pack();
		frame.setVisible(true);*/
	}
}
