package testHarnesses.originalGUI;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;

import lipstone.joshua.parser.ParserCore;

@SuppressWarnings({"serial"})
public class ParserGUI extends JPanel {
	private KeyListener keysTyped;
	private final Core core;
	
	public ParserGUI(ParserCore parser) {
		core = new Core(parser);
		this.setFocusable(true);
		this.requestFocus();
		super.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		c.gridheight = 1;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(0, 0, 0, 0);
		super.add(core.getDisplay(), c);
		c.gridwidth = 1;
		c.gridheight = 1;
		c.gridx = 2;
		super.add(core.getAngleBar(), c);
		c.gridy = 1;
		c.gridx = 0;
		c.gridwidth = 2;
		c.gridheight = 2;
		super.add(core.getLog(), c);
		c.gridwidth = 1;
		c.gridy = 1;
		c.gridx = 2;
		c.gridheight = 2;
		super.add(core.getButtons(), c);
		setupActionListeners();
		super.addKeyListener(keysTyped);
	}
	
	private void setupActionListeners() {
		keysTyped = new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				core.keyPressed(e);
			}
			
			@Override
			public void keyTyped(KeyEvent e) {}
			
			@Override
			public void keyReleased(KeyEvent e) {}
		};
	}
}