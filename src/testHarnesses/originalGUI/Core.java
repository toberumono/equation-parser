package testHarnesses.originalGUI;

import java.awt.event.KeyEvent;

import testHarnesses.originalGUI.mainWindow.AngleBar;
import testHarnesses.originalGUI.mainWindow.CalculatorMainButtons;
import testHarnesses.originalGUI.mainWindow.EquationLog;
import testHarnesses.originalGUI.mainWindow.UpperDisplay;

import lipstone.joshua.parser.ParserCore;

public class Core {
	private final UpperDisplay display;
	private final EquationLog log;
	private final CalculatorMainButtons buttons;
	private final ParserCore core;
	private final AngleBar angleBar;
	private String input;
	private boolean isFirstChar = true;
	public int historyLocation;
	
	public Core(ParserCore core) {
		super();
		this.core = core;
		this.display = new UpperDisplay(this);
		this.log = new EquationLog(this);
		this.buttons = new CalculatorMainButtons(this);
		this.angleBar = new AngleBar(this);
		historyLocation = core.getHistory().size();
		input = "";
	}
	
	public boolean isValidChar(Character character) {
		return "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+-={}[]/.,\n ;".contains(character.toString());
	}
	
	public void keyPressed(KeyEvent e) {
		String str = new String(new Character(e.getKeyChar()).toString());
		String ops = "+-/*^";
		if (e.getKeyCode() == 10) {
			input = display.currentNumber.getText();
			isFirstChar = true;
			run();
			log.log.ensureIndexIsVisible(0);
			return;
		}
		if (isValidChar(e.getKeyChar())) {
			if (isFirstChar && ops.contains(str))
				display.currentNumber.setText(display.previousNumber.getText());
			else if (isFirstChar && !ops.contains(str))
				display.currentNumber.setText("");
			if (!display.currentNumber.isFocusOwner())
				display.addToEquation(str);
			isFirstChar = false;
		}
		if (!display.currentNumber.isFocusOwner() && e.getKeyCode() == 8) {
			display.backspace();
			isFirstChar = false;
		}
		input = display.currentNumber.getText();
		scrollHistory(e);
	}
	
	public void scrollHistory(KeyEvent e) {
		//up is 38, down is 40.
		if (core.getHistory().size() > 0) {
			isFirstChar = false;
			if (e.getKeyCode() == 40)
				if (historyLocation < core.getHistory().size() - 1)
					historyLocation++;
				else
					historyLocation = core.getHistory().size() - 1;
			else if (e.getKeyCode() == 38 && historyLocation > 0)
				historyLocation--;
			else
				return;
			log.log.setSelectedIndex(historyLocation);
		}
	}
	
	public void setFromHistory(int location) {
		if (location < 0)
			return;
		display.currentNumber.setText(core.getHistory().get(location).getInput().toString());
		display.previousNumber.setText(core.getHistory().get(location).getResult().toString());
		input = display.currentNumber.getText();
	}
	
	/**
	 * @return the display
	 */
	public UpperDisplay getDisplay() {
		return display;
	}
	
	/**
	 * @return the log
	 */
	public EquationLog getLog() {
		return log;
	}
	
	/**
	 * @return the buttons
	 */
	public CalculatorMainButtons getButtons() {
		return buttons;
	}
	
	/**
	 * @return the {@link lipstone.joshua.parser.ParserCore ParserCore} that this GUI system is using
	 */
	public ParserCore getParserCore() {
		return core;
	}
	
	/**
	 * @return the angleBar
	 */
	public AngleBar getAngleBar() {
		return angleBar;
	}
	
	public void setInput(String input) {
		this.input = input;
	}
	
	public String getInput() {
		return input;
	}
	
	/**
	 * Basically passes the stored input to the <tt>Parser</tt>
	 */
	public void run() {
		String result = core.parse(input).getResult().toString();
		log.log.setSelectedIndex(0);
		display.previousNumber.setText(result);
		display.currentNumber.setText("");
	}
}
