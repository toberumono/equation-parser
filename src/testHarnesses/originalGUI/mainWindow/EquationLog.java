package testHarnesses.originalGUI.mainWindow;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.swing.AbstractListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import lipstone.joshua.parser.util.Equation;

import testHarnesses.originalGUI.Core;

public class EquationLog extends JPanel {
	public JList<Equation> log;
	private static final Font f = new Font("Serif", Font.PLAIN, 14);
	private KeyListener keysTyped;
	private ListSelectionListener selectionChanged;
	private final Core core;
	public final ListModel<Equation> data;
	
	public EquationLog(Core core) {
		super();
		this.core = core;
		this.data = new ListModel<>(core.getParserCore().getHistory().getList());
		super.setLayout(new GridBagLayout());
		super.setBackground(null);
		super.setBorder(null);
		super.setAlignmentY(LEFT_ALIGNMENT);
		super.setFocusable(false);
		super.setMinimumSize(new Dimension(250, 50));
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(0, 0, 0, 0);
		c.fill = GridBagConstraints.BOTH;
		SimpleAttributeSet attribs = new SimpleAttributeSet();
		attribs.addAttribute(StyleConstants.LeftIndent, 3);
		setupActionListeners();
		log = new JList<>(data);
		JScrollPane scrollPane = new JScrollPane(log);
		scrollPane.setPreferredSize(new Dimension(730, 245));
		scrollPane.setBorder(null);
		log.setFocusable(false);
		log.setForeground(Color.WHITE);
		log.setFont(f);
		log.setBackground(Color.BLACK);
		log.setVisible(true);
		log.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		if (log.getModel().getSize() > 0)
			log.ensureIndexIsVisible(0);
		log.addKeyListener(keysTyped);
		log.addListSelectionListener(selectionChanged);
		c.gridy = 0;
		c.gridx = 0;
		super.add(scrollPane, c);
	}
	
	private void setupActionListeners() {
		keysTyped = new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				core.keyPressed(e);
			}
			
			@Override
			public void keyTyped(KeyEvent e) {}
			
			@Override
			public void keyReleased(KeyEvent e) {}
		};
		selectionChanged = new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (data.size() < 1)
					return;
				int selected = log.getSelectedIndex();
				core.historyLocation = selected;
				core.setFromHistory(core.historyLocation);
				log.ensureIndexIsVisible(selected - 2);
			}
		};
	}
	
	public void clearLog() {
		core.getParserCore().getHistory().clear();
	}
	
	public void resetLog() {
		//clearLog();
		//data.addElement("0.0");
	}
	
	/*public void setCurrentInput(String current) {
		if (data.getSize() < 1)
			data.addElement("0.0");
		data.set(data.size() - 1, current);
	}*/
	
	/*private void loadFromFile() {
		ArrayList<Equation> history = core.history.get();
		for (Equation eqn : history) {
			String newline = "";
			if (eqn.eqn.contains("=")) {
				newline = eqn.eqn.trim() + " -> ";
				if (core.containsVariable(eqn.eqn, core.getVariables(eqn.eqn)) && core.getVariables(eqn.eqn).size() == 1)
					newline = newline + core.getVariables(eqn.eqn).get(0) + " = ";
				newline = newline + eqn.answer;
			}
			else if (!core.isCommand(eqn.eqn.trim()))
				newline = eqn.eqn.trim() + " = " + eqn.answer;
			else if (eqn.eqn.equalsIgnoreCase("get memory"))
				newline = "memory" + " = " + eqn.answer;
			int isInvalid = eqn.isInvalid;
			if (isInvalid != 0)
				newline += "  However, this answer may be incorrect because ";
			if (isInvalid == 1)
				newline += "there was an error in your parentheses.";
			if (isInvalid == 2)
				newline += "there was an invalid value in an inverse trigonometric function.";
			if (isInvalid == 3)
				newline += "you divided by zero.";
			if (isInvalid == 4)
				newline += "your factorial was out of range, it must be within (-25,25).";
			if (isInvalid == 5)
				newline += "there was an invalid determinant operation.";
			else if (isInvalid == 6)
				newline += "the integration system ran out of time.";
			data.add(0, newline);
		}
	}*/
	
	class ListModel<E> extends AbstractListModel<E> {
		private List<E> internal;
		
		public ListModel(List<E> internal) {
			this.internal = internal;
		}
		
		/**
		 * Returns the number of components in this list.
		 * <p>
		 * This method is identical to <code>size</code>, which implements the <code>List</code> interface defined in the 1.2
		 * Collections framework. This method exists in conjunction with <code>setSize</code> so that <code>size</code> is
		 * identifiable as a JavaBean property.
		 *
		 * @return the number of components in this list
		 * @see #size()
		 */
		@Override
		public int getSize() {
			return internal.size();
		}
		
		/**
		 * Returns the component at the specified index. <blockquote> <b>Note:</b> Although this method is not deprecated,
		 * the preferred method to use is <code>get(int)</code>, which implements the <code>List</code> interface defined in
		 * the 1.2 Collections framework. </blockquote>
		 * 
		 * @param index
		 *            an index into this list
		 * @return the component at the specified index
		 * @exception ArrayIndexOutOfBoundsException
		 *                if the <code>index</code> is negative or greater than the current size of this list
		 * @see #get(int)
		 */
		@Override
		public E getElementAt(int index) {
			return internal.get(index);
		}
		
		/**
		 * Returns the number of components in this list.
		 *
		 * @return the number of components in this list
		 * @see ArrayList#size()
		 */
		public int size() {
			return internal.size();
		}
		
		/**
		 * Tests whether this list has any components.
		 *
		 * @return <code>true</code> if and only if this list has no components, that is, its size is zero;
		 *         <code>false</code> otherwise
		 * @see ArrayList#isEmpty()
		 */
		public boolean isEmpty() {
			return internal.isEmpty();
		}
		
		/**
		 * Tests whether the specified object is a component in this list.
		 *
		 * @param elem
		 *            an object
		 * @return <code>true</code> if the specified object is the same as a component in this list
		 * @see ArrayList#contains(Object)
		 */
		public boolean contains(Object elem) {
			return internal.contains(elem);
		}
		
		/**
		 * Searches for the first occurrence of <code>elem</code>.
		 *
		 * @param elem
		 *            an object
		 * @return the index of the first occurrence of the argument in this list; returns <code>-1</code> if the object is
		 *         not found
		 * @see ArrayList#indexOf(Object)
		 */
		public int indexOf(Object elem) {
			return internal.indexOf(elem);
		}
		
		/**
		 * Returns the index of the last occurrence of <code>elem</code>.
		 *
		 * @param elem
		 *            the desired component
		 * @return the index of the last occurrence of <code>elem</code> in the list; returns <code>-1</code> if the object
		 *         is not found
		 * @see ArrayList#lastIndexOf(Object)
		 */
		public int lastIndexOf(Object elem) {
			return internal.lastIndexOf(elem);
		}
		
		/**
		 * Returns the first component of this list. Throws a <code>NoSuchElementException</code> if this vector has no
		 * components.
		 * 
		 * @return the first component of this list
		 */
		public E firstElement() {
			if (internal.size() == 0)
				throw new NoSuchElementException();
			return internal.get(0);
		}
		
		/**
		 * Returns the last component of the list. Throws a <code>NoSuchElementException</code> if this vector has no
		 * components.
		 *
		 * @return the last component of the list
		 */
		public E lastElement() {
			if (internal.size() == 0)
				throw new NoSuchElementException();
			return internal.get(internal.size() - 1);
		}
		
		/**
		 * Sets the component at the specified <code>index</code> of this list to be the specified element. The previous
		 * component at that position is discarded.
		 * <p>
		 * Throws an <code>ArrayIndexOutOfBoundsException</code> if the index is invalid. <blockquote> <b>Note:</b> Although
		 * this method is not deprecated, the preferred method to use is <code>set(int,Object)</code>, which implements the
		 * <code>List</code> interface defined in the 1.2 Collections framework. </blockquote>
		 *
		 * @param element
		 *            what the component is to be set to
		 * @param index
		 *            the specified index
		 * @see #set(int,Object)
		 * @see ArrayList#set(int, Object)
		 */
		public void setElementAt(E element, int index) {
			internal.set(index, element);
			fireContentsChanged(this, index, index);
		}
		
		/**
		 * Deletes the component at the specified index.
		 * <p>
		 * Throws an <code>ArrayIndexOutOfBoundsException</code> if the index is invalid. <blockquote> <b>Note:</b> Although
		 * this method is not deprecated, the preferred method to use is <code>remove(int)</code>, which implements the
		 * <code>List</code> interface defined in the 1.2 Collections framework. </blockquote>
		 *
		 * @param index
		 *            the index of the object to remove
		 * @see #remove(int)
		 * @see ArrayList#remove(int)
		 */
		public void removeElementAt(int index) {
			internal.remove(index);
			fireIntervalRemoved(this, index, index);
		}
		
		/**
		 * Inserts the specified element as a component in this list at the specified <code>index</code>.
		 * <p>
		 * Throws an <code>ArrayIndexOutOfBoundsException</code> if the index is invalid. <blockquote> <b>Note:</b> Although
		 * this method is not deprecated, the preferred method to use is <code>add(int,Object)</code>, which implements the
		 * <code>List</code> interface defined in the 1.2 Collections framework. </blockquote>
		 *
		 * @param element
		 *            the component to insert
		 * @param index
		 *            where to insert the new component
		 * @exception ArrayIndexOutOfBoundsException
		 *                if the index was invalid
		 * @see #add(int,Object)
		 * @see ArrayList#add(int, Object)
		 */
		public void insertElementAt(E element, int index) {
			internal.add(index, element);
			fireIntervalAdded(this, index, index);
		}
		
		/**
		 * Adds the specified component to the end of this list.
		 *
		 * @param element
		 *            the component to be added
		 */
		public void addElement(E element) {
			int index = internal.size();
			internal.add(element);
			fireIntervalAdded(this, index, index);
		}
		
		/**
		 * Removes the first (lowest-indexed) occurrence of the argument from this list.
		 *
		 * @param obj
		 *            the component to be removed
		 * @return <code>true</code> if the argument was a component of this list; <code>false</code> otherwise
		 */
		public boolean removeElement(Object obj) {
			int index = indexOf(obj);
			boolean rv = internal.remove(obj);
			if (index >= 0) {
				fireIntervalRemoved(this, index, index);
			}
			return rv;
		}
		
		/**
		 * Removes all components from this list and sets its size to zero. <blockquote> <b>Note:</b> Although this method is
		 * not deprecated, the preferred method to use is <code>clear</code>, which implements the <code>List</code>
		 * interface defined in the 1.2 Collections framework. </blockquote>
		 *
		 * @see #clear()
		 */
		public void removeAllElements() {
			int index1 = internal.size() - 1;
			internal.clear();
			if (index1 >= 0)
				fireIntervalRemoved(this, 0, index1);
		}
		
		/**
		 * Returns a string that displays and identifies this object's properties.
		 *
		 * @return a String representation of this object
		 */
		@Override
		public String toString() {
			return internal.toString();
		}
		
		/* The remaining methods are included for compatibility with the
		 * Java 2 platform Vector class.
		 */
		
		/**
		 * Returns an array containing all of the elements in this list in the correct order.
		 *
		 * @return an array containing the elements of the list
		 * @see ArrayList#toArray()
		 */
		public Object[] toArray() {
			return internal.toArray();
		}
		
		/**
		 * Returns the element at the specified position in this list.
		 * <p>
		 * Throws an <code>ArrayIndexOutOfBoundsException</code> if the index is out of range (
		 * <code>index &lt; 0 || index &gt;= size()</code>).
		 *
		 * @param index
		 *            index of element to return
		 */
		public E get(int index) {
			return internal.get(index);
		}
		
		/**
		 * Replaces the element at the specified position in this list with the specified element.
		 * <p>
		 * Throws an <code>ArrayIndexOutOfBoundsException</code> if the index is out of range (
		 * <code>index &lt; 0 || index &gt;= size()</code>).
		 *
		 * @param index
		 *            index of element to replace
		 * @param element
		 *            element to be stored at the specified position
		 * @return the element previously at the specified position
		 */
		public E set(int index, E element) {
			E rv = internal.get(index);
			internal.set(index, element);
			fireContentsChanged(this, index, index);
			return rv;
		}
		
		/**
		 * Inserts the specified element at the specified position in this list.
		 * <p>
		 * Throws an <code>ArrayIndexOutOfBoundsException</code> if the index is out of range (
		 * <code>index &lt; 0 || index &gt; size()</code>).
		 *
		 * @param index
		 *            index at which the specified element is to be inserted
		 * @param element
		 *            element to be inserted
		 */
		public void add(int index, E element) {
			internal.add(index, element);
			fireIntervalAdded(this, index, index);
		}
		
		/**
		 * Removes the element at the specified position in this list. Returns the element that was removed from the list.
		 * <p>
		 * Throws an <code>ArrayIndexOutOfBoundsException</code> if the index is out of range (
		 * <code>index &lt; 0 || index &gt;= size()</code>).
		 *
		 * @param index
		 *            the index of the element to removed
		 * @return the element previously at the specified position
		 */
		public E remove(int index) {
			E rv = internal.get(index);
			internal.remove(index);
			fireIntervalRemoved(this, index, index);
			return rv;
		}
		
		/**
		 * Removes all of the elements from this list. The list will be empty after this call returns (unless it throws an
		 * exception).
		 */
		public void clear() {
			int index1 = internal.size() - 1;
			internal.clear();
			if (index1 >= 0)
				fireIntervalRemoved(this, 0, index1);
		}
		
		/**
		 * Deletes the components at the specified range of indexes. The removal is inclusive, so specifying a range of (1,5)
		 * removes the component at index 1 and the component at index 5, as well as all components in between.
		 * <p>
		 * Throws an <code>ArrayIndexOutOfBoundsException</code> if the index was invalid. Throws an
		 * <code>IllegalArgumentException</code> if <code>fromIndex &gt; toIndex</code>.
		 *
		 * @param fromIndex
		 *            the index of the lower end of the range
		 * @param toIndex
		 *            the index of the upper end of the range
		 * @see #remove(int)
		 */
		public void removeRange(int fromIndex, int toIndex) {
			if (fromIndex > toIndex)
				throw new IllegalArgumentException("fromIndex must be <= toIndex");
			for (int i = toIndex; i >= fromIndex; i--)
				internal.remove(i);
			fireIntervalRemoved(this, fromIndex, toIndex);
		}
	}
}