package testHarnesses.originalGUI.mainWindow;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import testHarnesses.originalGUI.Core;

public class UpperDisplay extends JPanel {
	private final Core core;
	public final JTextField currentNumber = new JTextField("0.0"), previousNumber = new JTextField();
	private final JTextField logo = new JTextField("Calculator by Joshua Lipstone");
	private final JPanel upperSpacer = new JPanel();
	private static Font f = new Font("Serif", Font.PLAIN, 16);
	private KeyListener keysTyped, scrollHistory;
	
	public UpperDisplay(Core core) {
		super();
		this.core = core;
		super.setLayout(new GridBagLayout());
		setUpActionListeners();
		displaySetup();
	}
	
	public void displaySetup() {
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(0, 0, 0, 0);
		c.fill = GridBagConstraints.BOTH;
		c.gridwidth = 1;
		setupLogo(c);
		c.gridwidth = 2;
		previousNumber.setFocusable(true);
		previousNumber.setEditable(false);
		previousNumber.setBorder(null);
		previousNumber.setPreferredSize(new Dimension(447, 25));
		previousNumber.setFont(f);
		previousNumber.setBackground(Color.white);
		previousNumber.setVisible(true);
		previousNumber.setHorizontalAlignment(JTextField.LEFT);
		previousNumber.addKeyListener(keysTyped);
		c.gridx = 1;
		c.gridwidth = GridBagConstraints.REMAINDER;
		add(previousNumber, c);
		currentNumber.setFocusable(true);
		currentNumber.setEditable(true);
		Border border = BorderFactory.createEmptyBorder(0, 3, 0, 0);
		currentNumber.setBorder(border);
		currentNumber.setBackground(Color.white);
		currentNumber.setPreferredSize(new Dimension(730, 25));
		currentNumber.setFont(f);
		currentNumber.setVisible(true);
		currentNumber.setHorizontalAlignment(JTextField.LEFT);
		currentNumber.addKeyListener(keysTyped);
		currentNumber.addKeyListener(scrollHistory);
		c.gridx = 0;
		c.gridy = 1;
		add(currentNumber, c);
	}
	
	public void setupLogo(GridBagConstraints c) {
		logo.setFont(new Font("Serif", Font.BOLD, 16));
		logo.setPreferredSize(new Dimension(280, 25));
		logo.setBorder(null);
		logo.setOpaque(false);
		logo.setFocusable(true);
		logo.setEditable(false);
		logo.setHorizontalAlignment(JTextField.CENTER);
		logo.addKeyListener(keysTyped);
		upperSpacer.setOpaque(false);
		upperSpacer.setFocusable(true);
		upperSpacer.setBackground(Color.red);
		upperSpacer.setPreferredSize(new Dimension(280, 25));
		upperSpacer.setLayout(new GridBagLayout());
		upperSpacer.addKeyListener(keysTyped);
		upperSpacer.add(logo, c);
		super.add(upperSpacer, c);
	}
	
	public void clearEquation() {
		previousNumber.setText("");
	}
	
	public void updateDisplay() {
		currentNumber.setText(core.getInput());
	}
	
	public void clearDisplay() {
		currentNumber.setText("0.0");
		previousNumber.setText("");
	}
	
	private void setUpActionListeners() {
		keysTyped = new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				core.keyPressed(e);
			}
			
			@Override
			public void keyTyped(KeyEvent e) {}
			
			@Override
			public void keyReleased(KeyEvent e) {}
		};
		scrollHistory = new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {}
			
			@Override
			public void keyTyped(KeyEvent e) {}
			
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == 38 || e.getKeyCode() == 40)
					currentNumber.setCaretPosition(currentNumber.getText().length());
			}
		};
	}
	
	public void addToEquation(String str) {
		currentNumber.setText(currentNumber.getText() + str);
		core.setInput(currentNumber.getText());
	}
	
	public void backspace() {
		if (currentNumber.getText().length() > 0)
			currentNumber.setText(currentNumber.getText().substring(0, currentNumber.getText().length() - 1));
		core.setInput(currentNumber.getText());
	}
	
	/**
	 * Removes double negative, '++', '+-', and '-+' cases.
	 */
	public void removeDoubles() {
		String str = currentNumber.getText();
		str = str.replaceAll("\\-\\-", "+");
		str = str.replaceAll("\\+\\+", "+");
		str = str.replaceAll("[\\-\\+][\\-\\+]", "-");
		currentNumber.setText(str);
		core.setInput(str);
	}
}