//Contains a JPanel with a grid layout.
package testHarnesses.originalGUI.mainWindow;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import lipstone.joshua.parser.SettingsWindow;
import testHarnesses.originalGUI.Core;

@SuppressWarnings({"serial", "static-access"})
public class CalculatorMainButtons extends JPanel {
	public JButton[][] mainButtons = new JButton[5][5];
	private JButton addition, subtraction, multiplication, division, equals, clear, clearall, mRecall, options, help;
	
	private ActionListener numberClick, operatorClick, memoryClick, optionsClick, helpClick;
	public double currentNum = 0, firstInputNum = 0, SecondInputNum = 0;
	public int decimalCount = 0;
	boolean decimal = false, newOp = false, decimalLock = false, firstNum = true, thirdNum = false;
	private static final Font unselectedFont = new Font("Serif", Font.PLAIN, 16), selectedFont = new Font("Serif", Font.BOLD, 16),
			operatorSelectedFont = new Font("Serif", Font.BOLD, 20);
	private final Core core;
	private String operators = "+-*/^E";
	public SettingsWindow settingsWindow;
	
	public CalculatorMainButtons(Core core) {
		super();
		this.core = core;
		super.setLayout(new GridBagLayout());
		ActionListenerSetup();
		ButtonSetup();
	}
	
	private void ActionListenerSetup() {
		numberClick = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//load keypad interaction into the core
				if (e.getSource() != mainButtons[2][4]) {
					core.setInput(core.getInput() + ((JButton) e.getSource()).getText());
				}
				else if (e.getSource() == mainButtons[2][4]) {
					String str = new String(core.getDisplay().currentNumber.getText());
					for (int i = str.length() - 1; i >= 0; i--) {
						if (operators.contains(new Character(str.charAt(i)).toString()) || str.charAt(i) == '(' || str.charAt(i) == ')') {
							str = str.substring(0, i + 1) + "-" + str.substring(i + 1);
							break;
						}
						else if (i == 0)
							str = "-" + str;
					}
					core.getDisplay().removeDoubles();
					core.getDisplay().currentNumber.setText(str);
					core.setInput(str);
					return;
				}
				core.getDisplay().updateDisplay();
			}
		};
		
		operatorClick = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == equals) {
					core.run();
					reset();
					return;
				}
				else if (e.getSource() == clear) {
					clear();
					return;
				}
				else if (e.getSource() == clearall) {
					clear();
					//core.clearMemory();
					core.getDisplay().clearDisplay();
					core.getLog().clearLog();
				}
				else {
					core.getDisplay().addToEquation(((JButton) e.getSource()).getName());
				}
			}
		};
		/*memoryClick = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (((JButton) e.getSource()).getText().equals("mr")) {
					if (!core.memory.equals("")) {
						core.isFirstChar = false;
						if (core.getDisplay().currentNumber.getText().trim().equals("0.0"))
							core.setInput(core.memory);
						else
							core.getDisplay().addToEquation(core.memory);
						core.getDisplay().updateDisplay();
					}
					return;
				}
				if (!core.getInput().equals(""))
					core.run();
				if (((JButton) e.getSource()).getText().trim().equals("mc"))
					core.setInput("clear memory");
				if (((JButton) e.getSource()).getText().trim().equals("m+"))
					core.setInput(("add to memory"));
				if (((JButton) e.getSource()).getText().trim().equals("m-"))
					core.setInput(("subtract from memory"));
				core.getDisplay().updateDisplay();
				core.run();
			}
		};*/
		optionsClick = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SettingsWindow window = SettingsWindow.getSettingsWindow(core.getParserCore());
				Container c = core.getButtons();
				while ((c = c.getParent()) != null && !(c instanceof JFrame));
				if (c != null) {
					Point p = c.getLocation();
					p.x = p.x + c.getWidth();
					window.setLocation(p);
				}
				window.makeVisible();
			}
		};
		helpClick = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SettingsWindow window = SettingsWindow.getSettingsWindow(core.getParserCore());
				int index = window.indexOfTab("Help");
				if (index == -1)
					return;
				window.switchToTab(index);
				Container c = core.getButtons();
				while ((c = c.getParent()) != null && !(c instanceof JFrame));
				if (c != null) {
					Point p = c.getLocation();
					p.x = p.x + c.getWidth();
					window.setLocation(p);
				}
				window.makeVisible();
			}
		};
	}
	
	private void ButtonSetup() {
		GridBagConstraints c = new GridBagConstraints();
		Dimension buttonDimension = new Dimension(54, 40);
		c.insets = new Insets(1, 1, 1, 1);
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 5;
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridBagLayout());
		options = new JButton("settings");
		options.addActionListener(optionsClick);
		options.setFocusable(false);
		buttonPanel.add(options);
		help = new JButton("help");
		help.addActionListener(helpClick);
		help.setFocusable(false);
		buttonPanel.add(help);
		super.add(buttonPanel, c);
		c.gridwidth = 1;
		//Set up numpad
		Integer buttonLabel = new Integer(-2);
		for (int i = 4; i >= 0; i--) {
			for (int a = 0; a < 3; a++) {
				mainButtons[a][i] = new JButton();
				mainButtons[a][i].setPreferredSize(buttonDimension);
				mainButtons[a][i].setMinimumSize(buttonDimension);
				mainButtons[a][i].setFocusable(false);
				if (buttonLabel <= 9)
					mainButtons[a][i].addActionListener(numberClick);
				else
					mainButtons[a][i].addActionListener(memoryClick);
				mainButtons[a][i].setFont(unselectedFont);
				mainButtons[a][i].setHorizontalTextPosition(SwingConstants.CENTER);
				if (buttonLabel == -2) {
					mainButtons[a][i].setText(".");
					mainButtons[a][i].setFont(selectedFont);
				}
				else if (buttonLabel == -1)
					mainButtons[a][i].setText("0");
				else if (buttonLabel == 0)
					mainButtons[a][i].setText(new Character(((char) Integer.parseInt("00B1", 16))).toString());
				else if (buttonLabel == 10)
					mainButtons[a][i].setText("mc");
				else if (buttonLabel == 11)
					mainButtons[a][i].setText("m+");
				else if (buttonLabel == 12)
					mainButtons[a][i].setText("m-");
				else
					mainButtons[a][i].setText(buttonLabel.toString());
				c.gridx = a;
				c.gridy = i + 1;
				super.add(mainButtons[a][i], c);
				buttonLabel++;
			}
		}
		functionSetup(c, buttonDimension);
	}
	
	public void clearDecimal() {
		decimal = false;
		decimalLock = false;
		decimalCount = 0;
	}
	
	public void functionSetup(GridBagConstraints c, Dimension buttonDimension) {
		addition = new JButton("+");
		subtraction = new JButton("-");
		multiplication = new JButton("*");
		division = new JButton("/");
		equals = new JButton("=");
		clear = new JButton("C");
		clearall = new JButton("CE");
		mRecall = new JButton("mr");
		//primary sizing
		addition.setPreferredSize(buttonDimension);
		subtraction.setPreferredSize(buttonDimension);
		multiplication.setPreferredSize(buttonDimension);
		division.setPreferredSize(buttonDimension);
		equals.setPreferredSize(new Dimension(54, 82));
		clear.setPreferredSize(buttonDimension);
		clearall.setPreferredSize(buttonDimension);
		mRecall.setPreferredSize(buttonDimension);
		//secondary sizing
		addition.setMinimumSize(buttonDimension);
		subtraction.setMinimumSize(buttonDimension);
		multiplication.setMinimumSize(buttonDimension);
		division.setMinimumSize(buttonDimension);
		equals.setMinimumSize(new Dimension(54, 82));
		clear.setMinimumSize(buttonDimension);
		clearall.setMinimumSize(buttonDimension);
		mRecall.setMinimumSize(buttonDimension);
		//blocking square from forming around button text
		addition.setFocusable(false);
		subtraction.setFocusable(false);
		multiplication.setFocusable(false);
		division.setFocusable(false);
		equals.setFocusable(false);
		clear.setFocusable(false);
		clearall.setFocusable(false);
		mRecall.setFocusable(false);
		//formal Naming
		addition.setName("+");
		subtraction.setName("-");
		multiplication.setName("*");
		division.setName("/");
		equals.setName("equals");
		clear.setName("clear");
		clear.setName("clearall");
		mRecall.setName("mr");
		//setting primary Font and location
		addition.setFont(unselectedFont);
		subtraction.setFont(unselectedFont);
		multiplication.setFont(unselectedFont);
		division.setFont(unselectedFont);
		equals.setFont(unselectedFont);
		clear.setFont(unselectedFont);
		clearall.setFont(unselectedFont);
		mRecall.setFont(unselectedFont);
		addition.setHorizontalTextPosition(SwingConstants.CENTER);
		subtraction.setHorizontalTextPosition(SwingConstants.CENTER);
		multiplication.setHorizontalTextPosition(SwingConstants.CENTER);
		division.setHorizontalTextPosition(SwingConstants.CENTER);
		equals.setHorizontalTextPosition(SwingConstants.CENTER);
		clear.setHorizontalTextPosition(SwingConstants.CENTER);
		clearall.setHorizontalTextPosition(SwingConstants.CENTER);
		mRecall.setHorizontalTextPosition(SwingConstants.CENTER);
		//add action listener
		addition.addActionListener(operatorClick);
		subtraction.addActionListener(operatorClick);
		multiplication.addActionListener(operatorClick);
		division.addActionListener(operatorClick);
		equals.addActionListener(operatorClick);
		clear.addActionListener(operatorClick);
		clearall.addActionListener(operatorClick);
		mRecall.addActionListener(memoryClick);
		//adding buttons to the pad
		c.gridx = 3;
		c.gridy = 5;
		super.add(addition, c);
		c.gridx = 3;
		c.gridy = 4;
		super.add(subtraction, c);
		c.gridx = 3;
		c.gridy = 3;
		super.add(multiplication, c);
		c.gridx = 3;
		c.gridy = 2;
		super.add(division, c);
		c.gridx = 4;
		c.gridy = 4;
		c.gridheight = 2;
		super.add(equals, c);
		c.gridx = 4;
		c.gridy = 3;
		c.gridheight = 1;
		super.add(clear, c);
		c.gridx = 4;
		c.gridy = 2;
		super.add(clearall, c);
		c.gridx = 3;
		c.gridy = 1;
		super.add(mRecall, c);
	}
	
	public void buttonHighlight(ActionEvent e) {
		if (e.getSource() == addition)
			addition.setFont(operatorSelectedFont);
		else
			addition.setFont(unselectedFont);
		
		if (e.getSource() == subtraction)
			subtraction.setFont(operatorSelectedFont);
		else
			subtraction.setFont(unselectedFont);
		
		if (e.getSource() == multiplication)
			multiplication.setFont(operatorSelectedFont);
		else
			multiplication.setFont(unselectedFont);
		
		if (e.getSource() == division)
			division.setFont(operatorSelectedFont);
		else
			division.setFont(unselectedFont);
	}
	
	public void reset() {
		clearDecimal();
		firstNum = true;
		newOp = false;
		thirdNum = false;
		currentNum = 0;
	}
	
	public void clear() {
		core.getDisplay().clearDisplay();
		core.getDisplay().clearEquation();
		core.getDisplay().currentNumber.setText("0.0");
		core.getDisplay().previousNumber.setText("");
		reset();
	}
}