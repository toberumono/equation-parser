package testHarnesses.originalGUI.mainWindow;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import testHarnesses.originalGUI.Core;

import lipstone.joshua.parser.plugin.pluggable.AngleType;

public class AngleBar extends JPanel {
	public JRadioButton degreesButton = new JRadioButton("Degrees"), radiansButton = new JRadioButton("Radians"),
			gradsButton = new JRadioButton("Grads");
	public ButtonGroup angleButtons = new ButtonGroup();
	private JTextField angleInfo = new JTextField("360\u00B0 = 2\u03C0 radians = 400 grads");
	private final Core core;
	private ActionListener angleChange;
	
	public AngleBar(Core core) {
		super();
		this.core = core;
		super.setLayout(new GridBagLayout());
		super.setBackground(null);
		super.setBorder(null);
		super.setAlignmentY(LEFT_ALIGNMENT);
		super.setFocusable(false);
		super.setPreferredSize(new Dimension(250, 50));
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(1, 1, 1, 1);
		setupActionListeners();
		setupButtons(c);
		c.gridy = 1;
		c.gridwidth = 3;
		setupAngleInfo(c);
		c.gridy = 2;
	}
	
	private void setupButtons(GridBagConstraints c) {
		angleButtons.add(degreesButton);
		angleButtons.add(radiansButton);
		angleButtons.add(gradsButton);
		degreesButton.addActionListener(angleChange);
		radiansButton.addActionListener(angleChange);
		gradsButton.addActionListener(angleChange);
		AngleType angleType = core.getParserCore().getParser().activeAngleType.get();
		if (angleType.equals("Degrees"))
			degreesButton.setSelected(true);
		else if (angleType.equals("Radians"))
			radiansButton.setSelected(true);
		else if (angleType.equals("Grads"))
			gradsButton.setSelected(true);
		degreesButton.addActionListener(angleChange);
		radiansButton.addActionListener(angleChange);
		gradsButton.addActionListener(angleChange);
		degreesButton.setFocusable(false);
		radiansButton.setFocusable(false);
		gradsButton.setFocusable(false);
		super.add(degreesButton, c);
		super.add(radiansButton, c);
		super.add(gradsButton, c);
	}
	
	private void setupAngleInfo(GridBagConstraints c) {
		angleInfo.setBackground(null);
		angleInfo.setBorder(null);
		angleInfo.setFocusable(false);
		angleInfo.setEditable(false);
		super.add(angleInfo, c);
	}
	
	private void setupActionListeners() {
		angleChange = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				core.getParserCore().getParser().activeAngleType.set(core.getParserCore().getParser().angleTypes.get(((JRadioButton) e.getSource()).getText()));
			}
		};
		core.getParserCore().getParser().activeAngleType.addListener(
				(observer, oldType, newType) -> {
					String type = newType.getName();
					if (type.equalsIgnoreCase("Degrees"))
						degreesButton.setSelected(true);
					else if (type.equalsIgnoreCase("Radians"))
						radiansButton.setSelected(true);
					else if (type.equalsIgnoreCase("Grads"))
						gradsButton.setSelected(true);
				});
	}
}