package testHarnesses.newGUI;

import lipstone.joshua.parser.ParserCore;
import lipstone.joshua.parser.util.Equation;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;

public class ParserGUI extends Application {
	private IOPane ioPane = null;
	private ParserCore core = null;
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("Calculator");
		core = new ParserCore();
		ioPane = new IOPane(core);
		GridPane rootPane = new GridPane();
		rootPane.add(ioPane, 0, 0, 2, 1);
		
		//Create a listview that displays equations.  We don't want it to be editable, and we only want to allow single selection.
		ListView<Equation> listView = new ListView<>(core.getHistory()); //The history object in this library is designed to be easily integrable with JavaFx
		listView.setEditable(false);
		listView.setCellFactory(EquationListCell.forListView(core, ioPane));
		listView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		listView.getStyleClass().add("equation-list");
		listView.setOnKeyPressed(event -> {
			if (event.getCode().equals(KeyCode.ENTER))
				ioPane.replaceActive(listView.getSelectionModel().getSelectedItem());
		});
		rootPane.add(listView, 0, 1, 1, 1);
		
		Scene scene = new Scene(rootPane, 750, 500);
		//Configure the gui so that all of the expansion goes to the log when the window is resized vertically.
		ColumnConstraints cols[] = {new ColumnConstraints(250, 750, Double.MAX_VALUE)};
		RowConstraints rows[] = {new RowConstraints(), new RowConstraints(250, 400, Double.MAX_VALUE)};
		cols[0].setHgrow(Priority.ALWAYS);
		rows[0].setVgrow(Priority.NEVER);
		rows[1].setVgrow(Priority.ALWAYS);
		rootPane.getColumnConstraints().addAll(cols);
		ioPane.getColumnConstraints().addAll(cols);
		rootPane.getRowConstraints().addAll(rows);
		stage.setScene(scene);
		scene.getStylesheets().add(ParserGUI.class.getResource("ParserGUI.css").toExternalForm());
		stage.show();
	}
	
}
