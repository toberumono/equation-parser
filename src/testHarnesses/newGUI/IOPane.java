package testHarnesses.newGUI;

import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import toberumono.json.JSONObject;
import toberumono.json.JSONSystem;

import lipstone.joshua.parser.ParserCore;
import lipstone.joshua.parser.util.Equation;

/**
 * Displays the input and output fields for the current and previous equations respectively.<br>
 * Drag-and-Drop based on code from:
 * http://stackoverflow.com/questions/20412445/how-to-create-a-reorder-able-tableview-in-javafx
 * 
 * @author Joshua Lipstone
 */
public class IOPane extends GridPane {
	private static final String PROMPT_TEXT = "0";
	TextField output;
	TextField input;
	private ParserCore core;
	
	public IOPane(ParserCore core) {
		super();
		this.core = core;
		output = new TextField();
		output.setEditable(false);
		input = new TextField();
		
		input.setPromptText(PROMPT_TEXT);
		output.getStyleClass().add("calculator-output");
		input.getStyleClass().add("calculator-input");
		output.setFocusTraversable(false);
		
		output.setOnKeyPressed(ke -> fireEvent(ke));
		add(output, 0, 0);
		add(input, 0, 1);
		input.setAlignment(Pos.BASELINE_RIGHT);
		output.setAlignment(Pos.BASELINE_LEFT);
		
		setOnMouseClicked(me -> {
			if (!input.focusedProperty().get()) {
				input.requestFocus();
				input.end();
			}
		});
		setOnKeyPressed(ke -> {
			if (!input.focusedProperty().get() && !(ke.isAltDown() || ke.isControlDown() || ke.isMetaDown() || ke.isShiftDown())) {
				input.requestFocus();
				input.fireEvent(ke);
			}
			else if (ke.getCode().equals(KeyCode.ENTER)) {
				if (input.getText().length() < 1)
					output.setText(PROMPT_TEXT);
				else
					onEnter();
			}
		});
		
		setOnDragOver(event -> {
			if (event.getDragboard().hasString())
				event.acceptTransferModes(TransferMode.COPY);
			event.consume();
		});
		
		setOnDragEntered(event -> {
			if (event.getDragboard().hasString())
				setOpacity(0.3);
		});
		setOnDragExited(event -> {
			if (event.getDragboard().hasString())
				setOpacity(1);
		});
		
		setOnDragDropped(event -> {
			Dragboard db = event.getDragboard();
			boolean success = false;
			
			if (db.hasString()) {
				String data = db.getString();
				try {
					replaceActive(new Equation((JSONObject) JSONSystem.parseJSON(data), core.getParser()));
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				
				EquationListCell.startIndex = -1;
				success = true;
			}
			event.setDropCompleted(success);
			
			event.consume();
		});
		setOnDragDone(DragEvent::consume);
	}
	
	/**
	 * Processes the equation.
	 */
	void onEnter() {
		if (core == null)
			return;
		Equation result = core.parse(input.getText());
		output.setText(input.getText() + " -> " + (result.getError() == null ? result.getResult().toString() : result.getError().getMessage()));
		input.clear();
		requestFocus();
	}
	
	/**
	 * Replaces the current input and output fields' text with the values from a given equation
	 * 
	 * @param equation
	 *            the equation
	 */
	void replaceActive(Equation equation) {
		if (core == null || equation == null)
			return;
		input.setText(equation.getInput().toString());
		output.setText(equation.toString());
		input.requestFocus();
		input.end();
	}
}
