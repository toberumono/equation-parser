package testHarnesses.newGUI;

import javafx.scene.SnapshotParameters;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.util.Callback;

import toberumono.json.JSONObject;
import toberumono.json.JSONSystem;

import lipstone.joshua.parser.ParserCore;
import lipstone.joshua.parser.util.Equation;

/**
 * Displays a input / answer combination for an equation.<br>
 * Drag-and-Drop based on code from:
 * http://stackoverflow.com/questions/20412445/how-to-create-a-reorder-able-tableview-in-javafx
 * 
 * @author Joshua Lipstone
 */
public class EquationListCell extends ListCell<Equation> {
	private Equation back;
	private Label answer;
	private Text placeholderInput;
	static int startIndex = -1;
	
	/**
	 * Creates a factory for generating {@link EquationListCell EquationListCells}
	 * 
	 * @param core
	 *            the {@link lipstone.joshua.parser.ParserCore ParserCore} for all of the generated {@link EquationListCell
	 *            EquationListCells} to use
	 * @param ioPane
	 *            the {@link IOPane} from the gui that all of the generated {@link EquationListCell EquationListCells} will
	 *            be a part of
	 * @return a factory for generating {@link EquationListCell EquationListCells}
	 */
	public static Callback<ListView<Equation>, ListCell<Equation>> forListView(ParserCore core, IOPane ioPane) {
		return list -> new EquationListCell(core, ioPane);
	}
	
	public EquationListCell(ParserCore core, IOPane ioPane) {
		this.getStyleClass().add("equation-list-cell");
		placeholderInput = new Text("");
		answer = new Label();
		back = null;
		setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		this.setOnMouseClicked(me -> ioPane.replaceActive(back));
		EquationListCell thisCell = this;
		setOnDragDetected(event -> {
			if (getItem() == null)
				return;
			startIndex = getIndex(); //Store the originating index
			Dragboard board = startDragAndDrop(TransferMode.COPY_OR_MOVE);
			board.setDragView(this.snapshot(new SnapshotParameters(), null));
			ClipboardContent content = new ClipboardContent();
			content.putString(back.toJSONString());
			board.setContent(content);
			event.consume();
		});
		
		setOnDragOver(event -> {
			if (event.getGestureSource() != thisCell && event.getDragboard().hasString())
				event.acceptTransferModes(TransferMode.MOVE);
			event.consume();
		});
		
		setOnDragEntered(event -> {
			if (event.getGestureSource() != thisCell && event.getDragboard().hasString())
				setOpacity(0.3);
		});
		
		setOnDragExited(event -> {
			if (event.getGestureSource() != thisCell && event.getDragboard().hasString())
				setOpacity(1);
		});
		
		setOnDragDropped(event -> {
			if (getItem() == null)
				return;
			
			Dragboard db = event.getDragboard();
			boolean success = false;
			
			if (db.hasString()) {
				String data = db.getString();
				core.getHistory().remove(startIndex);
				try {
					core.getHistory().add(getIndex(), new Equation((JSONObject) JSONSystem.parseJSON(data), core.getParser()));
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				
				startIndex = -1;
				success = true;
			}
			event.setDropCompleted(success);
			
			event.consume();
		});
		
		setOnDragDone(DragEvent::consume);
	}
	
	@Override
	protected void updateItem(Equation equation, boolean empty) {
		super.updateItem(equation, empty);
		back = equation;
		this.focusTraversableProperty().set(false);
		
		if (empty || equation == null || equation.isEmpty()) {
			setGraphic(makeText());
		}
		else {
			setGraphic(makeText());
			answer.setText(back.resultString());
			placeholderInput.setText(back.getInput().toString());
		}
	}
	
	private GridPane makeText() {
		GridPane pane = new GridPane();
		pane.add(answer, 0, 0);
		pane.add(placeholderInput, 0, 1);
		return pane;
	}
}
