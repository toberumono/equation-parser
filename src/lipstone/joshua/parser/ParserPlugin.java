package lipstone.joshua.parser;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import toberumono.json.JSONObject;
import toberumono.json.JSONObjectWrapper;
import toberumono.json.JSONSystem;
import toberumono.json.exceptions.JSONException;

import lipstone.joshua.parser.exceptions.PluginConflictException;
import lipstone.joshua.parser.plugin.ParserSettingsPlugin;
import lipstone.joshua.parser.plugin.pluggable.Command;
import lipstone.joshua.parser.plugin.pluggable.Filter;
import lipstone.joshua.parser.plugin.pluggable.Keyword;
import lipstone.joshua.parser.plugin.pluggable.Operation;
import lipstone.joshua.parser.plugin.pluggable.Operator;
import lipstone.joshua.parser.plugin.pluggable.Pluggable;
import lipstone.joshua.parser.plugin.settings.ParserSettingsItem;
import lipstone.joshua.parser.tokenizer.ConsCell;
import lipstone.joshua.parser.tokenizer.TokenizerDescender;
import lipstone.joshua.parser.tokenizer.TokenizerRule;
import lipstone.joshua.pluginLoader.Plugin;

/**
 * The base class that all plugins for this library must extend.
 * 
 * @author Joshua Lipstone
 */
public abstract class ParserPlugin implements Pluggable, Cloneable {
	private Parser parser = null;
	private ParserPlugin loader = null;
	private final String id, version, author;
	protected final Logger logger;
	
	/**
	 * An open-access {@link JPanel} that uses a {@link GridBagLayout}.
	 */
	protected final JPanel settingsPanel = new JPanel();
	private final HashMap<String, TokenizerRule> tokenizerRules = new HashMap<>();
	private final HashMap<String, TokenizerDescender> tokenizerDescenders = new HashMap<>();
	
	/**
	 * Contains the data that is in the plugin's data JSON file
	 */
	private JSONObjectWrapper<ConsCell> data = null;
	private Path dataPath = null;
	
	/**
	 * A special instance of {@link ParserPlugin} that replaces null in the loader field for security purposes
	 */
	public static final ParserPlugin NONE;
	
	static {
		NONE = new NonePlugin();
		NONE.setLoader(NONE);
	}
	
	@Plugin(author = "NONE", description = "NONE", id = "NONE", version = "NONE")
	static final class NonePlugin extends ParserPlugin {
		
		@Override
		protected void load(Parser parser) throws PluginConflictException {}
		
		@Override
		protected void loadSettings(Parser parser) {}
	}
	
	public ParserPlugin() {
		Plugin annotation = getClass().getAnnotation(Plugin.class);
		id = annotation.id();
		version = annotation.version();
		author = annotation.author();
		logger = Logger.getLogger("toberumono.parser.plugin." + getID());
	}
	
	public final String getID() {
		return id;
	}
	
	public final boolean isSettingsPlugin() {
		return getClass().getAnnotation(ParserSettingsPlugin.class) != null && (System.getProperty("java.awt.headless") == null || !System.getProperty("java.awt.headless").equals("true"));
	}
	
	/**
	 * @return the {@link ParserPlugin} that it was called on
	 */
	@Override
	public ParserPlugin getPlugin() {
		return loader;
	}
	
	/**
	 * @return the id of the {@link ParserPlugin} that it was called on
	 */
	@Override
	public String getName() {
		return getID();
	}
	
	/**
	 * Never call this method from within a plugin.
	 * 
	 * @param parser
	 *            the parser that is loading this plugin
	 * @throws PluginConflictException
	 *             when plugins attempt to overwrite another plugin's data or attempt to register data under a different ID.
	 * @throws IOException
	 */
	final void loadPlugin(Parser parser, ParserPlugin loader) throws PluginConflictException {
		this.parser = parser;
		setLoader(loader);
		dataPath = parser.getBaseLocation().resolve("data").resolve(getID() + ".json");
		boolean exists = true;
		try {
			exists = Files.exists(dataPath) && Files.size(dataPath) > 0;
		}
		catch (IOException e) {
			logger.log(Level.WARNING, "Unable to determine the size of the data file of " + getID(), e);
			exists = false;
		}
		if (!exists) {
			if (!Files.exists(dataPath))
				try {
					Files.createFile(dataPath);
				}
				catch (IOException e) {
					logger.log(Level.SEVERE, "Could not create the data file for " + getID(), e);
				}
			data = new JSONObjectWrapper<>(new JSONObject());
			try {
				JSONSystem.writeJSON(data, dataPath); //Writes the initial JSON file
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		else {
			try {
				data = new JSONObjectWrapper<>(new JSONObject());
				((JSONObject) JSONSystem.loadJSON(dataPath)).forEach((k, v) -> {
					try {
						data.put(k, parser.tokenizer.lex((String) v.value()));
					}
					catch (Exception e) {
						e.printStackTrace();
						logger.log(Level.SEVERE, "Unable to load '" + k + "' for " + getID() + "; Could not parse: " + v, e);
					}
				});
			}
			catch (IOException | JSONException e) {
				logger.log(Level.SEVERE, "Unable to load the saved data for " + getID(), e);
				e.printStackTrace();
			}
		}
		load(parser);
		if (isSettingsPlugin()) {
			settingsPanel.setName(getID());
			settingsPanel.setLayout(new GridBagLayout());
			settingsPanel.setOpaque(false);
		}
	}
	
	final void setLoader(ParserPlugin loader) {
		if (this.loader != null)
			throw new UnsupportedOperationException("Cannot change the plugin that loaded this one after it has been set.");
		if (this.loader == null)
			this.loader = loader == null ? this : loader;
	}
	
	/**
	 * This function should load perform all of the operations are needed to fully load this plugin other than opening the
	 * default data and settings files from disk.
	 * 
	 * @param parser
	 *            the parser that is loading this plugin
	 * @throws PluginConflictException
	 *             when plugins attempt to overwrite another plugin's data or attempt to register data under a different ID.
	 */
	protected abstract void load(Parser parser) throws PluginConflictException;
	
	/**
	 * All steps required to set up this plugin's settings panel need to be implemented here. Otherwise, this method can be
	 * left empty.
	 * 
	 * @param parser
	 *            the parser that is loading this plugin
	 */
	protected abstract void loadSettings(Parser parser);
	
	/**
	 * Core method for operations that need to be performed when this plugin is unloaded are here. Any additional unloading
	 * operations should be added to the appropriate helper methods.
	 * 
	 * @throws PluginConflictException
	 *             when plugins attempt to overwrite another plugin's data or attempt to register data under a different ID.
	 */
	final void unloadPlugin() throws PluginConflictException {
		data.clear();
		settingsPanel.removeAll();
	}
	
	/**
	 * Get data from the data HashMap
	 * 
	 * @param key
	 *            the key that points to the data
	 * @return the data or null if it does not exist
	 */
	public final ConsCell getData(String key) {
		return data.get(key);
	}
	
	/**
	 * Puts data in the data HashMap
	 * 
	 * @param key
	 *            to store the data under
	 * @param value
	 *            the value to map to this key
	 */
	public final void putData(String key, ConsCell value) {
		data.put(key, value);
		try {
			JSONSystem.writeJSON(data, dataPath);
		}
		catch (IOException e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Unable to save the data for " + getID(), e);
		}
	}
	
	/**
	 * Remove the data specified by key from the data HashMap
	 * 
	 * @param key
	 *            the key pointing to the data to be removed
	 * @return the value mapped to the key being removed
	 */
	public final ConsCell removeData(String key) {
		ConsCell removed = data.remove(key);
		try {
			JSONSystem.writeJSON(data, dataPath);
		}
		catch (IOException e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Unable to save the data for " + getID(), e);
		}
		return removed;
	}
	
	public final Set<String> getDataKeyset() {
		return Collections.unmodifiableSet(data.keySet());
	}
	
	public final Map<String, ConsCell> getDataMap() {
		return Collections.unmodifiableMap(data);
	}
	
	/**
	 * Add a keyword with the given name and description to this plugin
	 * 
	 * @param keyword
	 *            the keyword to be added
	 * @throws PluginConflictException
	 *             if this plugin attempts to overwrite another plugin's data or attempts to register data under a different
	 *             ID.
	 */
	@Deprecated
	public final void addKeyword(Keyword keyword) throws PluginConflictException {
		parser.keywords.add(keyword);
	}
	
	/**
	 * Remove the keyword with the given name from this plugin
	 * 
	 * @param name
	 *            the name of the keyword to remove
	 * @return the removed keyword
	 * @throws PluginConflictException
	 *             if this plugin attempts to overwrite another plugin's data or attempts to register data under a different
	 *             ID.
	 */
	@Deprecated
	public final Keyword removeKeyword(String name) throws PluginConflictException {
		return parser.keywords.tryRemove(name);
	}
	
	/**
	 * Add a command with the given name and description to this plugin
	 * 
	 * @param command
	 *            the command to be added
	 * @throws PluginConflictException
	 *             if this plugin attempts to overwrite another plugin's data or attempts to register data under a different
	 *             ID.
	 */
	@Deprecated
	public final void addCommand(Command command) throws PluginConflictException {
		parser.commands.add(command);
	}
	
	/**
	 * Remove the command with the given name from this plugin
	 * 
	 * @param name
	 *            the name of the command to remove
	 * @return the removed command
	 * @throws PluginConflictException
	 *             if this plugin attempts to overwrite another plugin's data or attempts to register data under a different
	 *             ID.
	 */
	@Deprecated
	public final Command removeCommand(String name) throws PluginConflictException {
		return parser.commands.tryRemove(name);
	}
	
	/**
	 * Add the given operation to this plugin
	 * 
	 * @param operation
	 *            the operation to add
	 * @throws PluginConflictException
	 *             if this plugin attempts to overwrite another plugin's data or attempts to register data under a different
	 *             ID.
	 */
	@Deprecated
	public final void addOperation(Operation operation) throws PluginConflictException {
		parser.operations.add(operation);
	}
	
	/**
	 * Remove the operation with the given name from this plugin
	 * 
	 * @param name
	 *            the name of the operation to remove
	 * @return the removed operation
	 * @throws PluginConflictException
	 *             if this plugin attempts to overwrite another plugin's data or attempts to register data under a different
	 *             ID.
	 */
	@Deprecated
	public final Operation removeOperation(String name) throws PluginConflictException {
		return parser.operations.tryRemove(name);
	}
	
	/**
	 * Add the given operator to this plugin
	 * 
	 * @param operator
	 *            the operator to add
	 * @throws PluginConflictException
	 *             if this plugin attempts to overwrite another plugin's data or attempts to register data under a different
	 *             ID.
	 */
	@Deprecated
	public final void addOperator(Operator operator) throws PluginConflictException {
		parser.operators.add(operator);
	}
	
	/**
	 * Remove the operator with the given name from this plugin
	 * 
	 * @param name
	 *            the name of the operator to remove
	 * @return the removed operator
	 * @throws PluginConflictException
	 *             if this plugin attempts to overwrite another plugin's data or attempts to register data under a different
	 *             ID.
	 */
	@Deprecated
	public final Operator removeOperator(String name) throws PluginConflictException {
		return parser.operators.tryRemove(name);
	}
	
	/**
	 * Add the given preprocessing filter to this plugin
	 * 
	 * @param filter
	 *            the preprocessing filter to add
	 * @throws PluginConflictException
	 *             if this plugin attempts to overwrite another plugin's data or attempts to register data under a different
	 *             ID.
	 */
	@Deprecated
	public final void addPreprocessingFilter(Filter filter) throws PluginConflictException {
		parser.preprocessingFilters.add(filter);
	}
	
	/**
	 * Remove the preprocessing filter with the given name from this plugin
	 * 
	 * @param name
	 *            the name of the filter to remove
	 * @return the removed filter
	 * @throws PluginConflictException
	 *             if this plugin attempts to overwrite another plugin's data or attempts to register data under a different
	 *             ID.
	 */
	@Deprecated
	public final Filter removePreprocessingFilter(String name) throws PluginConflictException {
		return parser.preprocessingFilters.tryRemove(name);
	}
	
	/**
	 * Add the given postprocessing filter to this plugin
	 * 
	 * @param filter
	 *            the postprocessing filter to add
	 * @throws PluginConflictException
	 *             if this plugin attempts to overwrite another plugin's data or attempts to register data under a different
	 *             ID.
	 */
	@Deprecated
	public final void addPostprocessingFilter(Filter filter) throws PluginConflictException {
		parser.postprocessingFilters.add(filter);
	}
	
	/**
	 * Remove the postprocessing filter with the given name from this plugin
	 * 
	 * @param name
	 *            the name of the filter to remove
	 * @return the removed filter
	 * @throws PluginConflictException
	 *             if this plugin attempts to overwrite another plugin's data or attempts to register data under a different
	 *             ID.
	 */
	@Deprecated
	public final Filter removePostprocessingFilter(String name) throws PluginConflictException {
		return parser.postprocessingFilters.tryRemove(name);
	}
	
	/**
	 * Add the given {@link TokenizerRule} to this plugin
	 * 
	 * @param name
	 *            the name of the {@link TokenizerRule}, which is used for identification after it has been loaded
	 * @param rule
	 *            the {@link TokenizerRule} to add
	 * @throws PluginConflictException
	 *             if this plugin attempts to overwrite another plugin's data or attempts to register data under a different
	 *             ID.
	 */
	@Deprecated
	public final void addTokenizerRule(String name, TokenizerRule rule) throws PluginConflictException {
		parser.addTokenizerRule(name, rule, this);
		tokenizerRules.put(name, rule);
	}
	
	/**
	 * Remove the {@link TokenizerRule} with the given name from this plugin
	 * 
	 * @param name
	 *            the name of the {@link TokenizerRule} to remove
	 * @return the removed {@link TokenizerRule}
	 * @throws PluginConflictException
	 *             if this plugin attempts to overwrite another plugin's data or attempts to register data under a different
	 *             ID.
	 */
	@Deprecated
	public final TokenizerRule removeTokenizerRule(String name) throws PluginConflictException {
		TokenizerRule rule = parser.removeTokenizerRule(name, this);
		tokenizerRules.remove(name);
		return rule;
	}
	
	/**
	 * Add the given {@link TokenizerDescender} to this plugin
	 * 
	 * @param name
	 *            the name of the {@link TokenizerDescender}, which is used for
	 *            identification after it has been loaded
	 * @param descender
	 *            the {@link TokenizerDescender} to add
	 * @throws PluginConflictException
	 *             if this plugin attempts to overwrite another plugin's data or attempts to register data under a different
	 *             ID.
	 */
	@Deprecated
	public final void addTokenizerDescender(String name, TokenizerDescender descender) throws PluginConflictException {
		parser.addTokenizerDescender(name, descender, this);
		tokenizerDescenders.put(name, descender);
	}
	
	/**
	 * Remove the {@link TokenizerDescender} with the given name from
	 * this plugin
	 * 
	 * @param name
	 *            the name of the {@link TokenizerDescender} to remove
	 * @return the removed {@link TokenizerDescender}
	 * @throws PluginConflictException
	 *             if this plugin attempts to overwrite another plugin's data or attempts to register data under a different
	 *             ID.
	 */
	@Deprecated
	public final TokenizerDescender removeTokenizerDescender(String name) throws PluginConflictException {
		TokenizerDescender descender = parser.removeTokenizerDescender(name, this);
		tokenizerDescenders.remove(name);
		return descender;
	}
	
	/**
	 * Adds the given {@link ParserSettingsItem} to the
	 * {@link #getSettingsPanel() settingsPanel}.
	 * 
	 * @param settingsItem
	 *            the item to add to the panel
	 */
	public final void addSettingsItem(ParserSettingsItem<?> settingsItem) {
		settingsPanel.add(settingsItem.getGUIItem());
	}
	
	/**
	 * Adds the given {@link ParserSettingsItem} to the
	 * {@link #getSettingsPanel() settingsPanel} with the given {@link GridBagConstraints}.
	 * 
	 * @param settingsItem
	 *            the item to add to the panel
	 * @param c
	 *            the {@link GridBagConstraints} to use in adding this item.
	 */
	public final void addSettingsItem(ParserSettingsItem<?> settingsItem, GridBagConstraints c) {
		settingsPanel.add(settingsItem.getGUIItem(), c);
	}
	
	/**
	 * Adds the given {@link ParserSettingsItem} to the
	 * {@link #getSettingsPanel() settingsPanel} with the given {@link GridBagConstraints}.
	 * 
	 * @param settingsItem
	 *            the item to add to the panel
	 * @param c
	 *            the {@link GridBagConstraints} to use in adding this item.
	 * @param scrollPaneSize
	 *            the size of the scroll pane to place the given
	 *            {@link ParserSettingsItem} in.
	 */
	public final void addSettingsItem(ParserSettingsItem<?> settingsItem, GridBagConstraints c, Dimension scrollPaneSize) {
		JScrollPane scrollPane = new JScrollPane(settingsItem.getGUIItem());
		scrollPane.setPreferredSize(scrollPaneSize);
		settingsPanel.add(scrollPane, c);
	}
	
	/**
	 * @return an unmodifiable {@link Set} containing the names of all of the {@link TokenizerRule TokenizerRules} mapped to
	 *         this plugin
	 */
	protected final Set<String> getTokenizerRules() {
		return Collections.unmodifiableSet(tokenizerRules.keySet());
	}
	
	/**
	 * @return an unmodifiable {@link Set} containing the names of all of the
	 *         {@link TokenizerDescender TokenizerDescenders} mapped to this plugin
	 */
	protected final Set<String> getTokenizerDescenders() {
		return Collections.unmodifiableSet(tokenizerDescenders.keySet());
	}
	
	/**
	 * @return the JPanel that the <tt>Parser</tt> will use to display this plugin's settings.
	 */
	public final JPanel getSettingsPanel() {
		return settingsPanel;
	}
	
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof ParserPlugin))
			return false;
		return getID().equals(((ParserPlugin) other).getID());
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(id, version, author);
	}
}

@FunctionalInterface
interface ReaderCurrier<T> {
	public T curry(String dataType);
}
