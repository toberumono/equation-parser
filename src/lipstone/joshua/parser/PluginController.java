package lipstone.joshua.parser;

import java.util.logging.Level;
import java.util.logging.Logger;

import lipstone.joshua.pluginLoader.PluginManager;

public final class PluginController extends PluginManager<Parser, ParserPlugin> {
	private final Logger logger;
	
	/**
	 * Constructs a new <tt>PluginController</tt> for the specified <tt>Parser</tt> and loads the plugins from the
	 * <tt>Parser's</tt> default plugin directory.
	 * 
	 * @param parser
	 *            the <tt>Parser</tt> to be linked with this <tt>PluginController</tt>
	 */
	public PluginController(Parser parser) {
		this(parser, true);
	}
	
	/**
	 * Constructs a new <tt>PluginController</tt> for the specified <tt>Parser</tt> and loads the plugins from the
	 * <tt>Parser's</tt> default plugin directory if loadDefaultPluginDirectory is <tt>true</tt>
	 * 
	 * @param parser
	 *            the <tt>Parser</tt> to be linked with this <tt>PluginController</tt>
	 * @param loadDefaultPluginDirectory
	 *            whether to load plugins from the <tt>Parser's</tt> default plugin directory
	 */
	public PluginController(Parser parser, boolean loadDefaultPluginDirectory) {
		super(parser, loadDefaultPluginDirectory);
		logger = Logger.getLogger("toberumono.parser.PluginController");
	}
	
	/**
	 * @return the {@link Parser} linked with the {@link PluginController}
	 */
	public Parser getParser() {
		return (Parser) pluginUser;
	}
	
	/**
	 * Logs an error message using the <tt>Log</tt> initialized in the <tt>Parser</tt>
	 */
	@Override
	public void logError(String error) {
		logger.log(Level.SEVERE, error);
	}
	
	/**
	 * Logs an information message using the <tt>Log</tt> initialized in the <tt>Parser</tt>
	 */
	@Override
	public void logInfo(String info) {
		logger.log(Level.INFO, info);
	}
}
