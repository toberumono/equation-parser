package lipstone.joshua.parser;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import lipstone.joshua.parser.util.PairedList;

public class SettingsWindow extends JFrame {
	private static PairedList<ParserCore, SettingsWindow> openWindows = new PairedList<>();
	private final ParserCore core;
	private final JTabbedPane pane;
	
	public static void displaySettingsWindow(ParserCore core) {
		if (!openWindows.containsKey(core))
			openWindows.put(core, new SettingsWindow(core));
		openWindows.get(core).makeVisible();
	}
	
	private SettingsWindow(ParserCore core) {
		this.core = core;
		pane = new JTabbedPane();
		for (ParserPlugin plugin : core.getParser().getSettingsPlugins())
			pane.addTab(((ParserPlugin) plugin).getSettingsPanel().getName(), ((ParserPlugin) plugin).getSettingsPanel());
		add(pane);
		pack();
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(WindowEvent e) {}

			@Override
			public void windowClosing(WindowEvent e) {}

			@Override
			public void windowClosed(WindowEvent e) {
				removeSelf();
			}

			@Override
			public void windowIconified(WindowEvent e) {}

			@Override
			public void windowDeiconified(WindowEvent e) {}

			@Override
			public void windowActivated(WindowEvent e) {}

			@Override
			public void windowDeactivated(WindowEvent e) {}
		});
	}
	
	public static SettingsWindow getSettingsWindow(ParserCore core) {
		if (!openWindows.containsKey(core))
			openWindows.put(core, new SettingsWindow(core));
		return openWindows.get(core);
	}
	
	public void makeVisible() {
		setVisible(true);
		setExtendedState(JFrame.NORMAL);
	}
	
	public int indexOfTab(String name) {
		return pane.indexOfTab(name);
	}
	
	public void switchToTab(int index) {
		pane.setSelectedIndex(index);
	}
	
	public void switchToTab(String name) {
		int index = pane.indexOfTab(name);
		if (index > -1)
			pane.setSelectedIndex(index);
	}
	
	private final void removeSelf() {
		openWindows.remove(core);
	}
}
