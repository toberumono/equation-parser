package lipstone.joshua.parser.tokenizer;

import java.util.regex.MatchResult;
import java.util.regex.Pattern;

import toberumono.lexer.genericBase.GenericAction;
import toberumono.lexer.genericBase.GenericRule;

import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.plugin.pluggable.Pluggable;

/**
 * Represents a rule for the {@link Tokenizer} that this parsing library uses.
 * 
 * @author Joshua Lipstone
 */
public class TokenizerRule extends GenericRule<ConsCell, ConsType, TokenizerRule, TokenizerDescender, Tokenizer> implements Pluggable {
	private final ParserPlugin creator;
	private final String name;
	
	/**
	 * Constructs a new {@link TokenizerRule} with the given data
	 * 
	 * @param name
	 *            the name of this {@link TokenizerRule}
	 * @param pattern
	 *            the regex <tt>Pattern</tt> for this {@link TokenizerRule} to use.
	 * @param type
	 *            the type for {@link lipstone.joshua.parser.tokenizer.ConsCell Tokens} matched by this {@link TokenizerRule}
	 * @param creator
	 *            the {@link lipstone.joshua.parser.ParserPlugin ParserPlugin} that created this {@link TokenizerRule}
	 */
	public TokenizerRule(String name, Pattern pattern, ConsType type, ParserPlugin creator) {
		super(pattern, type);
		this.creator = creator;
		this.name = name;
	}
	
	/**
	 * Constructs a new {@link TokenizerRule} with the given data
	 * 
	 * @param name
	 *            the name of this {@link TokenizerRule}
	 * @param pattern
	 *            the regex {@link java.util.regex.Pattern Pattern} for this {@link TokenizerRule} to use.
	 * @param action
	 *            the action to perform on the part of the input matched by this {@link TokenizerRule}
	 * @param creator
	 *            the {@link lipstone.joshua.parser.ParserPlugin ParserPlugin} that created this {@link TokenizerRule}
	 */
	public TokenizerRule(String name, Pattern pattern, GenericAction<ConsCell, ConsType, TokenizerRule, TokenizerDescender, Tokenizer, MatchResult> action, ParserPlugin creator) {
		super(pattern, action);
		this.creator = creator;
		this.name = name;
	}
	
	/**
	 * @return the {@link lipstone.joshua.parser.ParserPlugin plugin} that created this {@link TokenizerRule}
	 */
	@Override
	public ParserPlugin getPlugin() {
		return creator;
	}
	
	/**
	 * @return the name of this {@link TokenizerRule}
	 */
	@Override
	public String getName() {
		return name;
	}
}
