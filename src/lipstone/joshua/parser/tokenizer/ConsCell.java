package lipstone.joshua.parser.tokenizer;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import toberumono.json.JSONSerializable;
import toberumono.structures.sexpressions.generic.GenericConsCell;

import static lipstone.joshua.parser.tokenizer.ConsType.*;

public class ConsCell extends GenericConsCell<ConsType, ConsCell> implements JSONSerializable {
	
	public ConsCell(Object car, ConsType carType, Object cdr, ConsType cdrType) {
		super(car, carType, cdr, cdrType, ConsCell::new, TOKEN, EMPTY);
	}
	
	public ConsCell(Object car, ConsType carType) {
		super(car, carType, ConsCell::new, TOKEN, EMPTY);
	}
	
	public ConsCell(Object car, ConsType carType, ConsCell cdr) {
		super(car, carType, cdr, TOKEN, ConsCell::new, TOKEN, EMPTY);
	}
	
	public ConsCell() {
		super(ConsCell::new, TOKEN, EMPTY);
	}
	
	@Override
	public ConsCell singular() {
		return new ConsCell(car, carType);
	}
	
	@Override
	protected ConsCell clone(ConsCell previous) {
		ConsCell clone = new ConsCell(car instanceof ConsCell ? ((ConsCell) car).clone() : car, carType, cdr instanceof ConsCell ? ((ConsCell) cdr).clone(this) : cdr, cdrType);
		clone.previous = previous;
		return clone;
	}
	
	@Override
	public boolean equals(Object o) {
		return o instanceof ConsCell && (carType == ((ConsCell) o).carType && cdrType == ((ConsCell) o).cdrType) && ((car == null && ((ConsCell) o).car == null) || car.equals(((ConsCell) o).car)) &&
				((cdr == null && ((ConsCell) o).cdr == null) || cdr.equals(((ConsCell) o).cdr));
	}
	
	/**
	 * Determines if this {@link ConsCell} tree is equal to <tt>other</tt> for the given number of <tt>steps</tt>.
	 * 
	 * @param o
	 *            the {@link ConsCell} to check
	 * @param steps
	 *            the number of steps through the tree (laterally) to check
	 * @return true if this {@link ConsCell} tree is equal to <tt>o</tt> for the given number of <tt>steps</tt>
	 */
	public boolean equals(ConsCell o, int steps) {
		ConsCell t = this;
		for (int i = 0; i < steps; i++) {
			if (t.carType != o.carType || (t.car == null && t.car != o.car) || (t.car instanceof ConsCell && o.car instanceof ConsCell && !t.equals((ConsCell) o.car, steps)) || !t.car.equals(o.car))
				return false;
			if (t.cdrType != o.cdrType || (t.cdr == null && t.cdr != o.cdr) || (t.cdr instanceof ConsCell && o.cdr instanceof ConsCell && !t.equals((ConsCell) o.cdr, steps)) || !t.cdr.equals(o.cdr))
				return false;
			t = t.getNextConsCell();
			o = o.getNextConsCell();
		}
		return true;
	}
	
	/**
	 * Replaces this {@link ConsCell} with the given {@link ConsCell} tree. If the length of the replacement {@link ConsCell} tree is
	 * greater than 1, it inserts the entire new tree after removing this {@link ConsCell}.
	 * 
	 * @param replacement
	 *            the {@link ConsCell} to replace this one with
	 * @return the end of the resulting {@link ConsCell} tree
	 */
	public ConsCell replace(ConsCell replacement) {
		ConsCell last = replacement.getLastConsCell();
		if (replacement.previous != null) {
			replacement.previous.cdr = null;
			replacement.previous.cdrType = EMPTY;
		}
		if (previous != null) {
			replacement.previous = previous;
			previous.cdr = replacement;
		}
		last.cdr = cdr;
		last.cdrType = cdrType;
		if (cdr instanceof ConsCell)
			((ConsCell) cdr).previous = last;
		return last.getLastConsCell();
	}
	
	/**
	 * Determines if the equation is at least as long as the given length
	 * 
	 * @param minimum
	 *            the minimum length
	 * @return true if {@link #length()} would return a number greater than or equal to minimum
	 */
	public boolean checkLength(int minimum) {
		if (isNull())
			return minimum <= 0;
		ConsCell token = this;
		int length = 1;
		while (token.cdrType == TOKEN && !(token = (ConsCell) token.cdr).isNull()) { //Effectively getNextConsCell() w/o generating a null token when called on the last token
			if (length >= minimum)
				return true;
			length++;
		}
		return length >= minimum;
	}
	
	/**
	 * Replaces the car and carType of this <tt>Token</tt> with the provided values
	 * 
	 * @param car
	 *            the new car
	 * @param carType
	 *            the new carType
	 */
	public void replaceCar(Object car, ConsType carType) {
		this.car = car;
		this.carType = carType;
	}
	
	/**
	 * Gets the car of the <tt>Token</tt> the specified number of cells ahead.<br>
	 * NOTE: this does NOT count <tt>ConsCells</tt> in the car position, it stays at the depth of the <tt>Token</tt> that it was
	 * initially called on.
	 * 
	 * @param numAhead
	 *            the number of <tt>ConsCells</tt> to look ahead by (a value of 0 is equivalent to {@link #getCar() getCar()})
	 * @return the car value of the <tt>Token</tt> the specified number of <tt>ConsCells</tt> ahead
	 * @see #getCar()
	 */
	public Object getCar(int numAhead) {
		ConsCell current = this;
		for (int i = 0; i < numAhead; i++)
			current = current.getNextConsCell();
		return current.getCar();
	}
	
	/**
	 * Gets the carType of the <tt>Token</tt> the specified number of cells ahead.<br>
	 * NOTE: this does NOT count <tt>ConsCells</tt> in the car position, it stays at the depth of the <tt>Token</tt> that it was
	 * initially called on.
	 * 
	 * @param numAhead
	 *            the number of <tt>ConsCells</tt> to look ahead by (a value of 0 is equivalent to {@link #getCarType()
	 *            getCarType()})
	 * @return the car value of the <tt>Token</tt> the specified number of <tt>ConsCells</tt> ahead
	 * @see #getCarType()
	 */
	public ConsType getCarType(int numAhead) {
		ConsCell current = this;
		for (int i = 0; i < numAhead; i++)
			current = current.getNextConsCell();
		return current.getCarType();
	}
	
	/**
	 * Gets the cdr of the <tt>Token</tt> the specified number of cells ahead.<br>
	 * NOTE: this does NOT count <tt>ConsCells</tt> in the car position, it stays at the depth of the <tt>Token</tt> that it was
	 * initially called on.
	 * 
	 * @param numAhead
	 *            the number of <tt>ConsCells</tt> to look ahead by (a value of 0 is equivalent to {@link #getCdr() getCdr()})
	 * @return the cdr value of the <tt>Token</tt> the specified number of <tt>ConsCells</tt> ahead
	 * @see #getCdr()
	 */
	public Object getCdr(int numAhead) {
		ConsCell current = this;
		for (int i = 0; i < numAhead; i++)
			current = current.getNextConsCell();
		return current.getCdr();
	}
	
	/**
	 * Gets the cdrType of the <tt>Token</tt> the specified number of cells ahead.<br>
	 * NOTE: this does NOT count <tt>ConsCells</tt> in the car position, it stays at the depth of the <tt>Token</tt> that it was
	 * initially called on.
	 * 
	 * @param numAhead
	 *            the number of <tt>ConsCells</tt> to look ahead by (a value of 0 is equivalent to {@link #getCdrType()
	 *            getCdrType()})
	 * @return the cdr value of the <tt>Token</tt> the specified number of <tt>ConsCells</tt> ahead
	 * @see #getCdrType()
	 */
	public ConsType getCdrType(int numAhead) {
		ConsCell current = this;
		for (int i = 0; i < numAhead; i++)
			current = current.getNextConsCell();
		return current.getCdrType();
	}
	
	/**
	 * Get the <tt>Token</tt> the specified number of <tt>Token</tt>s before this one, or a null <tt>Token</tt> if the
	 * specified number is beyond the bounds of this list
	 * 
	 * @param numBackwards
	 *            the number of cells to get before this one
	 * @return the specified previous <tt>Token</tt> or a null <tt>Token</tt> if it does not exist.
	 */
	public ConsCell getPreviousConsCell(int numBackwards) {
		ConsCell output = this;
		for (int i = 0; i < numBackwards && !output.isNull(); i++)
			output = output.getPreviousConsCell();
		return output;
	}
	
	/**
	 * Get the <tt>Token</tt> the specified number of <tt>Token</tt>s before this one, or a null <tt>Token</tt> if the
	 * specified number is beyond the bounds of this list
	 * 
	 * @param numForward
	 *            the number of cells to get after this one
	 * @return the specified previous <tt>Token</tt> or a null <tt>Token</tt> if it does not exist.
	 */
	public ConsCell getNextConsCell(int numForward) {
		ConsCell output = this;
		for (int i = 0; i < numForward && !output.isNull(); i++)
			output = output.getNextConsCell();
		return output;
	}
	
	/**
	 * @return true if this {@link ConsCell}'s cdr is an instance of {@link ConsCell}
	 */
	public boolean hasNextConsCell() {
		return cdr instanceof ConsCell;
	}
	
	/**
	 * @return true if this {@link ConsCell} has a previous {@link ConsCell}
	 */
	public boolean hasPreviousConsCell() {
		return previous != null;
	}
	
	/**
	 * Splits this <tt>Token</tt> <tt>tree</tt> on <tt>ConsCells</tt> containing <tt>identifier</tt>.<br>
	 * This function is <i>not</i> recursive - that is to say that it will check sub-trees for the given <tt>identifier</tt>
	 * 
	 * @param identifier
	 *            the identifier to split this <tt>Token</tt> <tt>tree</tt> on
	 * @return a {@link List}<tt>{@literal <Token>}</tt> of the sections of the <tt>Token</tt>
	 *         <tt>tree</tt>, excluding the <tt>ConsCells</tt> that contained the identifier.
	 */
	public List<ConsCell> splitOnIdentifier(String identifier) {
		List<ConsCell> output = new ArrayList<>();
		if (isNull())
			return output;
		ConsCell current = this, head = new ConsCell(), top = head;
		do {
			if (current.carType == IDENTIFIER && ((String) current.car).equals(identifier)) {
				output.add(top);
				top = head = new ConsCell();
				continue;
			}
			head = head.append(current.singular());
		} while (!((current = current.getNextConsCell()).isNull())); //This steps current forward while checking for nulls
		if (!top.isNull())
			output.add(top);
		return output;
	}
	
	/**
	 * Checks for <tt>identifier</tt> in this <tt>Token</tt> tree, including any sub-trees (e.g. a <tt>Token</tt> in the car
	 * position).
	 * 
	 * @param identifier
	 *            the identifier to check for as a <tt>String</tt>
	 * @return true if this <tt>Token</tt> tree contains a <tt>Token</tt> of type {@link ConsType#IDENTIFIER Identifier} and
	 *         a value equal to <tt>identifier</tt> otherwise false.
	 */
	public boolean containsIdentifier(String identifier) {
		ConsCell current = this;
		do {
			if ((current.carType == IDENTIFIER && ((String) current.car).equals(identifier)) || (current.carType == PARENTHESES && ((ConsCell) current.car).containsIdentifier(identifier)))
				return true;
		} while (!(current = current.getNextConsCell()).isNull());
		return false;
	}
	
	/**
	 * Splits the current level of the {@link ConsCell} tree on separators. If no separators are specified, it splits on all
	 * {@link ConsCell ConsCells} that have a car value of type {@link ConsType#SEPARATOR}.
	 * 
	 * @param separators
	 *            the subset of separators on which to split this {@link ConsCell} tree.
	 * @return the subtrees as an {@link List}
	 */
	public List<ConsCell> splitOnSeparator(String... separators) {
		List<ConsCell> output = new ArrayList<>();
		if (isNull())
			return output;
		ConsCell current = this;
		ConsCell head = new ConsCell();
		do {
			if (current.carType == SEPARATOR) {
				boolean selected = separators.length == 0;
				for (String separator : separators)
					if (separator.equals(current.getCar())) {
						selected = true;
						break;
					}
				if (selected) {
					output.add(head.getFirstConsCell());
					head = new ConsCell();
					continue;
				}
			}
			head = head.append(current.singular());
		} while (!((current = current.getNextConsCell()).isNull())); //This steps current forward while checking for nulls
		head = head.getFirstConsCell();
		if (!head.isNull() && head.length() > 0)
			output.add(head);
		return output;
	}
	
	@Override
	public String toJSONString() {
		return ((car != null ? carType.valueToJSONString(car) + " " : "") + (cdr != null ? cdrType.valueToJSONString(cdr) + " " : "")).trim();
	}
	
	public boolean containsType(ConsType type) {
		for (ConsCell t : this)
			if (t.getCarType() == type || (t.getCar() instanceof ConsCell && ((ConsCell) t.getCar()).containsType(type)))
				return true;
		return false;
	}
	
	public List<ConsCell> allInstancesOf(Object value, ConsType type) {
		List<ConsCell> out = new ArrayList<>();
		for (ConsCell cell = this; !cell.isNull(); cell = cell.getNextConsCell())
			if (cell.getCarType() == type) {
				if (cell.getCar().equals(value))
					out.add(cell);
			}
			else if (cell.getCar() instanceof ConsCell)
				out.addAll(((ConsCell) cell.getCar()).allInstancesOf(value, type));
		return out;
	}
	
	public Set<String> getVariables() {
		Set<String> out = new LinkedHashSet<>();
		for (ConsCell cell = this; !cell.isNull(); cell = cell.getNextConsCell())
			if (cell.getCarType() == ConsType.IDENTIFIER)
				out.add((String) cell.getCar());
			else if (cell.getCar() instanceof ConsCell)
				out.addAll(((ConsCell) cell.getCar()).getVariables());
		return out;
	}
	
	/**
	 * Replaces all instances of <tt>original</tt> with <tt>replacement</tt>. This does <i>not</i> modify the original
	 * {@link ConsCell} tree.
	 * 
	 * @param original
	 *            a {@link ConsCell} tree of any length greater than 0
	 * @param replacement
	 *            a {@link ConsCell} tree of any length (anything that isn't a null pointer itself)
	 * @return a copy of this {@link ConsCell} tree with every instance of <tt>original</tt> replaced
	 */
	public ConsCell replaceAll(ConsCell original, ConsCell replacement) {
		if (original.hasNextConsCell())
			return replaceAllGeneral(original, replacement);
		ConsCell output = new ConsCell(), head = output;
		for (ConsCell t : this)
			if (t.equals(original))
				head = head.append(replacement.clone());
			else
				head = head.append(t.getCar() instanceof ConsCell ? ((ConsCell) t.getCar()).replaceAll(original, replacement) : t);
		return output;
	}
	
	private ConsCell replaceAllGeneral(ConsCell original, ConsCell replacement) {
		ConsCell output = new ConsCell(), head = output;
		int steps = original.length();
		for (ConsCell t = this; !t.isNull();) {
			if (t.equals(original, steps)) {
				head = head.append(replacement.clone());
				t = t.getNextConsCell(steps);
			}
			else {
				head = head.append(t.getCar() instanceof ConsCell ? ((ConsCell) t.getCar()).replaceAll(original, replacement) : t.singular());
				t = t.getNextConsCell();
			}
		}
		return output;
	}
}
