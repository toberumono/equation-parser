package lipstone.joshua.parser.tokenizer;

import java.util.regex.MatchResult;
import java.util.regex.Pattern;

import toberumono.lexer.genericBase.DescenderAction;
import toberumono.lexer.genericBase.DescenderOpenAction;
import toberumono.lexer.genericBase.GenericAction;
import toberumono.lexer.genericBase.GenericDescender;

import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.plugin.pluggable.Pluggable;

/**
 * Represents a descender for the {@link Tokenizer} that this parsing library uses.
 * 
 * @author Joshua Lipstone
 */
public class TokenizerDescender extends GenericDescender<ConsCell, ConsType, TokenizerRule, TokenizerDescender, Tokenizer> implements Pluggable {
	private final ParserPlugin creator;
	private final String name;
	
	/**
	 * Constructs a new {@link TokenizerDescender} with the given data.
	 * 
	 * @param name
	 *            the name of this {@link TokenizerDescender}
	 * @param open
	 *            the symbol that marks the opening point for this {@link TokenizerDescender}
	 * @param close
	 *            the symbol that marks the closing point for this {@link TokenizerDescender}
	 * @param openAction
	 *            the action to take upon seeing the open symbol - this does runs <i>before</i> the inner input is tokenized
	 * @param closeAction
	 *            the action to take upon the tokenized input
	 * @param creator
	 *            the {@link lipstone.joshua.parser.ParserPlugin ParserPlugin} that created this {@link TokenizerDescender}
	 */
	public TokenizerDescender(String name, Pattern open, Pattern close, DescenderOpenAction<ConsCell, ConsType, TokenizerRule, TokenizerDescender, Tokenizer, MatchResult> openAction,
			GenericAction<ConsCell, ConsType, TokenizerRule, TokenizerDescender, Tokenizer, ConsCell> closeAction, ParserPlugin creator) {
		super(open, close, openAction, closeAction);
		this.creator = creator;
		this.name = name;
	}
	
	/**
	 * This is a convenience constructor for
	 * {@link #TokenizerDescender(String, String, String, DescenderAction, GenericAction, ParserPlugin)} for descenders that
	 * will only apply an action after the matching has occurred.
	 * 
	 * @param name
	 *            the name of this {@link TokenizerDescender}
	 * @param open
	 *            the symbol that marks the opening point for this {@link TokenizerDescender}
	 * @param close
	 *            the symbol that marks the closing point for this {@link TokenizerDescender}
	 * @param closeAction
	 *            the action to take upon the tokenized input
	 * @param creator
	 *            the {@link lipstone.joshua.parser.ParserPlugin ParserPlugin} that created this {@link TokenizerDescender}
	 */
	public TokenizerDescender(String name, Pattern open, Pattern close, GenericAction<ConsCell, ConsType, TokenizerRule, TokenizerDescender, Tokenizer, ConsCell> closeAction, ParserPlugin creator) {
		this(name, open, close, (tokenizer, state, match) -> {} , closeAction, creator);
	}
	
	/**
	 * Constructs a new {@link TokenizerDescender} with the given data.
	 * 
	 * @param name
	 *            the name of this {@link TokenizerDescender}
	 * @param open
	 *            the symbol that marks the opening point for this {@link TokenizerDescender}
	 * @param close
	 *            the symbol that marks the closing point for this {@link TokenizerDescender}
	 * @param openAction
	 *            the action to take upon seeing the open symbol - this does runs <i>before</i> the inner input is tokenized
	 * @param closeAction
	 *            the action to take upon the tokenized input
	 * @param creator
	 *            the {@link lipstone.joshua.parser.ParserPlugin ParserPlugin} that created this {@link TokenizerDescender}
	 */
	@Deprecated
	public TokenizerDescender(String name, String open, String close, DescenderAction<Tokenizer> openAction,
			GenericAction<ConsCell, ConsType, TokenizerRule, TokenizerDescender, Tokenizer, ConsCell> closeAction, ParserPlugin creator) {
		super(open, close, openAction, closeAction);
		this.creator = creator;
		this.name = name;
	}
	
	/**
	 * This is a convenience constructor for
	 * {@link #TokenizerDescender(String, String, String, DescenderAction, GenericAction, ParserPlugin)} for descenders that
	 * will only apply an action after the matching has occurred.
	 * 
	 * @param name
	 *            the name of this {@link TokenizerDescender}
	 * @param open
	 *            the symbol that marks the opening point for this {@link TokenizerDescender}
	 * @param close
	 *            the symbol that marks the closing point for this {@link TokenizerDescender}
	 * @param closeAction
	 *            the action to take upon the tokenized input
	 * @param creator
	 *            the {@link lipstone.joshua.parser.ParserPlugin ParserPlugin} that created this {@link TokenizerDescender}
	 */
	@Deprecated
	public TokenizerDescender(String name, String open, String close, GenericAction<ConsCell, ConsType, TokenizerRule, TokenizerDescender, Tokenizer, ConsCell> closeAction, ParserPlugin creator) {
		this(name, open, close, (tokenizer, state) -> {} , closeAction, creator);
	}
	
	/**
	 * @return the {@link lipstone.joshua.parser.ParserPlugin plugin} that created this {@link TokenizerDescender}
	 */
	@Override
	public ParserPlugin getPlugin() {
		return creator;
	}
	
	/**
	 * @return the name of this {@link TokenizerDescender}
	 */
	@Override
	public String getName() {
		return name;
	}
	
}
