package lipstone.joshua.parser.tokenizer;

import toberumono.json.JSONSerializable;
import toberumono.structures.sexpressions.generic.GenericConsType;

public enum ConsType implements GenericConsType {
	NUMBER("Number"),
	BOOLEAN("Boolean"),
	IDENTIFIER("Identifier"),
	STRING("String") {
		@Override
		public String valueToString(Object value) {
			return "\"" + ((String) value) + "\"";
		}
	},
	SEPARATOR("Separator"),
	OPERATOR("Operator"),
	OPERATION("Operation"),
	COMMAND("Command"),
	KEYWORD("Keyword"),
	TAG("Tag"),
	OBJECT("Object"),
	EXCEPTION("Exception"),
	/**
	 * Special type reserved for data that should not be touched until the final result is displayed
	 */
	RETURN("Return") {
		@Override
		public String valueToJSONString(Object value) {
			return "$Return(" + (value instanceof JSONSerializable ? ((JSONSerializable) value).toJSONString() : value.toString()) + ")";
		}
	},
	MATRIX("Matrix"),
	/**
	 * Special type reserved for tokens relating to CAS functions
	 */
	CAS("CAS"),
	ARRAY("Array", "[", "]"),
	PARENTHESES("Parentheses", "(", ")"),
	/**
	 * Indicates that the value is an instance of {@link ConsCell}
	 */
	TOKEN("Token") {
		
		@Override
		public ConsCell cloneValue(Object value) {
			return ((ConsCell) value).clone();
		}
	},
	/**
	 * Indicates that the value is null
	 */
	EMPTY("Empty") {
		@Override
		public String valueToString(Object value) {
			return "";
		}
		
		@Override
		public int compareValues(Object value1, Object value2) {
			return 0;
		}
	};
	
	private final String name, open, close;
	
	ConsType(String name) {
		this.name = name;
		open = close = null;
	}
	
	ConsType(String name, String open, String close) {
		this.name = name;
		this.open = open;
		this.close = close;
	}
	
	@Override
	public String getOpen() {
		return open;
	}
	
	@Override
	public String getClose() {
		return close;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	@Override
	public boolean marksDescender() {
		return open != null;
	}
	
	/**
	 * Converts the passed value to a JSON String. By default this forwards to {@link #valueToString(Object)}.
	 * 
	 * @param value
	 *            the value to convert to a JSON String
	 * @return the value as a JSON String.
	 */
	public String valueToJSONString(Object value) {
		return valueToString(value);
	}
}
