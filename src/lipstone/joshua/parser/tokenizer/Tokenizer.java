package lipstone.joshua.parser.tokenizer;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

import toberumono.lexer.errors.LexerException;
import toberumono.lexer.genericBase.GenericLexer;
import toberumono.lexer.genericBase.LexerState;
import toberumono.lexer.util.DefaultIgnorePatterns;
import toberumono.structures.tuples.Pair;

import lipstone.joshua.parser.Parser;
import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.exceptions.SyntaxException;
import lipstone.joshua.parser.plugin.pluggable.Action;
import lipstone.joshua.parser.plugin.pluggable.Operator;
import lipstone.joshua.parser.types.BigDec;
import lipstone.joshua.parser.types.Matrix;

import static lipstone.joshua.parser.tokenizer.ConsType.*;

public class Tokenizer extends GenericLexer<ConsCell, ConsType, TokenizerRule, TokenizerDescender, Tokenizer> {
	private static final String numberPattern = "(([0-9]+(\\.[0-9]*)?|\\.[0-9]+)(\\*?i)?|i|infinity)", identifierPattern = "([a-z_A-Z]+|[\\.$%&:;<>?~_\\|])",
			stringPattern = "\"(([^\"]|(?<=\\\\)\")*)\"";
	private int returning = 0;
	private static final OperatorChecker skipCheck = (tokenizer, state, parser, first) -> first;
	private static final OperatorChecker operatorChecker = (tokenizer, state, parser, first) -> {
		if (!state.hasNext())
			return first;
		ConsCell next = tokenizer.getNextConsCell(state, true);
		if (next.getCarType() != OPERATOR && next.getCarType() != TAG && next.getCarType() != CAS && next.getCarType() != SEPARATOR)
			first.append(new ConsCell(next.getCarType() == STRING ? parser.operators.get("+") : parser.operators.get("*"), OPERATOR, next));
		else
			first.append(next);
		return first;
	};
	private static final OperatorHandler skipHandle = (tokenizer, state, parser, operator) -> new ConsCell(operator, OPERATOR);
	private static final OperatorHandler operatorHandler = (tokenizer, state, parser, operator) -> {
		if (!state.hasNext())
			return new ConsCell(operator, OPERATOR);
		ConsCell next = tokenizer.getNextConsCell(state, true);
		if (next.getCarType() == OPERATOR) { //Basic simplifications go in here
			Operator nxt = (Operator) next.getCar();
			if (!operator.equals(nxt) && ((operator.equals("+") && nxt.equals("-")) || (operator.equals("-") && nxt.equals("+")))) //+- and -+ become -
				operator = parser.operators.get("-");
			else if (operator.equals(nxt) && (operator.equals("+") || operator.equals("-"))) //++ and -- become +
				operator = parser.operators.get("+");
			else
				return new ConsCell(operator, OPERATOR, next);
		}
		else
			return new ConsCell(operator, OPERATOR, next);
		return next.hasNextConsCell() ? new ConsCell(operator, OPERATOR, next.getNextConsCell()) : new ConsCell(operator, OPERATOR);
	};
	private OperatorChecker checker = operatorChecker;
	private OperatorHandler handler = operatorHandler;
	private boolean linkedMaps = false;
	private final Map<String, TokenizerRule> rules;
	private final Map<String, TokenizerDescender> descenders;
	
	public Tokenizer(Parser parser, ParserPlugin internalPlugin) {
		this(new LinkedHashMap<>(), new LinkedHashMap<>());
		initRules(parser, internalPlugin);
	}
	
	/**
	 * This allows the Tokenizer class to keep a point to the modifiable Maps that the Lexer uses.
	 * 
	 * @param rules
	 *            the rules map
	 * @param descenders
	 *            the descenders map
	 */
	private Tokenizer(Map<String, TokenizerRule> rules, Map<String, TokenizerDescender> descenders) {
		super(rules, descenders, new LinkedHashMap<>(), new LinkedHashMap<>(), ConsCell::new, EMPTY, DefaultIgnorePatterns.WHITESPACE);
		this.rules = rules;
		this.descenders = descenders;
	}
	
	public final void initRules(Parser parser, ParserPlugin internalPlugin) {
		addRule(new TokenizerRule("Number", Pattern.compile(numberPattern),
				(tokenizer, state, match) -> checkNextForOperator(tokenizer, state, parser, new ConsCell(new BigDec(match.group()), NUMBER)),
				internalPlugin));
		addRule(new TokenizerRule("Boolean", Pattern.compile("(true|false)"), (tokenizer, state, match) -> new ConsCell(match.group().charAt(0) == 't', BOOLEAN), internalPlugin));
		addRule(new TokenizerRule("CAS", Pattern.compile("="), (tokenizer, state, match) -> new ConsCell("=", CAS), internalPlugin));
		//Original pattern: "(['a-zA-Z_$%#&:;<=>?~\\|][a-zA-Z_\\.$%&:;<=>?~_\\|]*|(!)+)"
		addRule(new TokenizerRule("Identifier", Pattern.compile(identifierPattern), (tokenizer, state, match) -> {
			Action action = null;
			String ident = match.group();
			if ((action = parser.operations.get(ident)) != null)
				return new ConsCell(action, OPERATION);
			if ((action = parser.commands.get(ident)) != null)
				return new ConsCell(action, COMMAND);
			if ((action = parser.keywords.get(ident)) != null)
				return new ConsCell(action, KEYWORD);
			Operator op = parser.operators.get(ident);
			if (op != null)
				return operatorHandler(tokenizer, state, parser, op);
			return checkNextForOperator(tokenizer, state, parser, new ConsCell(ident, IDENTIFIER));
		} , internalPlugin));
		addRule(new TokenizerRule("String", Pattern.compile(stringPattern), (tokenizer, state, match) -> checkNextForOperator(tokenizer, state, parser, new ConsCell(match.group(1), STRING)),
				internalPlugin));
		addRule(new TokenizerRule("Separator", Pattern.compile("(,|\r?\n)"), (tokenizer, state, match) -> new ConsCell(match.group(), SEPARATOR), internalPlugin));
		addRule(new TokenizerRule("BasicOperators", Pattern.compile("[\\Q+-*/%^\\E]"), (tokenizer, state, match) -> operatorHandler(tokenizer, state, parser, parser.operators.get(match.group())),
				internalPlugin));
		addRule(new TokenizerRule("Tags", Pattern.compile("\\$(\\{([0-9]+)\\})?(" + numberPattern + "|" + identifierPattern + "|" + stringPattern + ")"), (tokenizer, state, match) -> {
			//String tag = match.group(4) != null ? match.group(4) : match.group(3); //Removes the "'s from around string-based tag names.
			if (!state.hasNext())
				throw new LexerException(new SyntaxException("A tag indicator ($) must be followed by a valid string, identifier, or number", parser.getLastPlugin()));
			ConsCell next = tokenizer.getNextConsCell(state, true);
			if (next.getCarType() != STRING && next.getCarType() != IDENTIFIER && next.getCarType() != NUMBER)
				throw new LexerException(new SyntaxException("A tag indicator ($) must be followed by a valid string, identifier, or number", parser.getLastPlugin()));
			return new ConsCell(next.getCar().toString(), TAG);
		} , internalPlugin));
		addDescender(new TokenizerDescender("Return", Pattern.compile("$Return(", Pattern.LITERAL), Pattern.compile(")", Pattern.LITERAL), (tokenizer, state, match) -> {
			returning++;
			checker = skipCheck;
			handler = skipHandle;
		} , (tokenizer, state, match) -> {
			returning--;
			if (returning == 0) {
				checker = operatorChecker;
				handler = operatorHandler;
			}
			return new ConsCell(match, RETURN);
		} , internalPlugin));
		addDescender(new TokenizerDescender("Parentheses", Pattern.compile("(", Pattern.LITERAL), Pattern.compile(")", Pattern.LITERAL),
				(tokenizer, state, match) -> checkNextForOperator(tokenizer, state, parser, new ConsCell(match, PARENTHESES)), internalPlugin));
		addDescender(new TokenizerDescender("Arrays", Pattern.compile("[", Pattern.LITERAL), Pattern.compile("]", Pattern.LITERAL),
				(tokenizer, state, match) -> checkNextForOperator(tokenizer, state, parser, new ConsCell(match.splitOnSeparator(), ARRAY)), internalPlugin));
		//This rule returns an object based on the tag preceding it or, if there wasn't a tag, it returns a matrix
		addDescender(new TokenizerDescender("Objects", Pattern.compile("{", Pattern.LITERAL), Pattern.compile("}", Pattern.LITERAL), (tokenizer, state, match) -> {
			//TODO implement tag handling.  Will probably require making a much more complex parsing algorithm.
			/*if (tokenizer.getPreviousToken().getCarType() == TAG) {
				String tag = tokenizer.popPreviousToken().getCar().toString(); //Remove the previous token, and store the tag type
				ParserObjectConstructor constructor = parser.getParserObjectConstructor(tag);
				if (constructor == null)
					throw new SyntaxException("\"" + tag + "\" is not a valid object type.", parser.getLastPlugin());
				return Tokenizer.checkNextForOperator(tokenizer, parser, new Token(constructor.construct(match, parser), OBJECT));
			}*/
			try {
				return checkNextForOperator(tokenizer, state, parser, new ConsCell(new Matrix(match, parser), MATRIX));
			}
			catch (Exception e) {
				throw new LexerException(e);
			}
		} , internalPlugin));
	}
	
	/**
	 * Adds a new {@link TokenizerRule rule} to this {@link Tokenizer}
	 * 
	 * @param rule
	 *            the {@link TokenizerRule rule} to add
	 */
	public void addRule(TokenizerRule rule) {
		addRule(rule.getName(), rule);
	}
	
	@Override
	public void addRule(String name, TokenizerRule rule) {
		if (linkedMaps)
			throw new UnsupportedOperationException("Cannot directly add rules to a Tokenizer with linked maps.");
		super.addRule(name, rule);
	}
	
	/**
	 * Adds a new {@link TokenizerDescender descender} to this {@link Tokenizer}
	 * 
	 * @param descender
	 *            the {@link TokenizerDescender descender} to add
	 */
	public void addDescender(TokenizerDescender descender) {
		addDescender(descender.getName(), descender);
	}
	
	@Override
	public void addDescender(String name, TokenizerDescender descender) {
		if (linkedMaps)
			throw new UnsupportedOperationException("Cannot directly add descenders to a Tokenizer with linked maps.");
		super.addDescender(descender.getName(), descender);
	}
	
	public Pair<Map<String, TokenizerRule>, Map<String, TokenizerDescender>> getMaps() {
		if (linkedMaps)
			throw new UnsupportedOperationException("The maps in this Tokenizer have already been linked, and cannot be linked to multiple locations.");
		linkedMaps = true;
		return new Pair<>(rules, descenders);
	}
	
	@FunctionalInterface
	static interface OperatorChecker {
		public ConsCell check(Tokenizer tokenizer, LexerState<ConsCell, ConsType, TokenizerRule, TokenizerDescender, Tokenizer> state, Parser parser, ConsCell first) throws LexerException;
	}
	
	@FunctionalInterface
	static interface OperatorHandler {
		public ConsCell handle(Tokenizer tokenizer, LexerState<ConsCell, ConsType, TokenizerRule, TokenizerDescender, Tokenizer> state, Parser parser, Operator operator) throws LexerException;
	}
	
	/**
	 * This function can ONLY be called from within {@link TokenizerRule rules}. Inserts an
	 * {@link lipstone.joshua.parser.plugin.pluggable.Operator Operator} between the provided {@link ConsCell} and the next
	 * one if necessary.
	 * 
	 * @param tokenizer
	 *            the {@link Tokenizer} who's {@link TokenizerRule} called this function
	 * @param state
	 *            the state of the {@link Tokenizer} from which this function was called
	 * @param parser
	 *            the {@link lipstone.joshua.parser.Parser Parser} that precipitated this method call
	 * @param first
	 *            the result from the most recently called {@link TokenizerRule}
	 * @return the provided {@link ConsCell} plus the next {@link ConsCell} with an
	 *         {@link lipstone.joshua.parser.plugin.pluggable.Operator Operator} if necessary
	 * @throws LexerException
	 *             for exception propagation purposes
	 */
	public ConsCell checkNextForOperator(Tokenizer tokenizer, LexerState<ConsCell, ConsType, TokenizerRule, TokenizerDescender, Tokenizer> state, Parser parser, ConsCell first) throws LexerException {
		return checker.check(tokenizer, state, parser, first);
	}
	
	/**
	 * This function can ONLY be called from within {@link TokenizerRule rules}
	 * 
	 * @param tokenizer
	 *            the {@link Tokenizer} who's {@link TokenizerRule} called this function
	 * @param state
	 *            the state of the {@link Tokenizer} from which this function was called
	 * @param parser
	 *            the {@link lipstone.joshua.parser.Parser Parser} that precipitated this method call
	 * @param operator
	 *            the {@link lipstone.joshua.parser.plugin.pluggable.Operator Operator} that was most recently matched
	 * @return a {@link ConsCell} containing the appropriate {@link lipstone.joshua.parser.plugin.pluggable.Operator
	 *         Operator}
	 * @throws LexerException
	 *             for exception propagation purposes
	 */
	public ConsCell operatorHandler(Tokenizer tokenizer, LexerState<ConsCell, ConsType, TokenizerRule, TokenizerDescender, Tokenizer> state, Parser parser, Operator operator) throws LexerException {
		return handler.handle(tokenizer, state, parser, operator);
	}
}
