package lipstone.joshua.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.UnaryOperator;

import lipstone.joshua.parser.cas.Solution;
import lipstone.joshua.parser.cas.Solver;
import lipstone.joshua.parser.cas.Term;
import lipstone.joshua.parser.exceptions.PluginConflictException;
import lipstone.joshua.parser.plugin.pluggable.AngleType;
import lipstone.joshua.parser.plugin.pluggable.Operator;
import lipstone.joshua.parser.tokenizer.ConsCell;
import lipstone.joshua.parser.types.BigDec;
import lipstone.joshua.pluginLoader.Plugin;

import static lipstone.joshua.parser.tokenizer.ConsType.*;
import static lipstone.joshua.parser.types.BigDec.*;

@Plugin(author = "Joshua Lipstone", description = "This plugin is used for prioritized loading of internal plugin-based components.", id = "InternalPlugin", version = "1.2")
class InternalPlugin extends ParserPlugin {
	@Override
	protected void loadSettings(Parser parser) {}
	
	@Override
	protected void load(Parser parser) throws PluginConflictException {
		//Add the basic operators
		setUpOperators(parser);
		
		//Add the basic angle types
		setUpAngleTypes(parser);
		
		//Add the basic CAS solvers
		setUpCASSolvers(parser);
	}
	
	private void setUpOperators(Parser parser) throws PluginConflictException {
		ConsCell one = new ConsCell(ONE, NUMBER);
		parser.operators.add(new Operator("%", this, Operator.BASE_PRECEDENCE + 3, one, false,
				(left, right) -> (left.getCarType() == NUMBER && right.getCarType() == NUMBER),
				(left, right) -> new ConsCell(((BigDec) left.getCar()).mod((BigDec) right.getCar()), NUMBER)));
		
		parser.operators.add(new Operator("^", this, Operator.BASE_PRECEDENCE + 2, one, true,
				(left, right) -> (left.getCarType() == NUMBER && right.getCarType() == NUMBER),
				(left, right) -> new ConsCell(((BigDec) left.getCar()).pow((BigDec) right.getCar()), NUMBER)));
		
		parser.operators.add(new Operator("*", this, Operator.BASE_PRECEDENCE + 1, one, false,
				(left, right) -> (left.getCarType() == NUMBER && right.getCarType() == NUMBER),
				(left, right) -> new ConsCell(((BigDec) left.getCar()).multiply((BigDec) right.getCar()), NUMBER)));
		
		parser.operators.add(new Operator("/", this, Operator.BASE_PRECEDENCE + 1, one, false,
				(left, right) -> (left.getCarType() == NUMBER && right.getCarType() == NUMBER),
				(left, right) -> new ConsCell(((BigDec) left.getCar()).divide((BigDec) right.getCar()), NUMBER)));
		
		parser.operators.add(new Operator("+", this, Operator.BASE_PRECEDENCE, new ConsCell(ZERO, NUMBER), false,
				(left, right) -> (left.getCarType() == NUMBER && right.getCarType() == NUMBER),
				(left, right) -> new ConsCell(((BigDec) left.getCar()).add((BigDec) right.getCar()), NUMBER)));
		
		parser.operators.add(new Operator("-", this, Operator.BASE_PRECEDENCE, new ConsCell(ZERO, NUMBER), false,
				(left, right) -> (left.getCarType() == NUMBER && right.getCarType() == NUMBER),
				(left, right) -> new ConsCell(((BigDec) left.getCar()).subtract((BigDec) right.getCar()), NUMBER)));
	}
	
	private void setUpAngleTypes(Parser parser) throws PluginConflictException {
		BigDec factor180 = new BigDec(180), factor200 = new BigDec(200);
		parser.angleTypes.add(new AngleType("Degrees", this, angle -> angle.multiply(PI).divide(factor180), angle -> angle.multiply(factor180).divide(PI)));
		parser.angleTypes.add(new AngleType("Radians", this, angle -> angle, angle -> angle));
		parser.angleTypes.add(new AngleType("Grads", this, angle -> angle.multiply(PI).divide(factor200), angle -> angle.multiply(factor200).divide(PI)));
		
		ConsCell angle = getData("angle");
		if (angle == null || angle.getCar() == AngleType.NONE)
			putData("angle", angle = new ConsCell("Degrees", IDENTIFIER));
		parser.activeAngleType.set(parser.angleTypes.get(angle.toString()));
		parser.activeAngleType.addListener((observable, oldType, newType) -> putData("angle", new ConsCell(newType.getName(), IDENTIFIER)));
	}
	
	private void setUpCASSolvers(Parser parser) throws PluginConflictException {
		BiFunction<ConsCell, BigDec, ConsCell> tf = (input, co) -> input.append(new ConsCell(parser.operators.get("+"), OPERATOR, new ConsCell(co, NUMBER), TOKEN));
		UnaryOperator<ConsCell> to = input -> input.getCar() == null ? new ConsCell(ONE, NUMBER) : input.getNextConsCell();
		parser.casSolvers.add(new Solver(p -> (p.equation.size() == 2 && p.variables.size() > 0),
				p -> {
					ConsCell var = p.variables.get(0);
					List<Solution> out = new ArrayList<>();
					//2 cases: 1 term has the variable or both terms have the variable
					Term combined = p.equation.get(0).divide(p.equation.get(1).negate(), parser); //The negate effectively puts the second term on the other side of the equation
					ConsCell solution = new ConsCell(new ConsCell(ONE, NUMBER, new ConsCell(parser.operators.get("/"), OPERATOR,
							new ConsCell(combined.toConsCellTree(parser.operators.get("*"), parser.operators.get("^"), var.clone()), PARENTHESES), TOKEN), TOKEN), PARENTHESES,
							new ConsCell(parser.operators.get("^"), OPERATOR, new ConsCell(new ConsCell(ONE, NUMBER, new ConsCell(parser.operators.get("/"), OPERATOR,
									new ConsCell(combined.variables.get(var).clone(), PARENTHESES))), PARENTHESES)));
					out.add(new Solution(var, parser.evaluate(solution)));
					return out;
				}, "TwoTerm", this));
		parser.casSolvers.add(new Solver(p -> (p.equation.size() == 3 && p.variables.size() > 0),
				p -> {
					ConsCell var = p.variables.get(0), one = new ConsCell(ONE, NUMBER);
					List<Solution> out = new ArrayList<>();
					ConsCell a = new ConsCell(), ah = a, b = new ConsCell(), bh = b, c = new ConsCell(), ch = c;
					for (Term t : p.equation)
						if (!t.variables.containsKey(var))
							ch = tf.apply(ch, t.coefficient);
						else if (t.variables.get(var).equals(one))
							bh = tf.apply(bh, t.coefficient);
						else
							ah = tf.apply(ah, t.coefficient);
					a = parser.evaluate(to.apply(a));
					b = parser.evaluate(to.apply(b));
					c = parser.evaluate(to.apply(c));
					Operator times = parser.operators.get("*");
					//(-(b)+((b)^2-4(a)(c))^(1/2))/(2*(a))
					ConsCell solution = new ConsCell(new ConsCell(MINUSONE, NUMBER, new ConsCell(times, OPERATOR, new ConsCell(b.clone(), PARENTHESES, new ConsCell(parser.operators.get("+"), OPERATOR,
							new ConsCell(new ConsCell(b, PARENTHESES, new ConsCell(parser.operators.get("^"), OPERATOR, new ConsCell(new BigDec(2), NUMBER, new ConsCell(parser.operators.get("-"), OPERATOR,
									new ConsCell(new BigDec(4), NUMBER, new ConsCell(times, OPERATOR, new ConsCell(a.clone(), PARENTHESES, new ConsCell(times, OPERATOR, new ConsCell(c, PARENTHESES))))))))), PARENTHESES,
									new ConsCell(parser.operators.get("^"), OPERATOR, new ConsCell(new BigDec(0.5), NUMBER))))))), PARENTHESES, new ConsCell(parser.operators.get("/"), OPERATOR,
							new ConsCell(new ConsCell(new BigDec(2), NUMBER, new ConsCell(times, OPERATOR, new ConsCell(a, PARENTHESES))), PARENTHESES)));
					out.add(new Solution(var, parser.evaluate(solution)));
					//Changes the equation to (-(b)-((b)^2-4(a)(c))^(1/2))/(2*(a))
					((ConsCell) solution.getCar()).getNextConsCell().getNextConsCell().getNextConsCell().replaceCar(parser.operators.get("-"), OPERATOR);
					out.add(new Solution(var, parser.evaluate(solution)));
					return out;
				}, "Quadratic", this));
	}
}
