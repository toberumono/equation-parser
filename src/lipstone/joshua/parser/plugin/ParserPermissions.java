package lipstone.joshua.parser.plugin;

import java.util.HashMap;

import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.exceptions.PluginConflictException;
import lipstone.joshua.parser.plugin.pluggable.Pluggable;

/**
 * This represents a set of methods that can be used to validate {@link ParserPlugin plugin} actions.<br>
 * This class cannot be initialized other than through the provided static methods.
 * 
 * @author Joshua Lipstone
 */
public abstract class ParserPermissions {
	private final ParserPlugin plugin;
	protected final HashMap<ParserPlugin, ParserPermissions> loaded = new HashMap<>();
	private final ParserPermissions superPermissions;
	
	private ParserPermissions(ParserPlugin plugin, ParserPermissions superPermissions) {
		this.plugin = plugin;
		loaded.put(plugin, this);
		this.superPermissions = superPermissions;
	}
	
	public abstract boolean validateRead(String action, Pluggable target) throws PluginConflictException;
	
	public abstract boolean validateModify(String action, Pluggable target) throws PluginConflictException;
	
	public abstract boolean validateSpecial(String action, Object target) throws PluginConflictException;
	
	public void loadPlugin(ParserPlugin plugin, ParserPermissions permissions) throws PluginConflictException {
		if (!validateModify("load", plugin))
			throw new PluginConflictException(this.plugin.getName() + " does not have permission to load " + plugin.getName() + ".", this.plugin);
		loaded.put(plugin, permissions);
	}
	
	public void unloadPlugin(ParserPlugin plugin) throws PluginConflictException {
		try {
			validateModify("unload", plugin);
		}
		catch (PluginConflictException e) {
			PluginConflictException pce = new PluginConflictException(this.plugin.getName() + " does not have permission to unload " + plugin.getName() + ".", this.plugin);
			pce.setStackTrace(e.getStackTrace());
			throw pce;
		}
		if (superPermissions != null)
			superPermissions.unloadPlugin(plugin);
		loaded.remove(plugin);
	}
	
	/**
	 * @return the {@link ParserPlugin plugin} that these {@link ParserPermissions permissions} are for
	 */
	public ParserPlugin getPlugin() {
		return plugin;
	}
	
	/**
	 * Creates a {@link ParserPermissions} token that represents the standard permissions for {@link ParserPlugin plugins}.<br>
	 * Specifically, it can only overwrite or remove {@link lipstone.joshua.parser.plugin.pluggable.Pluggable Pluggable}
	 * items that it added and can read any {@link lipstone.joshua.parser.plugin.pluggable.Pluggable Pluggable} item other
	 * than those that are instances of {@link ParserPlugin ParserPlugin}.
	 * 
	 * @param plugin
	 *            the {@link ParserPlugin} for which these permissions are being created
	 * @param superPermissions
	 *            the permissions for the {@link ParserPlugin} that is loading the {@link ParserPlugin} for which these
	 *            permissions are being created
	 * @return the permissions for <tt>plugin</tt>
	 */
	public static final ParserPermissions makeStandardPluginPermissions(ParserPlugin plugin, ParserPermissions superPermissions) {
		return new ParserPermissions(plugin, superPermissions) {
			@Override
			public boolean validateRead(String action, Pluggable target) throws PluginConflictException {
				if (target == null || target.getPlugin() == plugin)
					return true;
				if (target instanceof ParserPlugin && (!loaded.containsKey(target.getPlugin()) || !loaded.get(target.getPlugin()).validateRead(action, target)))
					throw new PluginConflictException(plugin.getID() + " does not have permission to read " + target.getName() + ".", plugin);
				return true;
			}
			
			@Override
			public boolean validateModify(String action, Pluggable target) throws PluginConflictException {
				if (target == plugin || target.getPlugin() == plugin)
					return true;
				if (!loaded.containsKey(target.getPlugin()) || !loaded.get(target.getPlugin()).validateModify(action, target))
					throw new PluginConflictException(plugin.getID() + " cannot " + action + " " + target.getName() + " because it belongs to " + target.getPlugin() + ".", plugin);
				return true;
			}
			
			@Override
			public boolean validateSpecial(String action, Object target) throws PluginConflictException {
				throw new PluginConflictException("This plugin does not have any special permissions", plugin);
			}
		};
	}
	
	/**
	 * Creates a {@link ParserPermissions} token that represents the root permissions.<br>
	 * Effectively, this {@link ParserPermissions} token never throws
	 * {@link lipstone.joshua.parser.exceptions.PluginConflictException PluginConflictExceptions}.
	 * 
	 * @return a {@link ParserPermissions} token that represents root permissions.
	 */
	public static final ParserPermissions makeRootPermissions() {
		return new ParserPermissions(ParserPlugin.NONE, null) {
			@Override
			public boolean validateRead(String action, Pluggable target) throws PluginConflictException {
				return true;
			}
			
			@Override
			public boolean validateModify(String action, Pluggable target) throws PluginConflictException {
				return true;
			}
			
			@Override
			public boolean validateSpecial(String action, Object target) throws PluginConflictException {
				return true;
			}
		};
	}
}
