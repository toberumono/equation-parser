package lipstone.joshua.parser.plugin;

import lipstone.joshua.parser.Parser;
import lipstone.joshua.parser.ParserPlugin;

/**
 * A simple functional interface that is designed to take a {@link ParserPlugin} and a {@link Parser} as arguments and return
 * an instance of the Functional Interface of type <b>{@literal <T>}</b>.
 * 
 * @author Joshua Lipstone
 * @param <T>
 *            the Functional Interface type to return.
 */
@FunctionalInterface
public interface PluginCurrier<T> {
	public T curry(ParserPlugin plugin, Parser parser);
}
