package lipstone.joshua.parser.plugin;

/**
 * This annotation indicates that the annotated plugin has a settings window.
 * 
 * @author Joshua Lipstone
 */
public @interface ParserSettingsPlugin {}
