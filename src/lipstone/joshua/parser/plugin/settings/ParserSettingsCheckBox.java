package lipstone.joshua.parser.plugin.settings;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;

import lipstone.joshua.parser.Parser;
import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.tokenizer.ConsType;
import lipstone.joshua.parser.tokenizer.ConsCell;

public class ParserSettingsCheckBox extends ParserSettingsItem<JCheckBox> {
	private ConsCell value;
	
	public ParserSettingsCheckBox(String name, ParserPlugin plugin, Parser parser, String displayName) {
		super(name, new JCheckBox(displayName), plugin, parser);
	}
	
	@Override
	protected void addListeners() {
		guiItem.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				onEnter();
			}
		});
	}
	
	@Override
	public ConsCell getValue() {
		return value;
	}
	
	@Override
	public void setValue(ConsCell value) {
		if (value.getCarType() == ConsType.BOOLEAN)
			guiItem.setSelected((Boolean) value.getCar());
		this.value = value;
	}
	
}
