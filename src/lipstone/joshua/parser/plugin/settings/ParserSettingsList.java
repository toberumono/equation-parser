package lipstone.joshua.parser.plugin.settings;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import lipstone.joshua.parser.Parser;
import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.exceptions.SyntaxException;
import lipstone.joshua.parser.tokenizer.ConsCell;
import lipstone.joshua.parser.tokenizer.ConsType;

public class ParserSettingsList<T> extends ParserSettingsItem<JList<T>> {
	protected final DefaultListModel<T> model;
	protected final Logger logger;
	
	public ParserSettingsList(String name, ParserPlugin plugin, Parser parser, DefaultListModel<T> model) {
		super(name, new JList<>(model), plugin, parser);
		this.model = model;
		logger = Logger.getLogger("toberumono.parser.plugin.settings.ParserSettingsList");
	}
	
	@Override
	protected void addListeners() {
		guiItem.getModel().addListDataListener(new ListDataListener() {
			
			@Override
			public void intervalAdded(ListDataEvent e) {}
			
			@Override
			public void intervalRemoved(ListDataEvent e) {}
			
			@Override
			public void contentsChanged(ListDataEvent e) {
				onEnter();
			}
		});
	}
	
	@Override
	public ConsCell getValue() {
		try {
			ConsCell result = new ConsCell(), head = result;
			for (T item : guiItem.getSelectedValuesList())
				head = head.append(getParser().tokenize(item.toString() + "\n"));
			if (head.getCarType() == ConsType.SEPARATOR)
				head.remove();
			return result;
		}
		catch (SyntaxException e) {
			logger.log(Level.SEVERE, "Unable to tokenize the selected items in " + getName() + " in " + getPlugin().getID(), e);
			return new ConsCell();
		}
	}
	
	@Override
	public void setValue(ConsCell value) {
		List<ConsCell> items = value.splitOnSeparator("\n");
		List<Integer> indices = new ArrayList<>();
		for (ConsCell item : items) {
			int index = model.indexOf(item.toString());
			if (index >= 0)
				indices.add(index);
		}
		int[] itms = new int[indices.size()];
		for (int i = 0; i < indices.size(); i++)
			itms[i] = indices.get(i);
		guiItem.setSelectedIndices(itms);
	}
	
}
