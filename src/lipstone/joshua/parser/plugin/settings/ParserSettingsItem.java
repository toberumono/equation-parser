package lipstone.joshua.parser.plugin.settings;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JComponent;

import lipstone.joshua.parser.Parser;
import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.tokenizer.ConsCell;

/**
 * A wrapper class that provides a structure for interfacing {@link javax.swing.JComponent JComponents} with the
 * {@link lipstone.joshua.parser.Parser Parser}.
 * 
 * @author Joshua Lipstone
 * @param <T>
 *            a JComponent that will be used in the creating plugin's settings pane.
 */
public abstract class ParserSettingsItem<T extends JComponent> {
	private final ParserPlugin plugin;
	private final Parser parser;
	protected final T guiItem;
	private final String name;
	
	/**
	 * Constructs a new <tt>ParserSettingsItem</tt> for the given plugin, which wraps the given JComponent.
	 * 
	 * @param name
	 *            The name of this <tt>ParserSettingsItem</tt>
	 * @param guiItem
	 *            the {@link javax.swing.JComponent JComponent} that this will wrap.
	 * @param plugin
	 *            the {@link lipstone.joshua.parser.ParserPlugin ParserPlugin} that created this <tt>ParserSettingsItem</tt>
	 * @param parser
	 *            the {@link lipstone.joshua.parser.Parser Parser} that loaded the
	 *            {@link lipstone.joshua.parser.ParserPlugin ParserPlugin}
	 */
	public ParserSettingsItem(String name, T guiItem, ParserPlugin plugin, Parser parser) {
		this.guiItem = guiItem;
		this.plugin = plugin;
		this.name = name;
		this.parser = parser;
		addListeners();
	}
	
	/**
	 * Adds the appropriate <tt>ActionListener</tt>, or <tt>ActionListener</tt>s as the case may be, to the wrapped
	 * {@link javax.swing.JComponent JComponent}. This is run as part of the constructor and by default simply adds a
	 * {@link java.awt.event.KeyListener KeyListener} that listens for the enter key on keyReleased.<br>
	 * Override it to change the default listener behavior for the wrapped component.
	 */
	protected void addListeners() {
		guiItem.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {}
			
			@Override
			public void keyPressed(KeyEvent e) {}
			
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == 10 || e.getKeyCode() == 13 || e.getKeyText(e.getKeyCode()).equals("Enter"))
					onEnter();
			}
		});
	}
	
	/**
	 * Converts the value in the wrapped {@link javax.swing.JComponent JComponent} to a
	 * {@link lipstone.joshua.parser.tokenizer.ConsCell Token}.<br>
	 * It must return a value such that setValue(getValue()) does not change the value in the wrapped
	 * {@link javax.swing.JComponent JComponent}.
	 * 
	 * @return the value in the wrapped {@link javax.swing.JComponent JComponent} as a
	 *         {@link lipstone.joshua.parser.tokenizer.ConsCell Token}.
	 * @see #setValue(ConsCell) setValue(Token value)
	 */
	public abstract ConsCell getValue();
	
	/**
	 * Sets the value stored in the wrapped {@link javax.swing.JComponent JComponent} to the value stored in the given
	 * {@link lipstone.joshua.parser.tokenizer.ConsCell Token}.<br>
	 * This must convert the value in the {@link lipstone.joshua.parser.tokenizer.ConsCell Token} such that setValue(getValue())
	 * does not change the value in the wrapped {@link javax.swing.JComponent JComponent}.
	 * 
	 * @param value
	 *            the value to set the wrapped {@link javax.swing.JComponent JComponent} to.
	 * @see #getValue() getValue()
	 */
	public abstract void setValue(ConsCell value);
	
	/**
	 * By default this updates an item in the plugin's data <tt>HashMap</tt> with the same name as this field with the result
	 * of a call to {@link #getValue() getValue()}.
	 * 
	 * @see #getValue() getValue()
	 */
	public void onEnter() {
		ConsCell value = getValue();
		plugin.putData(name, value);
	}
	
	public final T getGUIItem() {
		return guiItem;
	}
	
	public final ParserPlugin getPlugin() {
		return plugin;
	}
	
	public final String getName() {
		return name;
	}
	
	protected final Parser getParser() {
		return parser;
	}
	
}
