package lipstone.joshua.parser.plugin.settings;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JComboBox;

import lipstone.joshua.parser.Parser;
import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.exceptions.SyntaxException;
import lipstone.joshua.parser.tokenizer.ConsCell;

public class ParserSettingsComboBox<T> extends ParserSettingsItem<JComboBox<T>> {
	private final Logger logger;
	
	@SafeVarargs
	public ParserSettingsComboBox(String name, ParserPlugin plugin, Parser parser, boolean editable, T... initialItems) {
		super(name, new JComboBox<>(initialItems), plugin, parser);
		guiItem.setEditable(editable);
		logger = Logger.getLogger("toberumono.parser.plugin.settings.ParserSettingsComboBox");
	}
	
	@Override
	protected void addListeners() {
		guiItem.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				onEnter();
			}
		});
	}
	
	@Override
	public ConsCell getValue() {
		try {
			return getParser().tokenize(guiItem.getSelectedItem().toString());
		}
		catch (SyntaxException e) {
			logger.log(Level.SEVERE, "Unable to tokenize the selected item in " + getName() + " in " + getPlugin().getID(), e);
			return new ConsCell();
		}
	}
	
	@Override
	public void setValue(ConsCell value) {
		guiItem.setSelectedItem(value.toString());
	}
	
}
