package lipstone.joshua.parser.plugin.pluggable;

import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.exceptions.UndefinedResultException;
import lipstone.joshua.parser.types.BigDec;

/**
 * Represents a system of angle measure and its relationship to Radians
 * 
 * @author Joshua Lipstone
 */
public class AngleType implements Pluggable {
	private final AngleConversion toRadians, fromRadians;
	/**
	 * A backup AngleType that should only be used when no other types are available
	 */
	public static final AngleType NONE = new AngleType("NO TYPE", null, angle -> {
		throw new UndefinedResultException("This system of angle measure cannot be used in conversions", null);
	}, angle -> {
		throw new UndefinedResultException("This system of angle measure cannot be used in conversions", null);
	});
	private final String name;
	private final ParserPlugin plugin;
	
	/**
	 * Constructs an {@link AngleType} with the given name
	 * 
	 * @param name
	 *            the name of the measurement system that this {@link AngleType} represents.
	 * @param plugin
	 *            the {@link lipstone.joshua.parser.ParserPlugin plugin} that added this {@link AngleType}
	 * @param toRadians
	 *            a function that converts an angle in this system of measure to its equivalent in radians
	 * @param fromRadians
	 *            a function that converts an angle in radians to its equivalent in this system
	 */
	public AngleType(String name, ParserPlugin plugin, AngleConversion toRadians, AngleConversion fromRadians) {
		this.name = name;
		this.plugin = plugin;
		this.toRadians = toRadians;
		this.fromRadians = fromRadians;
	}
	
	/**
	 * Converts an angle measure in this system into its equivalent in radians.
	 * 
	 * @param angle
	 *            the angle to convert
	 * @return the given angle's equivalent in radians
	 * @throws UndefinedResultException
	 *             Allows errors generated in the conversion to be passed back to the calling function.
	 */
	public BigDec toRadians(BigDec angle) throws UndefinedResultException {
		return toRadians.convert(angle);
	}
	
	/**
	 * Converts an in radians into its equivalent in this system.
	 * 
	 * @param angle
	 *            the angle to convert in radians
	 * @return the given angle's equivalent in this system
	 * @throws UndefinedResultException
	 *             Allows errors generated in the conversion to be passed back to the calling function.
	 */
	public BigDec fromRadians(BigDec angle) throws UndefinedResultException {
		return fromRadians.convert(angle);
	}
	
	/**
	 * @return the name of this system of angle measure
	 */
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof String)
			return o.equals(name);
		if (o instanceof AngleType)
			return ((AngleType) o).name.equals(name);
		return false;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	/**
	 * @return the {@link lipstone.joshua.parser.ParserPlugin plugin} that added this {@link AngleType}
	 */
	@Override
	public ParserPlugin getPlugin() {
		return plugin;
	}
}
