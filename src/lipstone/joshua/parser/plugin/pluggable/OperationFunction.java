package lipstone.joshua.parser.plugin.pluggable;

import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.tokenizer.ConsCell;

/**
 * Represents a function that evaluates a given input and returns the result for an {@link Operation}
 * 
 * @author Joshua Lipstone
 */
@FunctionalInterface
public interface OperationFunction {
	
	/**
	 * Evaluates the given input and returns the result without modifying the original.
	 * 
	 * @param input
	 *            the {@link lipstone.joshua.parser.tokenizer.ConsCell Tokens} passed to the operation
	 * @return the result of evaluating the input in the way described by the {@link Operation} that contains this method
	 * @throws ParserException
	 *             to allow exceptions thrown while evaluating the input to propagate back to the calling function
	 */
	public ConsCell runOperation(ConsCell input) throws ParserException;
}