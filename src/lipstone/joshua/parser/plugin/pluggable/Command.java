package lipstone.joshua.parser.plugin.pluggable;

import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.tokenizer.ConsCell;

/**
 * Holds a complete description of a command that can be loaded added to this library's parsing capabilities and a Java
 * method that executes said command.
 * 
 * @author Joshua Lipstone
 */
public class Command extends Action {
	private final CommandFunction function;
	
	/**
	 * Creates a new command object with the specified description
	 * 
	 * @param name
	 *            the name of this command
	 * @param description
	 *            a description of what this command does
	 * @param plugin
	 *            the plugin that contains this command
	 * @param function
	 *            a method that executes the command that this {@link Command} describes
	 */
	public Command(String name, String description, ParserPlugin plugin, CommandFunction function) {
		this(name, description, null, plugin, function);
	}
	
	/**
	 * Creates a new command object with the specified name, description, and additional names and/or abbreviations
	 * 
	 * @param name
	 *            the name of this command
	 * @param description
	 *            a description of what this command does
	 * @param additionalNames
	 *            additional names and/or abbreviations for this command
	 * @param plugin
	 *            the plugin that contains this command
	 * @param function
	 *            a method that executes the command that this {@link Command} describes
	 */
	public Command(String name, String description, String[] additionalNames, ParserPlugin plugin, CommandFunction function) {
		super(name, description, plugin, additionalNames);
		constructDescription();
		this.function = function;
	}
	
	/**
	 * Evaluates the given input with this {@link Command}
	 * 
	 * @param input
	 *            the {@link lipstone.joshua.parser.tokenizer.ConsCell Token} tree to evaluate
	 * @return the result of evaluating the given {@link lipstone.joshua.parser.tokenizer.ConsCell Token} tree with this
	 *         {@link Command}
	 * @throws ParserException
	 *             so that exceptions propagate back to the calling function
	 */
	@Override
	public ConsCell evaluate(ConsCell input) throws ParserException {
		return function.runCommand(input.splitOnSeparator());
	}
}
