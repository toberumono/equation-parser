package lipstone.joshua.parser.plugin.pluggable;

import java.util.List;

import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.tokenizer.ConsCell;

/**
 * Represents a function that runs the {@link Command} that contains it using the given arguments.
 * 
 * @author Joshua Lipstone
 */
@FunctionalInterface
public interface CommandFunction {
	
	/**
	 * Runs the {@link Command} that contains this function using the given arguments.
	 * 
	 * @param arguments
	 *            the arguments with which to run the {@link Command}
	 * @return the output from the running the {@link Command}
	 * @throws ParserException
	 *             to allow exceptions thrown while evaluating the input to propagate back to the calling function
	 */
	public ConsCell runCommand(List<ConsCell> arguments) throws ParserException;
}