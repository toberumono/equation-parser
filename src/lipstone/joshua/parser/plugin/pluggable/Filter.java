package lipstone.joshua.parser.plugin.pluggable;

import lipstone.joshua.parser.Evaluator;
import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.tokenizer.ConsCell;

/**
 * Represents a filter that can be used in either preprocessing or postprocessing of inputs in the
 * {@link lipstone.joshua.parser.Parser Parser} that the {@link Filter} is added to.
 * 
 * @author Joshua Lipstone
 */
public class Filter implements Pluggable, Evaluator {
	private final String name;
	private final ParserPlugin plugin;
	private final ScrubbingFunction scrubber;
	
	public Filter(String name, ParserPlugin plugin, ScrubbingFunction scrubber) {
		this.name = name;
		this.plugin = plugin;
		this.scrubber = scrubber;
	}
	
	@Override
	public ConsCell evaluate(ConsCell input) throws ParserException {
		return scrubber.scrub(input);
	}
	
	@Override
	public ParserPlugin getPlugin() {
		return plugin;
	}
	
	@Override
	public String getName() {
		return name;
	}
}
