package lipstone.joshua.parser.plugin.pluggable;

import lipstone.joshua.parser.exceptions.UndefinedResultException;
import lipstone.joshua.parser.types.BigDec;

/**
 * For use with {@link lipstone.joshua.parser.plugin.pluggable.AngleType AngleType}
 * 
 * @author Joshua Lipstone
 */
@FunctionalInterface
public interface AngleConversion {
	
	/**
	 * Converts between one system of angle measure and another
	 * 
	 * @param angle
	 *            the angle in its system of measure
	 * @return the angle in the new system of measure
	 * @throws UndefinedResultException
	 *             if a math error occured while converting the angle
	 */
	public BigDec convert(BigDec angle) throws UndefinedResultException;
}
