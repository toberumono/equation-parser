package lipstone.joshua.parser.plugin.pluggable;

/**
 * Stores data for a parameter for an {@link Operation}.<br>
 * This is used in the auto-generated help documentation.
 * 
 * @author Joshua Lipstone
 */
public class Parameter {
	private final String name, information;
	private final boolean isRequired;
	
	/**
	 * Data for a {@link Parameter} used in the automatically generated help data for an {@link Operation}.<br>
	 * This is a convinience method for required parameters. (e.g. {@link #Parameter(String, String, boolean) Parameter(name,
	 * information, true)}
	 * 
	 * @param name
	 *            the name to be displayed
	 * @param information
	 *            information on what is expected in this item
	 */
	public Parameter(String name, String information) {
		this(name, information, true);
	}
	
	/**
	 * Data for a parameter used in the automatically generated help data
	 * 
	 * @param name
	 *            the name to be displayed
	 * @param information
	 *            information on what is expected in this item
	 * @param isRequired
	 *            flags this parameter as required
	 */
	public Parameter(String name, String information, boolean isRequired) {
		this.name = name;
		this.information = information;
		this.isRequired = isRequired;
	}
	
	public String getName() {
		return name;
	}
	
	public String getInformation() {
		return information;
	}
	
	public boolean isRequired() {
		return isRequired;
	}
	
}
