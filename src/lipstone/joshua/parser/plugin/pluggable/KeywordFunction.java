package lipstone.joshua.parser.plugin.pluggable;

import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.tokenizer.ConsCell;

/**
 * Represents a method that gets the keyword data for the {@link Keyword} that contains it.
 * 
 * @author Joshua Lipstone
 */
@FunctionalInterface
public interface KeywordFunction {
	
	/**
	 * Gets the data for the {@link Keyword} that contains this method.
	 * 
	 * @return the data for the {@link Keyword} that contains this method.
	 * @throws ParserException
	 *             to allow exceptions thrown while retrieving the keyword data to propagate back to the calling function
	 */
	public ConsCell getKeywordData() throws ParserException;
}