package lipstone.joshua.parser.plugin.pluggable;

import lipstone.joshua.parser.ParserPlugin;

/**
 * Represents an item that can be added to a {@link lipstone.joshua.parser.Parser Parser} from a
 * {@link lipstone.joshua.parser.ParserPlugin ParserPlugin}.
 * 
 * @author Joshua Lipstone
 */
public interface Pluggable {
	
	/**
	 * @return the {@link lipstone.joshua.parser.ParserPlugin ParserPlugin} that added this item
	 */
	public ParserPlugin getPlugin();
	
	/**
	 * @return the name of this item as a {@link String}
	 */
	public String getName();
}
