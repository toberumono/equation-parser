package lipstone.joshua.parser.plugin.pluggable;

import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.tokenizer.ConsCell;

/**
 * Represents a function that can be used in either preprocessing or postprocessing filters
 * 
 * @author Joshua Lipstone
 */
@FunctionalInterface
public interface ScrubbingFunction {
	
	/**
	 * <b>This function does <i>not</i> modify the input token tree.</b><br>
	 * This function must recurse through all levels of the tree. Calling
	 * {@link lipstone.joshua.parser.Parser#checkDescenders(ConsCell, ScrubbingFunction)} is recommended.
	 * 
	 * @param input
	 *            the token tree to be scrubbed.
	 * @return a scrubbed copy of the token tree.
	 * @throws ParserException
	 *             to allow exceptions to propagate back to the calling function.
	 * @see lipstone.joshua.parser.Parser#checkDescenders(ConsCell, ScrubbingFunction)
	 */
	public ConsCell scrub(ConsCell input) throws ParserException;
}
