package lipstone.joshua.parser.plugin.pluggable;

import lipstone.joshua.parser.Evaluator;
import lipstone.joshua.parser.ParserPlugin;

/**
 * Root class for actions that plugins can add to the {@link lipstone.joshua.parser.Parser}
 * 
 * @author Joshua Lipstone
 */
public abstract class Action implements Evaluator, Comparable<Action>, Pluggable {
	protected static final String DEFAULT_DESCRIPTION = "There is no additional information on this item.";
	protected final String name;
	protected String description = DEFAULT_DESCRIPTION;
	protected final ParserPlugin plugin;
	protected final String[] alternateNames;
	
	/**
	 * Creates a new {@link Action} with the specified name, description, containing plugin, and alternate names/abbreviations<br>
	 * All extending classes MUST call construct description at the end of their constructor.
	 * 
	 * @param name
	 *            the name of this {@link Action}
	 * @param description
	 *            a description of what this {@link Action} does
	 * @param plugin
	 *            the plugin that contains this {@link Action}
	 * @param alternateNames
	 *            alternate names or abbreviations for this {@link Action}
	 */
	public Action(String name, String description, ParserPlugin plugin, String[] alternateNames) {
		this.name = name;
		if (!description.equals(""))
			this.description = description;
		this.plugin = plugin;
		if (alternateNames != null)
			this.alternateNames = alternateNames;
		else
			this.alternateNames = new String[]{};
	}
	
	/**
	 * @return the name of this {@link Action}
	 */
	@Override
	public String getName() {
		return name;
	}
	
	/**
	 * @return the generated description and the plugin that this is found in.
	 */
	public String getDescription() {
		return description + "\nfound in: " + plugin.getID();
	}
	
	/**
	 * @return the {@link lipstone.joshua.parser.ParserPlugin plugin} that added this {@link Action}
	 */
	@Override
	public ParserPlugin getPlugin() {
		return plugin;
	}
	
	/**
	 * @return the alternate names for this item
	 */
	public final String[] getAlternateNames() {
		return alternateNames;
	}
	
	/**
	 * Constructs the description to use for this {@link Action}<br>
	 * DO NOT CALL THIS OUTSIDE OF A CONSTRUCTOR 
	 */
	protected void constructDescription() {
		description = description + "\n";
		String otherNames = "";
		for (String name : alternateNames)
			otherNames = ", " + name;
		if (otherNames.length() > 2)
			description = description + "additionalNames: " + otherNames.substring(2) + ".\n";
	}
	
	@Override
	public int compareTo(Action o) {
		return name.compareTo(o.name);
	}
	
	/**
	 * @return the name of this {@link Action}
	 */
	@Override
	public String toString() {
		return name;
	}
}
