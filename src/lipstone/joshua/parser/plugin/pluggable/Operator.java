package lipstone.joshua.parser.plugin.pluggable;

import java.util.function.BiPredicate;

import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.tokenizer.ConsCell;
import lipstone.joshua.parser.util.TokenFunction;

/**
 * Represents a two-argument operator
 * 
 * @author Joshua Lipstone
 */
public class Operator implements Pluggable, Comparable<Operator> {
	private final String symbol;
	private final ParserPlugin plugin;
	private final int precedence;
	private final ConsCell leftInitial;
	private final boolean isRightAssociative;
	private final BiPredicate<ConsCell, ConsCell> isApplicable;
	private final TokenFunction operatorFunction;
	private final LockedOperator locked;
	
	/**
	 * This allows the priority of the basic operators to be adjusted if more operators are added.
	 */
	public static final int BASE_PRECEDENCE = 0;
	
	/**
	 * This {@link Operator} is used to represent the absence of an {@link Operator}.
	 */
	public static final Operator noOp = new Operator("noOp", (a, b) -> false, (a, b) -> {
		throw new UnsupportedOperationException();
	}) {
		@Override
		public boolean isLocked() {
			return true;
		}
		
		@Override
		public Operator lock() {
			return this;
		}
	};
	
	/**
	 * Constructs a new {@link Operator}
	 * 
	 * @param symbol
	 *            the name that this operator will be identified by in the {@link lipstone.joshua.parser.tokenizer.Tokenizer
	 *            Tokenizer}
	 * @param plugin
	 *            the {@link lipstone.joshua.parser.ParserPlugin plugin} adding this {@link Operator}
	 * @param precedence
	 *            the precedence of this operator. Higher precedence operators are evaluated sooner.
	 * @param leftInitial
	 *            the default value used by this {@link Operator} if there is no
	 *            {@link lipstone.joshua.parser.tokenizer.ConsCell Token} directly to its left. Must have a length of 1.
	 * @param isRightAssociative
	 *            true if the operator is right associative, otherwise false. For example, the addition (+) operator is left
	 *            associative while the exponentiation (^) operator is right associative
	 * @param isApplicable
	 *            a short function that takes two {@link lipstone.joshua.parser.tokenizer.ConsCell Tokens} and returns true if
	 *            their types are applicable to this {@link Operator}, otherwise false
	 * @param operatorFunction
	 *            a function that takes two {@link lipstone.joshua.parser.tokenizer.ConsCell Tokens} and returns the result of
	 *            applying this {@link Operator} on them
	 */
	public Operator(String symbol, ParserPlugin plugin, int precedence, ConsCell leftInitial, boolean isRightAssociative, BiPredicate<ConsCell, ConsCell> isApplicable, TokenFunction operatorFunction) {
		super();
		this.symbol = symbol;
		this.plugin = plugin;
		this.precedence = precedence;
		this.leftInitial = leftInitial.clone();
		this.isRightAssociative = isRightAssociative;
		this.isApplicable = isApplicable;
		this.operatorFunction = operatorFunction;
		locked = new LockedOperator(this);
	}
	
	/**
	 * Creates a clone of <tt>base</tt>
	 * 
	 * @param base
	 *            the {@link Operator} to clone
	 */
	public Operator(Operator base) {
		this(base.symbol, base.plugin, base.precedence, base.leftInitial, base.isRightAssociative, base.isApplicable, base.operatorFunction);
	}
	
	Operator(Operator base, LockedOperator locked) {
		symbol = base.symbol;
		plugin = base.plugin;
		precedence = base.precedence;
		leftInitial = base.leftInitial.clone();
		isRightAssociative = base.isRightAssociative;
		isApplicable = base.isApplicable;
		operatorFunction = base.operatorFunction;
		this.locked = locked;
	}
	
	private Operator(String symbol, BiPredicate<ConsCell, ConsCell> isApplicable, TokenFunction operatorFunction) {
		super();
		this.symbol = symbol;
		plugin = null;
		precedence = Integer.MIN_VALUE;
		leftInitial = new ConsCell();
		isRightAssociative = false;
		this.isApplicable = isApplicable;
		this.operatorFunction = operatorFunction;
		locked = new LockedOperator(this);
	}
	
	/**
	 * @return the precedence of this {@link Operator}
	 */
	public int getPrecedence() {
		return precedence;
	}
	
	/**
	 * @return the {@link lipstone.joshua.parser.tokenizer.ConsCell Token} used as the default value for this {@link Operator
	 *         Operator's} left input
	 */
	public ConsCell getLeftInitial() {
		return leftInitial.clone();
	}
	
	/**
	 * @return true if this {@link Operator} is right associative
	 */
	public boolean isRightAssociative() {
		return isRightAssociative;
	}
	
	/**
	 * @param left
	 *            the left hand {@link lipstone.joshua.parser.tokenizer.ConsCell Token}
	 * @param right
	 *            the right hand {@link lipstone.joshua.parser.tokenizer.ConsCell Token}
	 * @return true if this {@link Operator} is valid for the types of the given
	 *         {@link lipstone.joshua.parser.tokenizer.ConsCell Tokens}
	 * @throws ParserException
	 *             if an exception occurs while testing the applicability of this {@link Operator} to the given values
	 */
	public boolean isApplicable(ConsCell left, ConsCell right) throws ParserException {
		return isApplicable.test(left, right);
	}
	
	/**
	 * Evaluates two {@link lipstone.joshua.parser.tokenizer.ConsCell Tokens} with this operator
	 * 
	 * @param left
	 *            the {@link lipstone.joshua.parser.tokenizer.ConsCell Token} to the left of the operator
	 * @param right
	 *            the {@link lipstone.joshua.parser.tokenizer.ConsCell Token} to the right of the operator
	 * @return the result of the applying this {@link Operator} to the given inputs
	 * @throws ParserException
	 *             if an error occurred during the evaluation
	 */
	public ConsCell evaluate(ConsCell left, ConsCell right) throws ParserException {
		return operatorFunction.apply(left, right);
	}
	
	/**
	 * @return true if other is an {@link Operator} that shares the same name as this {@link Operator} or other is a
	 *         {@link String} that equals the name of this {@link Operator}, otherwise false
	 */
	@Override
	public boolean equals(Object other) {
		return other instanceof Operator ? ((Operator) other).symbol.equals(symbol) : (other instanceof String ? symbol.equals(other) : false);
	}
	
	/**
	 * @return the {@link lipstone.joshua.parser.ParserPlugin plugin} that added this {@link Operator}
	 */
	@Override
	public ParserPlugin getPlugin() {
		return plugin;
	}
	
	/**
	 * @return the name/symbol for this {@link Operator} (e.g. '+' for addition, '*' for multiplication, etc.)
	 */
	@Override
	public String getName() {
		return symbol;
	}
	
	@Override
	public String toString() {
		return symbol;
	}
	
	/**
	 * Compares this {@link Operator} with the given one by their precedence.<br>
	 * Note: this class has a natural ordering that is inconsistent with equals.
	 * 
	 * @return 1 if this {@link Operator Operator's} precedence is greater than <tt>o</tt>'s precedence, -1 if this
	 *         {@link Operator Operator's} precedence is less, and 0 if they are equal.
	 */
	@Override
	public int compareTo(Operator o) {
		return precedence > o.precedence ? 1 : precedence < o.precedence ? -1 : 0;
	}
	
	/**
	 * <i>This method does not modify the {@link Operator} object that it is called on in any way.</i> Therefore, it is very
	 * important to reassign the variable that it is being called on to this function's return value.<br>
	 * Note: If this {@link Operator} instance is already locked, this method returns the {@link Operator} it was called on.
	 * 
	 * @return a locked version of this {@link Operator}. A locked {@link Operator}'s {@link #isApplicable(ConsCell, ConsCell)}
	 *         method will always evaluate to <tt>false</tt>
	 */
	public Operator lock() {
		return locked;
	}
	
	/**
	 * <i>This method does not modify the {@link Operator} object that it is called on in any way.</i> Therefore, it is very
	 * important to reassign the variable that it is being called on to this function's return value.<br>
	 * Note: If this {@link Operator} instance is already unlocked, this method returns the {@link Operator} it was called
	 * on.
	 * 
	 * @return a unlocked version of this {@link Operator}. An unlocked {@link Operator}'s
	 *         {@link #isApplicable(ConsCell, ConsCell)} method will forwards to the <tt>isApplicable</tt> method passed to the
	 *         constructor.
	 */
	public Operator unlock() {
		return this;
	}
	
	/**
	 * @return true if this {@link Operator} is locked, otherwise false
	 */
	public boolean isLocked() {
		return false;
	}
}

class LockedOperator extends Operator {
	private final Operator unlocked;
	
	public LockedOperator(Operator base) {
		super(base, null);
		unlocked = base;
	}
	
	@Override
	public boolean isApplicable(ConsCell left, ConsCell right) {
		return false;
	}
	
	@Override
	public Operator lock() {
		return this;
	}
	
	@Override
	public Operator unlock() {
		return unlocked;
	}
	
	@Override
	public boolean isLocked() {
		return true;
	}
}
