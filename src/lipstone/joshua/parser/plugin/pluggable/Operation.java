package lipstone.joshua.parser.plugin.pluggable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.tokenizer.ConsCell;

/**
 * Uses the provided details of an operation that can be loaded into this library's parsing capabilities to generate and
 * store a complete description of the operation along with a function to execute it.
 * 
 * @author Joshua Lipstone
 */
public class Operation extends Action {
	private final String information, returns;
	private final boolean isFinal;
	private final List<Parameter> parameters;
	private final OperationFunction function;
	
	/**
	 * Constructor for a non-final operation
	 * 
	 * @param name
	 *            the name of the operation
	 * @param parameters
	 *            the parameters that this operation expects
	 * @param returns
	 *            description what this operation returns
	 * @param information
	 *            additional information beyond the parameters and returns
	 * @param plugin
	 *            the plugin that contains this operation
	 * @param function
	 *            a method that performs the operation that this {@link Operation} describes
	 */
	public Operation(String name, List<Parameter> parameters, String returns, String information, ParserPlugin plugin, OperationFunction function) {
		this(name, parameters, returns, information, null, plugin, function);
	}
	
	/**
	 * Constructor for a non-final operation
	 * 
	 * @param name
	 *            the name of the operation
	 * @param parameters
	 *            the parameters that this operation expects
	 * @param returns
	 *            description what this operation returns
	 * @param information
	 *            additional information beyond the parameters and returns
	 * @param alternateNames
	 *            additional names and/or abbreviation for this operation
	 * @param plugin
	 *            the plugin that contains this operation
	 * @param function
	 *            a method that performs the operation that this {@link Operation} describes
	 */
	public Operation(String name, List<Parameter> parameters, String returns, String information, String[] alternateNames, ParserPlugin plugin, OperationFunction function) {
		super(name, DEFAULT_DESCRIPTION, plugin, alternateNames);
		this.parameters = new ArrayList<>(parameters); //To prevent unintended external modifications
		this.returns = returns;
		this.information = information;
		this.function = function;
		isFinal = false;
		constructDescription();
	}
	
	/**
	 * Constructor for any operation
	 * 
	 * @param name
	 *            the name of the operation
	 * @param parameters
	 *            the parameters that this operation expects
	 * @param returns
	 *            description what this operation returns
	 * @param information
	 *            additional information beyond the parameters and returns
	 * @param plugin
	 *            the plugin that contains this operation
	 * @param isFinal
	 *            whether this is a final operation
	 * @param function
	 *            a method that performs the operation that this {@link Operation} describes
	 */
	public Operation(String name, List<Parameter> parameters, String returns, String information, ParserPlugin plugin, boolean isFinal, OperationFunction function) {
		this(name, parameters, returns, information, null, plugin, isFinal, function);
	}
	
	/**
	 * Constructor for any operation
	 * 
	 * @param name
	 *            the name of the operation
	 * @param parameters
	 *            the parameters that this operation expects
	 * @param returns
	 *            description what this operation returns
	 * @param information
	 *            additional information beyond the parameters and returns
	 * @param alternateNames
	 *            additional names and/or abbreviation for this operation
	 * @param plugin
	 *            the plugin that contains this operation
	 * @param isFinal
	 *            whether this is a final operation
	 * @param function
	 *            a method that performs the operation that this {@link Operation} describes
	 */
	public Operation(String name, List<Parameter> parameters, String returns, String information, String[] alternateNames, ParserPlugin plugin, boolean isFinal, OperationFunction function) {
		super(name, DEFAULT_DESCRIPTION, plugin, alternateNames);
		this.parameters = new ArrayList<>(parameters); //To prevent unintended external modifications
		this.returns = returns;
		this.information = information;
		this.isFinal = isFinal;
		this.function = function;
		constructDescription();
	}
	
	/**
	 * Gets the additional information about this plugin
	 * 
	 * @return the additional information about this plugin
	 */
	public final String getInformation() {
		return information;
	}
	
	/**
	 * Gets the return data for this plugin
	 * 
	 * @return the returns section for this plugin
	 */
	public final String getReturns() {
		return returns;
	}
	
	/**
	 * Gets the parameters data from this plugin
	 * 
	 * @return the parameters list in this plugin
	 */
	public final List<Parameter> getParameters() {
		return Collections.unmodifiableList(parameters);
	}
	
	/**
	 * Gets the final status of this operation
	 * 
	 * @return whether the parser should stop processing after finding this plugin in an input string
	 */
	public final boolean isFinal() {
		return isFinal;
	}
	
	@Override
	protected void constructDescription() {
		if (!description.equals(DEFAULT_DESCRIPTION)) //If we don't need to do anything here, stop
			return;
		StringBuilder description = new StringBuilder();
		description.append("usage: ").append(name).append("(");
		//Construct the full name with parameters
		for (Parameter p : parameters) {
			if (p.isRequired())
				description.append(p.getName()).append(", ");
			else
				description.append("[").append(p.getName()).append("], ");
		}
		if (parameters.size() > 0) //Drop the last comma
			description.delete(description.length() - 2, description.length());
		description.append(")\n\n"); //Finished with the usage line
		
		//Construct the parameter information section
		for (Parameter p : parameters)
			if (p.isRequired())
				description.append(p.getName()).append(": ").append(p.getInformation()).append("\n");
			else
				description.append(p.getName()).append(": ").append(p.getInformation()).append(" [optional]\n");
		if (parameters.size() > 0)
			description.append("\n"); //Finished with the parameters section
			
		description.append("returns: ").append(returns).append("\n"); //Finished with the returns line
		if (isFinal)
			description.append("This operation is final.\n");
		String otherNames = "";
		for (String name : alternateNames)
			otherNames = ", " + name;
		if (otherNames.length() > 2)
			description.append("additionalNames: ").append(otherNames.substring(2)).append(".\n");
		if (information.length() > 0)
			description.append(information); //Finished with the additional information section
		this.description = description.toString();
	}
	
	/**
	 * Evaluates the given input with this {@link Operation}
	 * 
	 * @param input
	 *            the {@link lipstone.joshua.parser.tokenizer.ConsCell Token} tree to evaluate
	 * @return the result of evaluating the given {@link lipstone.joshua.parser.tokenizer.ConsCell Token} tree with this
	 *         {@link Operation}
	 * @throws ParserException
	 *             so that exceptions propagate back to the calling function
	 */
	@Override
	public ConsCell evaluate(ConsCell input) throws ParserException {
		return function.runOperation(input);
	}
}
