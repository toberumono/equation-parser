package lipstone.joshua.parser.plugin.pluggable;

import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.tokenizer.ConsCell;

/**
 * Holds a complete description of a keyword that can be loaded added to this library's parsing capabilities and a Java
 * method that gets the data as described.
 * 
 * @author Joshua Lipstone
 */
public class Keyword extends Action {
	private final KeywordFunction function;
	
	/**
	 * Creates a new keyword object with the specified description
	 * 
	 * @param name
	 *            the name of this keyword does
	 * @param description
	 *            a description of what this keyword does
	 * @param plugin
	 *            the plugin that contains this keyword
	 * @param function
	 *            a method that returns the keyword data that this {@link Keyword} describes
	 */
	public Keyword(String name, String description, ParserPlugin plugin, KeywordFunction function) {
		this(name, description, null, plugin, function);
	}
	
	/**
	 * Creates a new keyword object with the specified description
	 * 
	 * @param name
	 *            the name of this keyword does
	 * @param description
	 *            a description of what this keyword does
	 * @param additionalNames
	 *            additional names and/or abbreviations for this keyword
	 * @param plugin
	 *            the plugin that contains this keyword
	 * @param function
	 *            a method that returns the keyword data that this {@link Keyword} describes
	 */
	public Keyword(String name, String description, String[] additionalNames, ParserPlugin plugin, KeywordFunction function) {
		super(name, description, plugin, additionalNames);
		constructDescription();
		this.function = function;
	}
	
	/**
	 * Evaluates the given input with this {@link Keyword}
	 * 
	 * @param input
	 *            this should be <tt>null</tt>
	 * @return the value of this {@link Keyword}
	 * @throws ParserException
	 *             so that exceptions propagate back to the calling function
	 */
	@Override
	public ConsCell evaluate(ConsCell input) throws ParserException {
		return function.getKeywordData();
	}
}
