package lipstone.joshua.parser.plugin.mapping;

import java.util.Collection;
import java.util.Iterator;

import lipstone.joshua.parser.exceptions.PluginConflictException;
import lipstone.joshua.parser.plugin.mapping.ObservablePluggableMap.Returner;

public abstract class ObservablePluggableCollection<E> implements Collection<E> {
	Returner<Collection<E>> backingCollection;
	
	protected ObservablePluggableCollection(Returner<Collection<E>> backingCollection) {
		this.backingCollection = backingCollection;
	}
	
	@Override
	public boolean contains(Object o) {
		return backingCollection.get().contains(o);
	}
	
	@Override
	public Iterator<E> iterator() {
		return backingCollection.get().iterator();
	}
	
	@Override
	public Object[] toArray() {
		return backingCollection.get().toArray();
	}
	
	@Override
	public <T> T[] toArray(T[] a) {
		return backingCollection.get().toArray(a);
	}
	
	@Override
	public boolean add(E e) {
		throw new UnsupportedOperationException("Not supported.");
	}
	
	@Override
	public boolean containsAll(Collection<?> c) {
		return backingCollection.get().containsAll(c);
	}
	
	@Override
	public boolean addAll(Collection<? extends E> c) {
		throw new UnsupportedOperationException("Not supported.");
	}
	
	/**
	 * Attempts to remove the entry <tt>e</tt> from this collection.<br>
	 * This method ensures that the caller has permission to modify the {@link lipstone.joshua.parser.plugin.pluggable.Pluggable
	 * item} being removed.
	 * 
	 * @param e
	 *            the element to remove from this set
	 * @return true if <tt>e</tt> was removed from the set, otherwise false
	 * @throws PluginConflictException
	 *             if the caller does not have permission to modify the {@link lipstone.joshua.parser.plugin.pluggable.Pluggable
	 *             item} being removed.
	 * @throws NullPointerException
	 *             if <tt>e</tt> is null
	 */
	public abstract boolean tryRemove(E e) throws PluginConflictException;
	
	/**
	 * {@inheritDoc}<br>
	 * <i>This method will fail silently if the {@link lipstone.joshua.parser.ParserPlugin plugin} calling it does not
	 * have permission to modify one or more of the values in this map that are not mapped to the keys in <tt>c</tt>.</i>
	 */
	@Override
	@Deprecated
	public boolean retainAll(Collection<?> c) {
		return removeRetain(c, false);
	}
	
	protected abstract boolean removeRetain(Collection<?> c, boolean remove);
	
	/**
	 * {@inheritDoc}<br>
	 * <i>This method will fail silently if the {@link lipstone.joshua.parser.ParserPlugin plugin} calling it does not
	 * have permission to modify one or more of the values mapped to the keys in <tt>c</tt>.</i>
	 */
	@Override
	@Deprecated
	public boolean removeAll(Collection<?> c) {
		return removeRetain(c, true);
	}
}
