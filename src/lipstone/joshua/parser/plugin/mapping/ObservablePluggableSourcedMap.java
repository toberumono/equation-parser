package lipstone.joshua.parser.plugin.mapping;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lipstone.joshua.parser.exceptions.PluginConflictException;
import lipstone.joshua.parser.plugin.ParserPermissions;
import lipstone.joshua.parser.plugin.pluggable.Pluggable;
import lipstone.joshua.parser.util.ListenerTracker;
import lipstone.joshua.parser.util.PluggableTracker;

import javafx.beans.InvalidationListener;
import javafx.collections.MapChangeListener;

public class ObservablePluggableSourcedMap<V extends Pluggable> extends ObservablePluggableMap<V> implements ListenerTracker, PluggableTracker {
	private final HashMap<String, V> pluginMapped = new HashMap<>();
	private int clearingPluginOwned = 0; //This is used to avoid ConcurrentModificationExceptions
	private final List<Object> listeners = new ArrayList<>();
	
	public ObservablePluggableSourcedMap(Map<String, V> backing, ParserPermissions permissions) {
		super(backing, permissions);
	}
	
	public ObservablePluggableSourcedMap(ObservablePluggableMap<V> base, ParserPermissions permissions) {
		super(base, permissions);
	}
	
	@Override
	public void addListener(InvalidationListener listener) {
		synchronized (kernel) {
			super.addListener(listener);
			listeners.add(listener);
		}
	}
	
	@Override
	public void removeListener(InvalidationListener listener) {
		synchronized (kernel) {
			super.removeListener(listener);
			listeners.remove(listener);
		}
	}
	
	@Override
	public void addListener(MapChangeListener<? super String, ? super V> listener) {
		synchronized (kernel) {
			super.addListener(listener);
			listeners.add(listener);
		}
	}
	
	@Override
	public void removeListener(MapChangeListener<? super String, ? super V> listener) {
		synchronized (kernel) {
			super.removeListener(listener);
			listeners.remove(listener);
		}
	}
	
	/**
	 * @param key
	 *            the <tt>key</tt> to check
	 * @return true if the {@link lipstone.joshua.parser.ParserPlugin plugin} owns the
	 *         {@link lipstone.joshua.parser.plugin.pluggable.Pluggable Pluggable} item mapped to <tt>key</tt>
	 */
	public boolean owns(String key) {
		return pluginMapped.containsKey(key);
	}
	
	/**
	 * @param value
	 *            the <tt>value</tt> to check
	 * @return true if the {@link lipstone.joshua.parser.ParserPlugin plugin} owns <tt>value</tt>
	 */
	public boolean owns(V value) {
		return pluginMapped.containsValue(value);
	}
	
	/**
	 * This method is solely for the purpose of clearing the {@link lipstone.joshua.parser.plugin.pluggable.Pluggable
	 * Pluggable} items loaded by the calling {@link lipstone.joshua.parser.ParserPlugin plugin}.
	 * 
	 * @throws PluginConflictException
	 *             if the plugin does not own all of the items in the {@link ObservablePluggableSourcedMap map}
	 */
	@Override
	public void clearTrackedPluggables() throws PluginConflictException {
		synchronized (kernel) {
			try {
				clearingPluginOwned++;
				clearTrackedListeners();
				//Because this version of clearing is guaranteed to only affect pluggable items loaded by the calling plugin, we don't need to check for 'clear' permissions.
				for (Iterator<Entry<String, V>> i = pluginMapped.entrySet().iterator(); i.hasNext();) {
					Entry<String, V> e = i.next();
					super.tryRemove(e.getKey());
					i.remove();
				}
			}
			finally {
				clearingPluginOwned--;
			}
		}
	}
	
	@Override
	public void clearTrackedListeners() {
		synchronized (kernel) {
			try {
				clearingPluginOwned++;
				while (listeners.size() > 0) {
					Object listener = listeners.get(0);
					if (listener instanceof InvalidationListener)
						removeListener((InvalidationListener) listener);
					else if (listener instanceof MapChangeListener) {
						@SuppressWarnings("unchecked")
						MapChangeListener<? super String, ? super V> listen = (MapChangeListener<? super String, ? super V>) listener;
						removeListener(listen);
					}
				}
			}
			finally {
				clearingPluginOwned--;
			}
		}
	}
	
	@Override
	protected void doPut(String key, V value) {
		this.pluginMapped.put(key, value);
	}
	
	@Override
	protected void doRemove(String key, V value) throws PluginConflictException {
		if (clearingPluginOwned == 0)
			this.pluginMapped.remove(key);
	}
	
	/**
	 * @return an unmodifiable {@link Set} of the pluggable items in this {@link ObservablePluggableSourcedMap map} owned by the calling
	 *         {@link lipstone.joshua.parser.ParserPlugin plugin}
	 */
	public Set<String> getPluginOwned() {
		return Collections.unmodifiableSet(pluginMapped.keySet());
	}
}
