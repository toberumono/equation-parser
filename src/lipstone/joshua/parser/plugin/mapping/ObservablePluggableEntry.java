package lipstone.joshua.parser.plugin.mapping;

import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import lipstone.joshua.parser.exceptions.PluginConflictException;
import lipstone.joshua.parser.plugin.pluggable.Pluggable;

public abstract class ObservablePluggableEntry<K, V extends Pluggable> implements Entry<K, V> {
	
	protected final Entry<K, V> backingEntry;
	private final Logger logger;
	
	public ObservablePluggableEntry(Entry<K, V> backingEntry) {
		this.backingEntry = backingEntry;
		logger = Logger.getLogger("toberumono.parser.plugin.mapping.ObservablePluggableEntry");
	}
	
	@Override
	public K getKey() {
		return backingEntry.getKey();
	}
	
	@Override
	public V getValue() {
		return backingEntry.getValue();
	}
	
	/**
	 * {@inheritDoc}<br>
	 * <i>This method will fail silently if the {@link lipstone.joshua.parser.ParserPlugin plugin} calling it does not have
	 * permission to modify the value in this mapping. While this is not a problem in most circumstances, using
	 * {@link #trySetValue(Pluggable)} is advised.</i>
	 * 
	 * @see #trySetValue(Pluggable)
	 */
	@Override
	public V setValue(V value) {
		try {
			return trySetValue(value);
		}
		catch (PluginConflictException e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Failed to set the value mapped to " + backingEntry.getKey() + ".", e);
			return null;
		}
	}
	
	/**
	 * Attempts to replace the current value in this mapping with <tt>value</tt>.<br>
	 * This method ensures that the caller has permission to modify the mapping for both <tt>value</tt> and the
	 * {@link lipstone.joshua.parser.plugin.pluggable.Pluggable item} already in this mapping.
	 * 
	 * @param value
	 *            value to replace the current value in this mapping
	 * @return the previous value in this map
	 * @throws PluginConflictException
	 *             if the caller does not have permission to modify the mapping for either <tt>value</tt> or the
	 *             {@link lipstone.joshua.parser.plugin.pluggable.Pluggable item} already in this mapping
	 * @throws NullPointerException
	 *             if <tt>value</tt> is null
	 */
	public abstract V trySetValue(V value) throws PluginConflictException;
	
	@Override
	public final boolean equals(Object o) {
		if (!(o instanceof Map.Entry))
			return false;
		@SuppressWarnings("rawtypes")
		Map.Entry e = (Map.Entry) o;
		Object k1 = getKey();
		Object k2 = e.getKey();
		if (k1 == k2 || (k1 != null && k1.equals(k2))) {
			Object v1 = getValue();
			Object v2 = e.getValue();
			if (v1 == v2 || (v1 != null && v1.equals(v2)))
				return true;
		}
		return false;
	}
	
	@Override
	public final int hashCode() {
		return (getKey() == null ? 0 : getKey().hashCode()) ^ (getValue() == null ? 0 : getValue().hashCode());
	}
	
	@Override
	public final String toString() {
		return getKey() + "=" + getValue();
	}
}
