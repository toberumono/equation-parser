package lipstone.joshua.parser.plugin.mapping;

import java.util.Map;

import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.exceptions.PluginConflictException;
import lipstone.joshua.parser.exceptions.PluginInstantiationException;
import lipstone.joshua.parser.plugin.ParserPermissions;

public abstract class ObservableParserPluginMap extends ObservablePluggableSourcedMap<ParserPlugin> {
	
	public ObservableParserPluginMap(Map<String, ParserPlugin> backing, ParserPermissions permissions) {
		super(backing, permissions);
	}
	
	public ObservableParserPluginMap(ObservableParserPluginMap base, ParserPermissions permissions) {
		super(base, permissions);
	}
	
	/**
	 * Loads the {@link lipstone.joshua.parser.ParserPlugin plugin} via the given class.
	 * 
	 * @param plugin
	 *            the class from which to load the plugin
	 * @return null
	 * @throws PluginInstantiationException
	 *             if the plugin could not be instantiated
	 * @throws PluginConflictException
	 *             if one of the pluggable items added by the plugin conflicts with those loaded by another plugin
	 */
	public abstract ParserPlugin add(Class<? extends ParserPlugin> plugin) throws PluginInstantiationException, PluginConflictException;
	
	/**
	 * @return an iterator over the loaded plugin classes
	 */
	public abstract ObservablePluggableCollectionIterator<Class<ParserPlugin>> pluginClassIterator();
}
