package lipstone.joshua.parser.plugin.mapping;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.javafx.collections.MapListenerHelper;

import lipstone.joshua.parser.exceptions.PluginConflictException;
import lipstone.joshua.parser.plugin.ParserPermissions;
import lipstone.joshua.parser.plugin.pluggable.Pluggable;

import javafx.beans.InvalidationListener;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;

public class ObservablePluggableMap<V extends Pluggable> implements ObservableMap<String, V> {
	private ObservableEntrySet entrySet;
	private ObservableKeySet keySet;
	private ObservableValues values;
	
	protected final ObservablePluggableMapKernel<V> kernel;
	private final Map<String, V> backingMap;
	private final Logger logger;
	protected final HashMap<String, ObservablePluggableMap<V>> linkedMaps = new HashMap<>();
	private final ParserPermissions permissions;
	
	public ObservablePluggableMap(Map<String, V> backingMap, ParserPermissions permissions) {
		this(backingMap, new ObservablePluggableMapKernel<>(), permissions);
	}
	
	public ObservablePluggableMap(ObservablePluggableMap<V> base, ParserPermissions permissions) {
		this(base.backingMap, base.kernel, permissions);
	}
	
	public ObservablePluggableMap(Map<String, V> backingMap, ObservablePluggableMapKernel<V> kernel, ParserPermissions permissions) {
		String pluginID = permissions.getPlugin().getID();
		if (pluginID.equals("NONE"))
			logger = Logger.getLogger("toberumono.parser.ObservablePluggableMap");
		else
			logger = Logger.getLogger("toberumono.parser.plugin." + pluginID + ".ObservablePluggableMap");
		this.backingMap = backingMap;
		this.kernel = kernel;
		this.permissions = permissions;
	}
	
	private class SimpleChange extends MapChangeListener.Change<String, V> {
		
		private final String key;
		private final V old;
		private final V added;
		private final boolean wasAdded;
		private final boolean wasRemoved;
		
		public SimpleChange(String key, V old, V added, boolean wasAdded, boolean wasRemoved) {
			super(ObservablePluggableMap.this);
			this.key = key;
			this.old = old;
			this.added = added;
			this.wasAdded = wasAdded;
			this.wasRemoved = wasRemoved;
		}
		
		@Override
		public boolean wasAdded() {
			return wasAdded;
		}
		
		@Override
		public boolean wasRemoved() {
			return wasRemoved;
		}
		
		@Override
		public String getKey() {
			return key;
		}
		
		@Override
		public V getValueAdded() {
			return added;
		}
		
		@Override
		public V getValueRemoved() {
			return old;
		}
		
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			if (wasAdded) {
				if (wasRemoved)
					builder.append("replaced ").append(old).append(" with ").append(added);
				else
					builder.append("added ").append(added);
			}
			else
				builder.append("removed ").append(old);
			builder.append(" at key ").append(key);
			return builder.toString();
		}
		
	}
	
	@Override
	public int size() {
		return backingMap.size();
	}
	
	@Override
	public boolean isEmpty() {
		return backingMap.isEmpty();
	}
	
	@Override
	public boolean containsKey(Object key) {
		return backingMap.containsKey(key);
	}
	
	@Override
	public boolean containsValue(Object value) {
		return backingMap.containsValue(value);
	}
	
	/**
	 * Attempts to read the value mapped to <tt>key</tt>.<br>
	 * This method ensures that the caller has permission to read the mapping for <tt>key</tt>.
	 * 
	 * @param key
	 *            the key whose associated value is to be returned
	 * @return the value to which the specified key is mapped, or {@code null} if this map contains no mapping for the key
	 * @throws PluginConflictException
	 *             if the caller does not have permission to read the value mapped to <tt>key</tt>.
	 * @throws NullPointerException
	 *             if either <tt>key</tt> is {@code null}
	 */
	public V tryGet(Object key) throws PluginConflictException {
		if (key == null)
			throw new NullPointerException();
		permissions.validateRead("read", backingMap.get(key));
		return backingMap.get(key);
	}
	
	/**
	 * {@inheritDoc}<br>
	 * <i>This method will fail silently if the {@link lipstone.joshua.parser.ParserPlugin plugin} calling it does not have
	 * permission to read the <tt>value</tt> mapped to <tt>key</tt>. While this is not a problem for most purposes, using
	 * {@link #tryGet(Object) tryGet(key)} instead is advised.</i>
	 * 
	 * @see #tryGet(Object)
	 */
	@Override
	public V get(Object key) {
		try {
			return tryGet(key);
		}
		catch (PluginConflictException e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Failed to get the value mapped to " + key, e);
			return null;
		}
	}
	
	/**
	 * Attempts to put <tt>value</tt> into this {@link ObservablePluggableMap map} using <tt>key</tt>.<br>
	 * This method ensures that the caller has permission to modify the mapping for both <tt>value</tt> and the
	 * {@link lipstone.joshua.parser.plugin.pluggable.Pluggable item} paired with <tt>key</tt> (provided that this
	 * {@link ObservablePluggableMap map} already contains a mapping for <tt>key</tt>) before updating the
	 * {@link ObservablePluggableMap map}.
	 * 
	 * @param key
	 *            key with which the specified value is to be associated
	 * @param value
	 *            value to be associated with the specified key
	 * @return the previous value associated with <tt>key</tt>, or <tt>null</tt> if there was no mapping for <tt>key</tt>.
	 * @throws PluginConflictException
	 *             if the caller does not have permission to modify the mapping for both the
	 *             {@link lipstone.joshua.parser.plugin.pluggable.Pluggable item} that is being inserted into the map (
	 *             <tt>value</tt>) and the {@link lipstone.joshua.parser.plugin.pluggable.Pluggable item} that it is
	 *             replacing if applicable.
	 * @throws NullPointerException
	 *             if either <tt>key</tt> or <tt>value</tt> is null
	 */
	public V tryPut(String key, V value) throws PluginConflictException {
		if (key == null || value == null)
			throw new NullPointerException();
		V ret;
		if (backingMap.containsKey(key)) {
			permissions.validateModify("replace", backingMap.get(key));
			ret = backingMap.put(key, value);
			linkedMaps.get(key).doPut(key, value);
			if (ret == null && value != null || ret != null && !ret.equals(value))
				callObservers(new SimpleChange(key, ret, value, true, true));
		}
		else {
			permissions.validateModify("insert", value);
			ret = backingMap.put(key, value);
			linkedMaps.put(key, this);
			doPut(key, value);
			callObservers(new SimpleChange(key, ret, value, true, false));
		}
		return ret;
	}
	
	/**
	 * This is used to allow extending classes to perform additional actions before the observers are called when putting new
	 * mappings or replacing old ones.
	 * 
	 * @param key
	 *            the key being put
	 * @param value
	 *            the value being put
	 */
	protected void doPut(String key, V value) {}
	
	/**
	 * Attempts to put <tt>value</tt> into this {@link ObservablePluggableMap map}.<br>
	 * This method ensures that the caller has permission to modify the mapping for both <tt>value</tt> and the
	 * {@link lipstone.joshua.parser.plugin.pluggable.Pluggable item} paired with <tt>key</tt> (provided that this
	 * {@link ObservablePluggableMap map} already contains a mapping for <tt>key</tt>) before updating the
	 * {@link ObservablePluggableMap map}.<br>
	 * This is a convenience method for {@link #tryPut(String, Pluggable)} because by definition {@link Pluggable pluggable
	 * objects} have a name field, which should be used as its <tt>key</tt> in this {@link ObservablePluggableMap map}.
	 * 
	 * @param value
	 *            {@link Pluggable pluggable} item to be added
	 * @return the previous value associated with <tt>value</tt>'s <tt>name</tt> (derived from
	 *         {@link lipstone.joshua.parser.plugin.pluggable.Pluggable#getName() Pluggable.getName()}), or <tt>null</tt> if
	 *         there was no mapping for <tt>key</tt>.
	 * @throws PluginConflictException
	 *             if the caller does not have permission to modify the mapping for both the
	 *             {@link lipstone.joshua.parser.plugin.pluggable.Pluggable item} that is being inserted into the map (
	 *             <tt>value</tt>) and the {@link lipstone.joshua.parser.plugin.pluggable.Pluggable item} that it is
	 *             replacing if applicable.
	 * @throws NullPointerException
	 *             if <tt>value</tt> is null
	 */
	public V add(V value) throws PluginConflictException {
		if (value == null)
			throw new NullPointerException();
		return tryPut(value.getName(), value);
	}
	
	/**
	 * {@inheritDoc}<br>
	 * <i>This method will fail silently if the {@link lipstone.joshua.parser.ParserPlugin plugin} calling it does not have
	 * permission to insert <tt>value</tt> or remove the old value denoted by <tt>key</tt>. While this is not a problem for
	 * most purposes, using {@link #tryPut(String, Pluggable) tryPut(key, value)} instead is advised.</i>
	 * 
	 * @see #tryPut(String, Pluggable) tryPut(key, value)
	 */
	@Override
	public V put(String key, V value) {
		try {
			return tryPut(key, value);
		}
		catch (PluginConflictException e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Failed to map " + key + " -> " + value, e);
			return null;
		}
	}
	
	/**
	 * Attempts to remove the entry denoted by <tt>key</tt> from this {@link ObservablePluggableMap map}.<br>
	 * This method ensures that the caller has permission to modify the mapping for the
	 * {@link lipstone.joshua.parser.plugin.pluggable.Pluggable item} that is being removed.
	 * 
	 * @param key
	 *            key whose mapping is to be removed from the map
	 * @return the previous value associated with <tt>key</tt>, or <tt>null</tt> if there was no mapping for <tt>key</tt>.
	 * @throws PluginConflictException
	 *             if the caller does not have permission to modify the mapping for the
	 *             {@link lipstone.joshua.parser.plugin.pluggable.Pluggable item} that is being removed
	 * @throws NullPointerException
	 *             if <tt>key</tt> is null
	 */
	public V tryRemove(String key) throws PluginConflictException {
		if (key == null)
			throw new NullPointerException();
		if (!backingMap.containsKey(key))
			return null;
		permissions.validateModify("remove", backingMap.get(key));
		V ret = backingMap.remove(key);
		linkedMaps.get(key).doRemove(key, ret);
		callObservers(new SimpleChange((String) key, ret, null, false, true));
		return ret;
	}
	
	/**
	 * This is used to allow extending classes to perform additional actions before the observers are called when removing
	 * (but /not/ replacing) mappings.
	 * 
	 * @param key
	 *            the key being removed
	 * @param value
	 *            the value being removed
	 * @throws PluginConflictException
	 *             for exception propagation
	 */
	protected void doRemove(String key, V value) throws PluginConflictException {}
	
	/**
	 * {@inheritDoc}<br>
	 * <i>This method will fail silently if the {@link lipstone.joshua.parser.ParserPlugin plugin} calling it does not have
	 * permission to remove the value denoted by the given key. While this is not a problem for most purposes, using
	 * {@link #tryRemove(String) tryRemove(key)} instead is advised.</i>
	 * 
	 * @see #tryRemove(String) tryRemove(key)
	 */
	@Override
	public V remove(Object key) {
		try {
			return tryRemove((String) key);
		}
		catch (PluginConflictException e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Failed to remove the value mapped to " + key, e);
			return null;
		}
	}
	
	/**
	 * Use of this method is <i>highly</i> inadvisable. It will fail silently if any of the put operations fail and given no
	 * indication of how much the {@link ObservablePluggableMap map} was modified.<br>
	 * {@inheritDoc}
	 */
	@Override
	@Deprecated
	public void putAll(Map<? extends String, ? extends V> m) {
		for (Map.Entry<? extends String, ? extends V> e : m.entrySet())
			put(e.getKey(), e.getValue());
	}
	
	/**
	 * Attempts to clear this {@link ObservablePluggableMap map}.<br>
	 * This method ensures that the caller has permission to either clear this {@link ObservablePluggableMap map} or remove
	 * every mapping from it.
	 * 
	 * @throws PluginConflictException
	 *             if the caller does not have permission to either clear this {@link ObservablePluggableMap map} or remove
	 *             every mapping from it.
	 */
	public void tryClear() throws PluginConflictException {
		try {
			//If the caller has permission to clear this map, go ahead and clear it.
			permissions.validateSpecial("clear", this);
			doClear();
		}
		catch (PluginConflictException e) {
			//If the caller does not have permission to clear this map, but does have permission to remove every mapping from this map, do that.
			for (Iterator<Entry<String, V>> i = backingMap.entrySet().iterator(); i.hasNext();)
				permissions.validateModify("remove", i.next().getValue());
			doClear();
		}
	}
	
	/**
	 * {@inheritDoc}<br>
	 * <i>This method will fail silently if the caller does not either have permission to clear this collection or remove
	 * every element within it. Using {@link #tryClear()} instead is advised</i>
	 */
	@Override
	public void clear() {
		try {
			tryClear();
		}
		catch (PluginConflictException e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Failed to clear the map.", e);
		}
	}
	
	private void doClear() throws PluginConflictException {
		for (Iterator<Entry<String, V>> i = backingMap.entrySet().iterator(); i.hasNext();) {
			Entry<String, V> e = i.next();
			String key = e.getKey();
			V val = e.getValue();
			i.remove();
			linkedMaps.get(key).doRemove(key, val);
			callObservers(new SimpleChange(key, val, null, false, true));
		}
	}
	
	@Override
	public ObservableKeySet keySet() {
		return keySet == null ? keySet = new ObservableKeySet() : keySet;
	}
	
	@Override
	public ObservableValues values() {
		return values == null ? values = new ObservableValues() : values;
	}
	
	@Override
	public ObservableEntrySet entrySet() {
		return entrySet == null ? entrySet = new ObservableEntrySet() : entrySet;
	}
	
	protected void callObservers(MapChangeListener.Change<String, V> change) {
		MapListenerHelper.fireValueChangedEvent(kernel.listenerHelper, change);
	}
	
	@Override
	public void addListener(InvalidationListener listener) {
		synchronized (kernel) {
			kernel.listenerHelper = MapListenerHelper.addListener(kernel.listenerHelper, listener);
		}
	}
	
	@Override
	public void removeListener(InvalidationListener listener) {
		synchronized (kernel) {
			kernel.listenerHelper = MapListenerHelper.removeListener(kernel.listenerHelper, listener);
		}
	}
	
	@Override
	public void addListener(MapChangeListener<? super String, ? super V> listener) {
		synchronized (kernel) {
			kernel.listenerHelper = MapListenerHelper.addListener(kernel.listenerHelper, listener);
		}
	}
	
	@Override
	public void removeListener(MapChangeListener<? super String, ? super V> listener) {
		synchronized (kernel) {
			kernel.listenerHelper = MapListenerHelper.removeListener(kernel.listenerHelper, listener);
		}
	}
	
	@FunctionalInterface
	interface Returner<E> {
		public E get();
	}
	
	abstract class ObservablePluggableCollectionHelper<E> extends ObservablePluggableCollection<E> {
		Returner<Collection<E>> backingCollection;
		
		ObservablePluggableCollectionHelper(Returner<Collection<E>> backingCollection) {
			super(backingCollection);
		}
		
		@Override
		public int size() {
			return backingMap.size();
		}
		
		@Override
		public boolean isEmpty() {
			return backingMap.isEmpty();
		}
		
		/**
		 * This method will fail silently if the {@link lipstone.joshua.parser.ParserPlugin plugin} calling it does not have
		 * permission to remove the value mapped to <tt>key</tt>. Using {@link #tryRemove(Object) tryRemove(key)} instead is
		 * advised.
		 * 
		 * @see #tryRemove(Object)
		 */
		@Override
		@SuppressWarnings("unchecked")
		public boolean remove(Object o) {
			try {
				return tryRemove((E) o);
			}
			catch (PluginConflictException e) {
				e.printStackTrace();
				logger.log(Level.SEVERE, "Failed to remove the value mapped to " + o, e);
				return false;
			}
		}
		
		/**
		 * {@inheritDoc}<br>
		 * <i>This method will fail silently if the caller does not either have permission to clear this collection or remove
		 * every element within it. Using {@link #tryClear()} instead is advised.</i>
		 */
		@Override
		public void clear() {
			ObservablePluggableMap.this.clear();
		}
		
		/**
		 * Attempts to clear this collection.<br>
		 * This method ensures that the caller has permission to either clear this collection or remove every mapping from
		 * it.
		 * 
		 * @throws PluginConflictException
		 *             if the caller does not have permission to either clear this collection or remove every mapping from
		 *             it.
		 */
		public void tryClear() throws PluginConflictException {
			ObservablePluggableMap.this.tryClear();
		}
		
		@Override
		public String toString() {
			return backingCollection.get().toString();
		}
		
		@Override
		public boolean equals(Object obj) {
			return backingCollection.get().equals(obj);
		}
		
		@Override
		public int hashCode() {
			return backingCollection.get().hashCode();
		}
	}
	
	abstract class ObservablePluggableCollectionIteratorHelper<E> extends ObservablePluggableCollectionIterator<E> {
		Iterator<Entry<String, V>> entryIt = backingMap.entrySet().iterator();
		String lastKey;
		V lastValue;
		
		@Override
		public boolean hasNext() {
			return entryIt.hasNext();
		}
		
		@Override
		public void tryRemove() throws PluginConflictException {
			permissions.validateModify("remove", lastValue);
			entryIt.remove();
			linkedMaps.get(lastKey).doRemove(lastKey, lastValue);
			//ObservablePluggableMap.this.doRemove(lastKey, lastValue);
			callObservers(new SimpleChange(lastKey, lastValue, null, false, true));
		}
	}
	
	public class ObservableKeySet extends ObservablePluggableCollectionHelper<String> implements Set<String> {
		
		ObservableKeySet() {
			super(backingMap::keySet);
		}
		
		@Override
		public boolean tryRemove(String e) throws PluginConflictException {
			return ObservablePluggableMap.this.tryRemove(e) != null;
		}
		
		@Override
		public ObservablePluggableCollectionIterator<String> iterator() {
			return new ObservablePluggableCollectionIteratorHelper<String>() {
				@Override
				public String next() {
					Entry<String, V> last = entryIt.next();
					lastKey = last.getKey();
					lastValue = last.getValue();
					return last.getKey();
				}
			};
		}
		
		@Override
		protected boolean removeRetain(Collection<?> c, boolean remove) {
			synchronized (kernel) {
				List<Entry<String, V>> removed = new ArrayList<>();
				for (Iterator<Entry<String, V>> i = backingMap.entrySet().iterator(); i.hasNext();) {
					Entry<String, V> e = i.next();
					if (remove == c.contains(e.getKey())) {
						try {
							permissions.validateModify("remove", e.getValue());
							removed.add(e);
							i.remove();
						}
						catch (PluginConflictException e1) {
							e1.printStackTrace();
							logger.log(Level.SEVERE, "Failed to remove the value mapped to " + e.getKey(), e1);
							for (Entry<String, V> r : removed)
								backingMap.put(r.getKey(), r.getValue());
							return false;
						}
					}
				}
				for (Entry<String, V> e : removed) {
					try {
						linkedMaps.get(e.getKey()).doRemove(e.getKey(), e.getValue());
						ObservablePluggableMap.this.doRemove(e.getKey(), e.getValue());
						callObservers(new SimpleChange(e.getKey(), e.getValue(), null, false, true));
					}
					catch (PluginConflictException e1) {
						e1.printStackTrace();
					}
				}
				return removed.size() > 0;
			}
		}
	}
	
	public class ObservableValues extends ObservablePluggableCollectionHelper<V> {
		
		ObservableValues() {
			super(backingMap::values);
		}
		
		@Override
		public ObservablePluggableCollectionIterator<V> iterator() {
			return new ObservablePluggableCollectionIteratorHelper<V>() {
				@Override
				public V next() {
					Entry<String, V> last = entryIt.next();
					lastKey = last.getKey();
					lastValue = last.getValue();
					return last.getValue();
				}
			};
		}
		
		@Override
		protected boolean removeRetain(Collection<?> c, boolean remove) {
			synchronized (kernel) {
				List<Entry<String, V>> removed = new ArrayList<>();
				for (Iterator<Entry<String, V>> i = backingMap.entrySet().iterator(); i.hasNext();) {
					Entry<String, V> e = i.next();
					if (remove == c.contains(e.getValue())) {
						try {
							permissions.validateModify("remove", e.getValue());
							removed.add(e);
							i.remove();
						}
						catch (PluginConflictException e1) {
							e1.printStackTrace();
							logger.log(Level.SEVERE, "Failed to remove the value mapped to " + e.getKey(), e1);
							for (Entry<String, V> r : removed)
								backingMap.put(r.getKey(), r.getValue());
							return false;
						}
					}
				}
				for (Entry<String, V> e : removed) {
					try {
						linkedMaps.get(e.getKey()).doRemove(e.getKey(), e.getValue());
						ObservablePluggableMap.this.doRemove(e.getKey(), e.getValue());
						callObservers(new SimpleChange(e.getKey(), e.getValue(), null, false, true));
					}
					catch (PluginConflictException e1) {
						e1.printStackTrace();
					}
				}
				return removed.size() > 0;
			}
		}
		
		/**
		 * Removal from the {@link ObservablePluggableMap.ObservableValues ObservableValues} collection is unsupported.
		 * 
		 * @throws UnsupportedOperationException
		 *             this operation is unsupported, so it always throws this exception
		 */
		@Override
		public boolean remove(Object e) throws UnsupportedOperationException {
			throw new UnsupportedOperationException("Cannot remove from the ObservableValues collection in ObservablePluggableMap.");
		}
		
		/**
		 * Removal from the {@link ObservablePluggableMap.ObservableValues ObservableValues} collection is unsupported.
		 * 
		 * @throws UnsupportedOperationException
		 *             this operation is unsupported, so it always throws this exception
		 */
		@Override
		public boolean tryRemove(V e) throws UnsupportedOperationException {
			throw new UnsupportedOperationException("Cannot remove from the ObservableValues collection in ObservablePluggableMap.");
		}
	}
	
	class ObservablePluggableEntryHelper extends ObservablePluggableEntry<String, V> {
		public ObservablePluggableEntryHelper(Entry<String, V> backingEntry) {
			super(backingEntry);
		}
		
		@Override
		public V trySetValue(V value) throws PluginConflictException {
			if (value == null)
				throw new NullPointerException();
			permissions.validateModify("replace", backingEntry.getValue());
			V oldValue = backingEntry.setValue(value);
			callObservers(new SimpleChange(getKey(), oldValue, value, true, true));
			return oldValue;
		}
	}
	
	private class ObservablePluggableCollectionEntryIteratorHelper extends ObservablePluggableCollectionIteratorHelper<Entry<String, V>> {

		@Override
		public ObservablePluggableEntry<String, V> next() {
			Entry<String, V> last = entryIt.next();
			lastKey = last.getKey();
			lastValue = last.getValue();
			return new ObservablePluggableEntryHelper(last);
		}
	}
	
	public class ObservableEntrySet extends ObservablePluggableCollectionHelper<Entry<String, V>> implements Set<Entry<String, V>> {
		
		ObservableEntrySet() {
			super(backingMap::entrySet);
		}
		
		@Override
		public ObservablePluggableCollectionIterator<Entry<String, V>> iterator() {
			return new ObservablePluggableCollectionEntryIteratorHelper();
		}
		
		@Override
		protected boolean removeRetain(Collection<?> c, boolean remove) {
			synchronized (kernel) {
				List<Entry<String, V>> removed = new ArrayList<>();
				for (Iterator<Entry<String, V>> i = backingMap.entrySet().iterator(); i.hasNext();) {
					Entry<String, V> e = i.next();
					if (remove == c.contains(e)) {
						try {
							permissions.validateModify("remove", e.getValue());
							removed.add(e);
							i.remove();
						}
						catch (PluginConflictException e1) {
							e1.printStackTrace();
							logger.log(Level.SEVERE, "Failed to remove the value mapped to " + e.getKey(), e1);
							for (Entry<String, V> r : removed)
								backingMap.put(r.getKey(), r.getValue());
							return false;
						}
					}
				}
				for (Entry<String, V> e : removed) {
					try {
						linkedMaps.get(e.getKey()).doRemove(e.getKey(), e.getValue());
						ObservablePluggableMap.this.doRemove(e.getKey(), e.getValue());
						callObservers(new SimpleChange(e.getKey(), e.getValue(), null, false, true));
					}
					catch (PluginConflictException e1) {
						e1.printStackTrace();
					}
				}
				return removed.size() > 0;
			}
		}
		
		@Override
		public boolean tryRemove(Entry<String, V> e) throws PluginConflictException {
			permissions.validateModify("remove", e.getValue());
			boolean ret = backingMap.entrySet().remove(e);
			if (ret) {
				linkedMaps.get(e.getKey()).doRemove(e.getKey(), e.getValue());
				callObservers(new SimpleChange(e.getKey(), e.getValue(), null, false, true));
			}
			return ret;
		}
	}
}

/**
 * This is used to allow the listeners to be sync'd across multiple wrappings of the same map.
 * 
 * @author Toberumono
 */
class ObservablePluggableMapKernel<V> {
	MapListenerHelper<String, V> listenerHelper;
}
