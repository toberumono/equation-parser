package lipstone.joshua.parser.plugin.mapping;

import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import lipstone.joshua.parser.exceptions.PluginConflictException;

public abstract class ObservablePluggableCollectionIterator<E> implements Iterator<E> {
	protected final Logger logger;
	
	/**
	 * Constructs an {@link ObservablePluggableCollectionIterator} with its default {@link Logger} (package name.class name).
	 */
	public ObservablePluggableCollectionIterator() {
		logger = Logger.getLogger("toberumono.parser.plugin.mapping.ObservablePluggableCollectionIterator");
	}
	
	/**
	 * Constructs an {@link ObservablePluggableCollectionIterator} with the given {@link Logger}.
	 * 
	 * @param logger
	 *            the {@link Logger} to use
	 */
	public ObservablePluggableCollectionIterator(Logger logger) {
		this.logger = logger;
	}
	
	@Override
	public abstract boolean hasNext();
	
	/**
	 * Attempts to remove the last-seen entry from the over which this is iterating.<br>
	 * This method ensures that the caller has permission to modify the mapping for the
	 * {@link lipstone.joshua.parser.plugin.pluggable.Pluggable item} that is being removed.<br>
	 * This method can be called only once per call to {@link #next()}. The behavior of this iterator is unspecified if the
	 * underlying collection is modified while the iteration is in progress in any way other than by calling this method.
	 * 
	 * @throws PluginConflictException
	 *             if the caller does not have permission to modify the mapping for the
	 *             {@link lipstone.joshua.parser.plugin.pluggable.Pluggable item} that is being removed
	 */
	public abstract void tryRemove() throws PluginConflictException;
	
	/**
	 * {@inheritDoc}<br>
	 * <i>This method will fail silently if the {@link lipstone.joshua.parser.ParserPlugin plugin} calling it does not have
	 * permission to remove the current item.While this is not a problem for most purposes, using {@link #tryRemove()}
	 * instead is advised.</i>
	 * 
	 * @see #tryRemove()
	 */
	@Override
	public void remove() {
		try {
			tryRemove();
		}
		catch (PluginConflictException e) {
			logger.log(Level.SEVERE, "Failed to remove an element.", e);
		}
	}
}
