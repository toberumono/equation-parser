package lipstone.joshua.parser.cas;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import lipstone.joshua.parser.Parser;
import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.plugin.pluggable.Operator;
import lipstone.joshua.parser.tokenizer.ConsCell;

import static lipstone.joshua.parser.tokenizer.ConsType.*;

public class Solution {
	Map<ConsCell, ConsCell> variables;
	
	public Solution() {
		variables = new HashMap<>();
	}
	
	public Solution(ConsCell variable, ConsCell value) {
		this();
		variables.put(variable, value);
	}
	
	public Solution(Map<ConsCell, ConsCell> variables) {
		this.variables = variables;
	}
	
	public boolean check(Problem problem, Parser parser) throws ParserException {
		return true;//TODO add tolerance to: parser.evaluate(replaceVars(problem.equation, parser)).equals(new Token(BigDec.ZERO, NUMBER));
	}
	
	public ConsCell replaceVars(ConsCell input, Operator multiplication, Operator power) {
		ConsCell out = new ConsCell(), head = out;
		for (ConsCell t : input) {
			if (variables.containsKey(t))
				t = variables.get(t);
			ConsCell rep = t.getCar() instanceof ConsCell ? replaceVars((ConsCell) t.getCar(), multiplication, power) : t.clone();
			if (rep.length() > 1)
				rep = new ConsCell(rep, PARENTHESES);
			head = head.append(rep);
		}
		return out;
	}
	
	public ConsCell replaceVars(List<Term> input, Parser parser) {
		ConsCell out = new ConsCell(), head = out;
		Operator addition = parser.operators.get("+"), multiplication = parser.operators.get("*"), power = parser.operators.get("^");
		for (Term t : input) {
			head = head.append(new ConsCell(addition, OPERATOR));
			for (ConsCell to : t.toConsCellTree(multiplication, power)) {
				if (!variables.containsKey(to)) {
					head = head.append(to);
					continue;
				}
				to = variables.get(to);
				ConsCell rep = to.getCar() instanceof ConsCell ? replaceVars((ConsCell) to.getCar(), multiplication, power) : to.clone();
				head = head.append(rep.length() > 1 ? new ConsCell(rep, PARENTHESES) : rep);
			}
		}
		return out;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Solution))
			return false;
		Solution s = (Solution) o;
		if (s.variables.size() != variables.size())
			return false;
		for (Entry<ConsCell, ConsCell> e : variables.entrySet())
			if (!s.variables.containsKey(e.getKey()) || !s.variables.get(e.getKey()).equals(e.getValue()))
				return false;
		return true;
	}
	
	@Override
	public String toString() {
		if (variables.size() == 0)
			return "";
		StringBuilder sb = new StringBuilder();
		for (Entry<ConsCell, ConsCell> v : variables.entrySet())
			sb.append(v.getKey()).append(" = ").append(v.getValue()).append(", ");
		return sb.toString().substring(0, sb.length() - 2);
	}
}
