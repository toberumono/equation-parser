package lipstone.joshua.parser.cas;

import java.util.List;

import lipstone.joshua.parser.exceptions.ParserException;

/**
 * Represents a method for solving a {@link Problem} with the {@link CAS}
 * 
 * @author Joshua Lipstone
 */
@FunctionalInterface
public interface SolverMethod {
	public List<Solution> apply(Problem p) throws ParserException;
}
