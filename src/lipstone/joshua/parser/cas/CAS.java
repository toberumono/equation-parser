package lipstone.joshua.parser.cas;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.locks.ReentrantLock;

import toberumono.structures.tuples.Pair;
import toberumono.utils.files.NativeLibraryManager;

import lipstone.joshua.parser.Parser;
import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.exceptions.UndefinedResultException;
import lipstone.joshua.parser.plugin.pluggable.Operator;
import lipstone.joshua.parser.tokenizer.ConsCell;
import lipstone.joshua.parser.types.BigDec;
import lipstone.joshua.parser.util.MathFunction;

import static lipstone.joshua.parser.tokenizer.ConsType.*;
import static lipstone.joshua.parser.types.BigDec.*;

/**
 * The Computer Algebra System for this library.
 * 
 * @author Joshua Lipstone
 */
public class CAS {
	
	static {
		NativeLibraryManager.loadLibrary("CAS");
		primesLock = new ReentrantLock();
		cachePrimes(10000000l);
	}
	
	private final Parser parser;
	
	private static long primesLimit;
	
	private static long[] primes;
	private static final ReentrantLock primesLock;
	public static final ConsCell one = new ConsCell(ONE, NUMBER);
	
	public CAS(Parser parser) {
		this.parser = parser;
	}
	
	public ConsCell solve(ConsCell input) throws ParserException {
		List<Term> inp = condenseTerms(makeEqualZero(input));
		Problem problem = new Problem(inp, determineVariables(input));
		for (Solver s : parser.casSolvers.values())
			if (s.isApplicable(problem))
				problem.attempt(s);
		problem.checkWork(parser);
		if (problem.solutions.size() == 0)
			return new ConsCell("No solution", RETURN);
		String out = problem.solutions.toString();
		return new ConsCell(out.substring(1, out.length() - 1), RETURN);
	}
	
	public List<Term> makeEqualZero(ConsCell input) throws ParserException {
		Pair<ConsCell, ConsCell> eqn = splitEquation(input);
		List<Term> out = toTerms(eqn.getX());
		out.addAll(invertSign(toTerms(eqn.getY())));
		return out;
	}
	
	public static Pair<ConsCell, ConsCell> splitEquation(ConsCell input) {
		ConsCell left = new ConsCell(), right = new ConsCell(), head = left;
		for (; !input.isNull() && !input.getCar().equals("="); input = input.getNextConsCell())
			head = head.append(input.singular());
		head = right;
		for (input = input.getNextConsCell(); !input.isNull(); input = input.getNextConsCell())
			head = head.append(input.singular());
		if (!right.checkLength(1))
			right = new ConsCell(ZERO, NUMBER);
		return new Pair<>(left, right);
	}
	
	/**
	 * This method assumes that there is at least one non-empty {@link lipstone.joshua.parser.tokenizer.ConsCell Token} being
	 * passed.
	 * 
	 * @param input
	 *            the {@link lipstone.joshua.parser.tokenizer.ConsCell Token} tree to extract the terms from
	 * @return a list of the {@link lipstone.joshua.parser.cas.Term Terms} found within the tree, non-recursive
	 * @throws ParserException
	 *             for exception propagation
	 */
	public List<Term> toTerms(ConsCell input) throws ParserException {
		List<Term> terms = new ArrayList<>();
		Term current = new Term();
		Operator addition = parser.operators.get("+"), subtraction = parser.operators.get("-");
		if (input.getCarType() == OPERATOR) {
			if (input.getCar().equals("-"))
				current.coefficient = current.coefficient.multiply(MINUSONE);
			input = input.getNextConsCell();
		}
		do {
			if (input.getCarType() == OPERATOR) {
				if (input.getCar().equals("+") || input.getCar().equals("-")) {
					if (!current.coefficient.eq(ZERO))
						terms.add(current);
					current = new Term();
					if (input.getCar().equals("-"))
						current.coefficient = current.coefficient.multiply(MINUSONE);
				}
				else {
					if (input.getCar().equals("*")) {
						while ((input = input.getNextConsCell()).getCarType() == OPERATOR)
							if (input.getCar().equals("-"))
								current.coefficient = current.coefficient.multiply(MINUSONE);
						input = toTermsInner(current, input, (curr, mult) -> curr.multiply(mult), addition);
					}
					else if (input.getCar().equals("/")) {
						while ((input = input.getNextConsCell()).getCarType() == OPERATOR)
							if (input.getCar().equals("-"))
								current.coefficient = current.coefficient.multiply(MINUSONE);
						input = toTermsInner(current, input, (curr, div) -> curr.divide(div), subtraction);
					}
				}
			}
			else
				input = toTermsInner(current, input, (curr, mult) -> curr.multiply(mult), addition);
		} while (!(input = input.getNextConsCell()).isNull());
		if (!current.coefficient.eq(ZERO))
			terms.add(current);
		return terms;
	}
	
	//This returns the input advanced to the appropriate location
	private ConsCell toTermsInner(Term current, ConsCell input, MathFunction op, Operator powerOperator) throws ParserException {
		ConsCell exp = new ConsCell(), head = exp, key = input.singular();
		if (input.getCarType() == OPERATION)
			key.append((input = input.getNextConsCell()).singular());
		ConsCell next = input.getNextConsCell();
		if (next.isNull() || next.getCarType() != OPERATOR || !next.getCar().equals("^"))
			exp = new ConsCell(ONE, NUMBER);
		else {
			input = next.getNextConsCell(); //Step forward to first token of exponent
			do
				head = head.append(input.singular());
			while (!(input = input.getNextConsCell()).isNull() && (input.getCarType() != OPERATOR || input.getCar().equals("^")));
			input = input.getPreviousConsCell();
		}
		if (key.getCarType() == NUMBER && !key.hasNextConsCell()) {
			ConsCell co = key.singular();
			if (!exp.equals(one)) {
				co.append(new ConsCell(parser.operators.get("^"), OPERATOR, exp, TOKEN));
				co = parser.evaluate(co);
			}
			if (co.getCarType() == NUMBER && !co.hasNextConsCell()) {
				current.coefficient = op.calculate(current.coefficient, (BigDec) co.getCar());
				return input;
			}
		}
		if (current.tails.containsKey(key))
			current.tails.put(key, current.tails.get(key).append(new ConsCell(powerOperator, OPERATOR, exp, TOKEN)));
		else {
			current.variables.put(key, exp);
			current.tails.put(key, exp.getLastConsCell());
		}
		return input;
	}
	
	/**
	 * Takes the list of {@link Term terms} and converts it back into the simplest
	 * {@link lipstone.joshua.parser.tokenizer.ConsCell Token} tree that would generate it.
	 * 
	 * @param input
	 *            the {@link Term terms} to convert
	 * @return the generated {@link lipstone.joshua.parser.tokenizer.ConsCell Token} tree
	 */
	public ConsCell toConsCellTree(List<Term> input) {
		Operator addition = parser.operators.get("+"), subtraction = parser.operators.get("-"), multiplication = parser.operators.get("*"), power = parser.operators.get("^");
		if (input.size() == 0)
			return new ConsCell();
		ConsCell out = new ConsCell(), head = out;
		for (Term t : input) {
			if (t.coefficient.lt(ZERO)) {
				ConsCell temp = t.toConsCellTree(multiplication, power);
				BigDec co = ((BigDec) temp.getCar()).multiply(MINUSONE);
				temp.replaceCar(co, NUMBER);
				head = head.append(new ConsCell(subtraction, OPERATOR, temp, TOKEN));
			}
			else
				head = head.append(new ConsCell(addition, OPERATOR, t.toConsCellTree(multiplication, power), TOKEN));
		}
		return out.getNextConsCell();
	}
	
	public List<Term> condenseTerms(List<Term> terms) throws ParserException {
		List<Term> output = new ArrayList<>();
		for (Term term : terms)
			output.add(term.condense(parser));
		return output;
	}
	
	/**
	 * This method assumes that {@link #simplifyTerms(List)} has already been called.
	 * 
	 * @param terms
	 *            the {@link lipstone.joshua.parser.cas.Term terms} to sort
	 * @param sortBy
	 *            the bases whose powers will be used to sort <tt>terms</tt> in order of importance
	 * @return a sorted copy of the list of {@link lipstone.joshua.parser.cas.Term terms}
	 * @see Term#condense(Parser)
	 * @see #simplifyTerms(List)
	 */
	public static List<Term> sortTerms(List<Term> terms, ConsCell... sortBy) {
		List<Term> output = new ArrayList<>();
		for (int i = 0; i < terms.size(); i++) {
			int j = 0;
			for (; j < output.size(); j++) {
				boolean stop = false;
				for (ConsCell base : sortBy) {
					ConsCell t = terms.get(i).variables.get(base), o = output.get(j).variables.get(base);
					if (t == null) {
						if (o != null) {
							stop = true;
							break;
						}
					}
					else {
						if (o == null)
							break;
						else {
							int comp = output.get(j).variables.get(base).compareTo(terms.get(i).variables.get(base));
							if (comp < 0)
								break;
							else if (comp > 0) {
								stop = true;
								break;
							}
						}
					}
				}
				if (stop)
					break;
			}
			output.add(j, terms.get(i));
		}
		return terms;
	}
	
	private ConsCell getExponent(ConsCell input) {
		if ((input = input.getNextConsCell()).isNull() || input.getCarType() != OPERATOR || !input.getCar().equals("^"))
			return new ConsCell(ONE, NUMBER);
		ConsCell exp = new ConsCell(), head = exp;
		input = input.getNextConsCell(); //Step forward to first token of exponent
		do
			head = head.append(input.singular());
		while (!(input = input.getNextConsCell()).isNull() && (input.getCarType() != OPERATOR || input.getCar().equals("^")));
		return exp;
	}
	
	public List<Term> distribute(List<Term> left, List<Term> right) throws ParserException {
		List<Term> out = new ArrayList<>();
		for (Term l : left)
			for (Term r : right)
				out.add(l.multiply(r, parser));
		return out;
	}
	
	/**
	 * Convenience method for calling {@link #simplifyTerms(List)} on a {@link lipstone.joshua.parser.tokenizer.ConsCell
	 * token} tree.
	 * 
	 * @param input
	 *            the {@link lipstone.joshua.parser.tokenizer.ConsCell token} tree to simplify
	 * @return the simplified {@link lipstone.joshua.parser.tokenizer.ConsCell token} tree
	 * @throws ParserException
	 *             for exception propagation purposes
	 * @see #simplifyTerms(List)
	 */
	public ConsCell simplifyTokenTree(ConsCell input) throws ParserException {
		return toConsCellTree(simplifyTerms(toTerms(input)));
	}
	
	/**
	 * Simplifies the given {@link Term terms} as much as possible. This includes recursively applying this function to all
	 * of the base-exponent pairs in all of the {@link Term terms}.<br>
	 * <i>This function is slow relative to the evaluation functions in this library. Therefore, it is advisable to only call
	 * this function when necessary (e.g. immediately before final output).</i><br>
	 * <i>NOTE</i>: This method does <i>not</i> modify the input {@link Term terms} list.
	 * 
	 * @param terms
	 *            the {@link Term terms} to simplify
	 * @return the simplified {@link Term terms}
	 * @throws ParserException
	 *             for exception propagation purposes
	 */
	public List<Term> simplifyTerms(List<Term> terms) throws ParserException {
		List<Term> output = new ArrayList<>();
		for (Term term : terms) {
			Term t = term.condense(parser); //This term is only referenced here.  Therefore, we can safely modify it directly.
			if (t.coefficient.eq(ZERO) && t.variables.size() == 0)
				continue;
			output.add(t);
		}
		//All of the terms in output are guaranteed to only be referenced by index in output.  Therefore, we can safely modify them directly.
		for (int i = 0; i < output.size(); i++) {
			Term t = output.get(i);
			for (int j = i + 1; j < output.size(); j++) {
				Term o = output.get(j);
				if (o.variables.size() != t.variables.size())
					continue;
				boolean equal = true;
				for (Entry<ConsCell, ConsCell> base : t.variables.entrySet())
					if (o.variables.get(base.getKey()) == null || !o.variables.get(base.getKey()).equals(base.getValue())) {
						equal = false;
						break;
					}
				if (equal) {
					t.coefficient = t.coefficient.add(o.coefficient);
					output.remove(j--);
				}
			}
		}
		if (output.size() == 0)
			output.add(new Term(ZERO));
		return output;
	}
	
	public static boolean canDropParentheses(ConsCell left, ConsCell parentheses, ConsCell right) {
		ConsCell paren = (ConsCell) parentheses.getCar();
		if (!paren.checkLength(2)) //If the parenthesized token tree has a length of 0 or 1
			return true;
		if ((left.getCar() != null && (left.getCar().equals("^") || left.getCar().equals("%") || left.getCar().equals("/"))) ||
				(right.getCar() != null && (right.getCar().equals("^") || right.getCar().equals("%"))))
			return false;
		if (isOneTerm(paren))
			return true;
		return left.getCar() == null || !left.getCar().equals("-");
	}
	
	/**
	 * This removes all excess parentheses from the given {@link lipstone.joshua.parser.tokenizer.ConsCell Token} tree.<br>
	 * This is achieved by modifying the depth of elements within the tree.<br>
	 * <b>Note</b>: This method does <i>not</i> modify the input tree.
	 * 
	 * @param input
	 *            the {@link lipstone.joshua.parser.tokenizer.ConsCell Token} tree to be cleaned
	 * @return the {@link lipstone.joshua.parser.tokenizer.ConsCell Token} tree without any excess parentheses.
	 */
	public static ConsCell removeExcessParentheses(ConsCell input) {
		//This takes care of inputs entirely encapsulated in parentheses
		while (input.getCarType() == PARENTHESES && input.getCdrType() == EMPTY)
			input = (ConsCell) input.getCar();
		ConsCell output = input.singular(), head = output;
		/*
		 * don't drop parentheses if ^, % on either side and length > 1
		 * don't drop parentheses if *, / on either side and terms > 1
		 * don't drop parentheses if - in front and terms > 1
		 * otherwise, drop parentheses
		 */
		for (ConsCell next = input.getNextConsCell(); !head.isNull() && !next.isNull(); head = head.append(next.singular()), next = next.getNextConsCell())
			if (head.getCarType() == PARENTHESES) {
				ConsCell paren = removeExcessParentheses((ConsCell) head.getCar()), previous = head.getPreviousConsCell();
				if (!paren.hasNextConsCell())
					head = head.replace(paren);
				else if (!(((previous.getCar().equals("-") || previous.getCar().equals("*") || previous.getCar().equals("/")) || (next.getCar().equals("*") || next.getCar().equals("/"))) && !isOneTerm(paren)))
					head = head.replace(paren);
			}
		return output;
	}
	
	public static boolean isOneTerm(ConsCell input) {
		//Using a while loop that steps forward before checking prevents leading +/- signs from being counted
		while (!(input = input.getNextConsCell()).isNull())
			if (input.getCarType() == OPERATOR) {
				if (input.getCar().equals("+") || input.getCar().equals("-"))
					return false;
				else
					input = input.getNextConsCell(); //This allows 1*-2 to be considered as 1 term
			}
		return true;
	}
	
	public static List<ConsCell> determineVariables(ConsCell input) {
		List<ConsCell> out = new ArrayList<>();
		for (ConsCell t : input)
			if (t.getCarType() == IDENTIFIER && !out.contains(t))
				out.add(t);
		return out;
	}
	
	public static List<Term> invertSign(List<Term> terms) {
		List<Term> out = new ArrayList<>();
		for (Term t : terms)
			out.add(t.negate());
		return out;
	}
	
	public ConsCell factor(ConsCell input) throws ParserException {
		ConsCell output = new ConsCell(), head = output;
		input = simplifyTokenTree(parser.evaluate(input));
		if (!input.checkLength(2) && input.getCarType() == NUMBER)
			for (BigDec factor : primeFactorization((BigDec) input.getCar()))
				head = head.append(new ConsCell(factor, NUMBER, new ConsCell(",", SEPARATOR)));
		if (head.getCarType() != SEPARATOR) //TODO might need to add to this after implementing additional factoring methods
			return input;
		head.remove();
		return output;
	}
	
	public static List<BigDec> primeFactorization(BigDec input) throws UndefinedResultException {
		if (!input.isInt())
			throw new UndefinedResultException("Prime factorization can only be performed on integers.", null);
		List<BigDec> output = new ArrayList<>();
		cachePrimes(input.longValue());
		for (int i = 0; i < primes.length && primes[i] <= input.longValue() && input.gt(ONE); i++) {
			BigDec p = new BigDec(getCachedPrimes()[i]), next = input;
			while (input.gt(ONE) && (next = input.divide(p)).isInt()) {
				output.add(p);
				input = next;
			}
		}
		return output;
	}
	
	public static long[] getCachedPrimes() {
		return primes;
	}
	
	/**
	 * If the upper bound of primes requested is greater than the last upper bound, this method generates all the primes in
	 * the range [2, n], and then caches the result. Otherwise, it just returns the cached values. This <i>can</i> result in
	 * primes larger than n being returned.
	 * 
	 * @param n
	 *            the upper bound for the list of primes
	 * @return A <tt>long</tt> array of all the primes in the range [2, n]
	 */
	public static long[] cachePrimes(long n) {
		try {
			primesLock.lock();
			if (n > primesLimit) {
				primesLimit = n;
				primes = generatePrimes(n);
			}
			return primes;
		}
		finally {
			primesLock.unlock();
		}
	}
	
	private native static long[] generatePrimes(long n);
}
