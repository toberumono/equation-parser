package lipstone.joshua.parser.cas;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import lipstone.joshua.parser.Parser;
import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.tokenizer.ConsCell;

public class Problem {
	public final List<Term> equation;
	public final List<ConsCell> variables;
	public final List<Solution> solutions = new ArrayList<>();
	
	/**
	 * @param equation
	 *            the equation for this problem, with all of the terms on one side
	 * @param variables
	 *            the variables that are relevant to the problem
	 */
	public Problem(List<Term> equation, List<ConsCell> variables) {
		this.equation = equation;
		this.variables = variables;
	}
	
	public void checkWork(Parser parser) {
		for (Iterator<Solution> s = solutions.iterator(); s.hasNext();)
			try {
				if (!s.next().check(this, parser))
					s.remove();
			}
			catch (ParserException e) {
				s.remove();
			}
	}
	
	public void attempt(Solver solver) throws ParserException {
		for (Solution s : solver.solve(this))
			if (!solutions.contains(s))
				solutions.add(s);
	}
	
	@Override
	public String toString() {
		return "Problem: " + equation + " Solutions: " + solutions;
	}
}
