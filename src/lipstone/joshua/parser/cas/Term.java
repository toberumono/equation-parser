package lipstone.joshua.parser.cas;

import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import toberumono.json.JSONObject;
import toberumono.json.JSONRepresentable;
import toberumono.json.JSONSystem;

import lipstone.joshua.parser.Parser;
import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.plugin.pluggable.Operator;
import lipstone.joshua.parser.tokenizer.ConsCell;
import lipstone.joshua.parser.types.BigDec;
import lipstone.joshua.parser.util.MathFunction;

import static lipstone.joshua.parser.tokenizer.ConsType.*;
import static lipstone.joshua.parser.types.BigDec.*;

/**
 * Represents the coefficient and base-exponent pairs within a term.<br>
 * These are modifiable objects, but they should only be modified during their construction within the {@link CAS}.<br>
 * None of the methods in this class modify the internal state of the object they are called on, so it is important to
 * reassign the variables as appropriate.
 * 
 * @author Joshua Lipstone
 */
public class Term implements Cloneable, JSONRepresentable {
	public BigDec coefficient;
	public TreeMap<ConsCell, ConsCell> variables = new TreeMap<>(), tails = new TreeMap<>();
	private static final ConsCell one = new ConsCell(ONE, NUMBER);
	
	/**
	 * Constructs a new {@link Term} object with a coefficient of one
	 */
	public Term() {
		this(ONE);
	}
	
	/**
	 * Constructs a new {@link Term} object with the given coefficient
	 * 
	 * @param coefficient
	 *            the coefficient for this {@link Term}
	 */
	public Term(BigDec coefficient) {
		this.coefficient = coefficient;
	}
	
	/**
	 * Condenses this {@link Term} by pushing the base-exponent pairs that evaluate to a single number into the coefficient.<br>
	 * This method does <i>not</i> modify the input {@link Term}.
	 * 
	 * @param parser
	 *            the {@link lipstone.joshua.parser.Parser Parser} that precipitated this method call
	 * @return a condensed version of this {@link Term}
	 * @throws ParserException
	 *             for exception propagation purposes
	 */
	public Term condense(Parser parser) throws ParserException {
		Term output = new Term();
		output.coefficient = this.coefficient;
		for (Entry<ConsCell, ConsCell> base : this.variables.entrySet()) {
			ConsCell key = parser.evaluate(parser.cas.removeExcessParentheses(base.getKey())), val = parser.evaluate(parser.cas.removeExcessParentheses(base.getValue()));
			if (key.getCarType() == NUMBER && val.getCarType() == NUMBER && !key.checkLength(2) && !val.checkLength(2))
				output.coefficient = output.coefficient.multiply(((BigDec) key.getCar()).pow((BigDec) val.getCar()));
			else {
				if (key.checkLength(2))
					key = parser.cas.simplifyTokenTree(key);
				if (val.checkLength(2))
					val = parser.cas.simplifyTokenTree(val);
				output.variables.put(key, val);
				output.tails.put(key, val.getLastConsCell());
			}
		}
		return output;
	}
	
	/**
	 * Multiplies this {@link Term} by another.<br>
	 * This method does <i>not</i> modify the either this {@link Term} term or input {@link Term}.
	 * 
	 * @param other
	 *            the {@link Term} by which to multiply this {@link Term}
	 * @param parser
	 *            the {@link lipstone.joshua.parser.Parser Parser} that precipitated this method call
	 * @return the <i>condensed</i> result of multiplying this {@link Term} by the given {@link Term}
	 * @throws ParserException
	 *             if an error occurs while condensing the result
	 */
	public Term multiply(Term other, Parser parser) throws ParserException {
		return multDiv(other, (current, mult) -> current.multiply(mult), parser.operators.get("+"), parser);
	}
	
	/**
	 * Divides this {@link Term} by another.<br>
	 * This method does <i>not</i> modify the either this {@link Term} term or input {@link Term}.
	 * 
	 * @param other
	 *            the {@link Term} by which to divide this {@link Term}
	 * @param parser
	 *            the {@link lipstone.joshua.parser.Parser Parser} that precipitated this method call
	 * @return the <i>condensed</i> result of dividing this {@link Term} by the given {@link Term}
	 * @throws ParserException
	 *             if an error occurs while condensing the result
	 */
	public Term divide(Term other, Parser parser) throws ParserException {
		return multDiv(other, (current, div) -> current.divide(div), parser.operators.get("-"), parser);
	}
	
	private Term multDiv(Term other, MathFunction coOp, Operator expOp, Parser parser) throws ParserException {
		Term out = new Term();
		out.coefficient = coOp.calculate(coefficient, other.coefficient);
		for (Entry<ConsCell, ConsCell> base : variables.entrySet()) {
			out.variables.put(base.getKey(), base.getValue().clone());
			out.tails.put(base.getKey(), out.variables.get(base.getKey()).getLastConsCell());
		}
		for (Entry<ConsCell, ConsCell> base : other.variables.entrySet())
			if (out.variables.get(base.getKey()) != null)
				out.tails.put(base.getKey(), out.tails.get(base.getKey()).append(new ConsCell(expOp, OPERATOR, new ConsCell(base.getValue().clone(), PARENTHESES), TOKEN)));
			else
				out.variables.put(base.getKey(), new ConsCell(expOp, OPERATOR, new ConsCell(base.getValue().clone(), PARENTHESES), TOKEN));
		for (ConsCell key : out.variables.keySet()) {
			out.variables.put(key, parser.evaluate(out.variables.get(key)));
			out.tails.put(key, out.variables.get(key).getLastConsCell());
		}
		return out;
	}
	
	public Term negate() {
		Term out = new Term();
		out.coefficient = coefficient.multiply(MINUSONE);
		for (Entry<ConsCell, ConsCell> base : variables.entrySet()) {
			out.variables.put(base.getKey(), base.getValue().clone());
			out.tails.put(base.getKey(), out.variables.get(base.getKey()).getLastConsCell());
		}
		return out;
	}
	
	@Override
	public Term clone() {
		Term clone = new Term();
		clone.coefficient = coefficient;
		for (Entry<ConsCell, ConsCell> base : variables.entrySet()) {
			clone.variables.put(base.getKey(), base.getValue().clone());
			clone.tails.put(base.getKey(), clone.variables.get(base.getKey()).getLastConsCell());
		}
		return clone;
	}
	
	@Override
	public JSONObject toJSONObject() {
		JSONObject root = new JSONObject();
		root.put("coefficient", JSONSystem.wrap(coefficient));
		for (Entry<ConsCell, ConsCell> var : variables.entrySet())
			root.put(var.getKey().toJSONString(), JSONSystem.wrap(var.getValue()));
		return root;
	}
	
	public ConsCell toConsCellTree(Operator multiplier, Operator exponent, ConsCell... exclude) {
		ConsCell output = new ConsCell(coefficient, NUMBER), head = output;
		List<ConsCell> d = Arrays.asList(exclude);
		for (Entry<ConsCell, ConsCell> var : variables.entrySet()) {
			if (d.contains(var.getKey()))
				continue;
			head = head.append(new ConsCell(multiplier, OPERATOR, var.getKey().checkLength(2) ? new ConsCell(var.getKey().clone(), PARENTHESES) : var.getKey().clone(), TOKEN));
			if (!var.getValue().equals(one))
				head = head.append(new ConsCell(exponent, OPERATOR, var.getValue().hasNextConsCell() ? new ConsCell(var.getValue().clone(), PARENTHESES) : var.getValue().clone(), TOKEN));
		}
		if (coefficient.eq(ONE) && output.checkLength(3))
			output = output.getNextConsCell().getNextConsCell();
		return output;
	}
	
	@Override
	public String toString() {
		return coefficient + "*" + variables.toString();
	}
}
