package lipstone.joshua.parser.cas;

import java.util.List;
import java.util.function.Predicate;

import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.plugin.pluggable.Pluggable;

/**
 * Represents a method for solving {@link Problem Problems}
 * 
 * @author Joshua Lipstone
 */
public class Solver implements Pluggable {
	private Predicate<Problem> isApplicable;
	private SolverMethod method;
	private final String name;
	private final ParserPlugin plugin;
	
	/**
	 * Constructs a new CAS solver.
	 * 
	 * @param isApplicable
	 *            a function that takes a {@link Problem} and returns whether this {@link Solver} should attempt to solve it
	 * @param method
	 *            a function that takes a {@link Problem} as an argument and then solves it
	 * @param name
	 *            the name of this {@link Solver}
	 * @param plugin
	 *            the {@link lipstone.joshua.parser.ParserPlugin plugin} that is adding this {@link Solver}
	 */
	public Solver(Predicate<Problem> isApplicable, SolverMethod method, String name, ParserPlugin plugin) {
		this.isApplicable = isApplicable;
		this.method = method;
		this.name = name;
		this.plugin = plugin;
	}
	
	/**
	 * Attempts to solve the given {@link Problem} with this {@link Solver}
	 * 
	 * @param problem
	 *            the {@link Problem} to solve
	 * @return a {@link List} of {@link Solution Solutions}
	 * @throws ParserException
	 *             for exception propagation
	 */
	public List<Solution> solve(Problem problem) throws ParserException {
		return method.apply(problem);
	}
	
	/**
	 * @param problem
	 *            the {@link Problem} to solve
	 * @return {@code true} if this {@link Solver} is applicable to the given {@link Problem}
	 */
	public boolean isApplicable(Problem problem) {
		return isApplicable.test(problem);
	}
	
	@Override
	public ParserPlugin getPlugin() {
		return plugin;
	}
	
	@Override
	public String getName() {
		return name;
	}
}
