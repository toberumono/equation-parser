package lipstone.joshua.parser.types;

public interface ParserObject {
	
	public String getType();
	
	public default String toTaggedString() {
		String tag = getType();
		if (tag.matches("((?=.*[^a-zA-Z_0-9])|((?=.*[a-z_A-Z])(?=.*[0-9]))).+"))
			tag = "\"" + tag + "\"";
		return "$" + tag + toString();
	}
	
	/**
	 * The output {@link String} should be a tokenizable representation of the {@link ParserObject}. Effectively,
	 * {@link lipstone.joshua.parser.Parser#tokenize(String) tokenize}(this.toString()) should result in a copy of the
	 * {@link ParserObject}.<br>
	 * <b>Note</b>: this method should include the opening and closing \u007B\u007D's
	 */
	@Override
	public String toString();
}
