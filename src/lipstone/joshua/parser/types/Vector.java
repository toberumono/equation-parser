package lipstone.joshua.parser.types;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import lipstone.joshua.parser.Parser;
import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.tokenizer.ConsCell;
import lipstone.joshua.parser.tokenizer.ConsType;

public final class Vector implements ParserObject, Iterable<ConsCell> {
	private final ConsCell[] components;
	
	public Vector(Parser parser, ConsCell... components) throws ParserException {
		this.components = components;
		for (int i = 0; i < this.components.length; i++)
			this.components[i] = parser.evaluate(this.components[i]);
	}
	
	public Vector(Parser parser, List<ConsCell> components) throws ParserException {
		this.components = new ConsCell[components.size()];
		for (int i = 0; i < components.size(); i++)
			this.components[i] = parser.evaluate(components.get(i));
	}
	
	public ConsCell dot(Vector b, Parser caller) throws ParserException {
		ConsCell result = new ConsCell();
		for (int i = 0; i < components.length; i++)
			result = result.append(new ConsCell(caller.operators.get("+"), ConsType.OPERATOR, components[i])).append(new ConsCell(caller.operators.get("*"), ConsType.OPERATOR, b.components[i]));
		if (!result.isNull())
			result = result.getFirstConsCell().remove();
		return caller.evaluate(result);
	}
	
	public int length() {
		return components.length;
	}
	
	@Override
	public Iterator<ConsCell> iterator() {
		return Arrays.asList(components).iterator();
	}
	
	@Override
	public String toString() {
		String out = "";
		for (ConsCell cell : components)
			out = out + ", " + cell;
		return "<" + (out.length() > 0 ? out.substring(2) : "") + ">";
	}

	@Override
	public String getType() {
		return "Vector";
	}
	
}
