package lipstone.joshua.parser.types;

/**
 * Represents the three different states that {@link BigDec} can have that are related to infinity.<br>
 * Note: {@link Infinity#ordinal()}-based comparisons are valid.
 * 
 * @author Toberumono
 */
public enum Infinity {
	//Negative is initialized first so that ordinal comparisons will return logical values
	/**
	 * Indicates that the {@link BigDec} has a value of negative infinity
	 */
	Negative {
		/**
		 * @return {@link Double#NEGATIVE_INFINITY}
		 */
		@Override
		public double doubleValue() {
			return Double.NEGATIVE_INFINITY;
		}
		
		/**
		 * @return {@link Infinity#Positive}
		 */
		@Override
		public Infinity negate() {
			return Positive;
		}
		
		@Override
		public Infinity multiply(Infinity other) {
			return other == Positive || other == Not ? Negative : Positive;
		}
		
		/**
		 * @return {@Link BigDec#MINUS_INFINITY}
		 */
		@Override
		public BigDec value() {
			return BigDec.MINUS_INFINITY;
		}
	},
	/**
	 * Indicates that the {@link BigDec} is does not have an infinite value
	 */
	Not {
		/**
		 * @return 0
		 */
		@Override
		public double doubleValue() {
			return 0;
		}
		
		/**
		 * @return {@link Infinity#Not}
		 */
		@Override
		public Infinity negate() {
			return Not;
		}
		
		/**
		 * @return <tt>other</tt> because multiplying any {@link Infinity} by something that is not infinite is trivially
		 *         that {@link Infinity}
		 */
		@Override
		public Infinity multiply(Infinity other) {
			return other;
		}
		
		/**
		 * @return {@link BigDec#ZERO}
		 */
		@Override
		public BigDec value() {
			return BigDec.ZERO;
		}
	},
	/**
	 * Indicates that the {@link BigDec} has a value of positive infinity
	 */
	Positive {
		/**
		 * @return {@link Double#POSITIVE_INFINITY}
		 */
		@Override
		public double doubleValue() {
			return Double.POSITIVE_INFINITY;
		}
		
		/**
		 * @return {@link Infinity#Negative}
		 */
		@Override
		public Infinity negate() {
			return Negative;
		}
		
		@Override
		public Infinity multiply(Infinity other) {
			return other == Positive || other == Not ? Positive : Negative;
		}
		
		/**
		 * @return {@Link BigDec#INFINITY}
		 */
		@Override
		public BigDec value() {
			return BigDec.INFINITY;
		}
	};
	
	/**
	 * @return the {@link Double} equivalent for the {@link Infinity}
	 */
	public abstract double doubleValue();
	
	/**
	 * Negates the {@link Infinity}
	 * 
	 * @return the equivalent of multiplying the {@link Infinity} by -1
	 */
	public abstract Infinity negate();
	
	/**
	 * @param other
	 *            the {@link Infinity} by which to multiply
	 * @return this {@link Infinity} * <tt>other</tt>
	 */
	public abstract Infinity multiply(Infinity other);
	
	/**
	 * Each {@link Infinity} has a corresponding {@link BigDec} constant. This convenience method just returns that constant.
	 * 
	 * @return the corresponding {@link BigDec} constant
	 */
	public abstract BigDec value();
}
