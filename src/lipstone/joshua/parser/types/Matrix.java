package lipstone.joshua.parser.types;

import java.util.List;

import lipstone.joshua.parser.Parser;
import lipstone.joshua.parser.exceptions.MatrixCoordinateOutOfBoundsException;
import lipstone.joshua.parser.exceptions.MatrixSyntaxException;
import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.exceptions.SyntaxException;
import lipstone.joshua.parser.exceptions.UnbalancedDescendersException;
import lipstone.joshua.parser.exceptions.UndefinedOperationException;
import lipstone.joshua.parser.exceptions.UndefinedResultException;
import lipstone.joshua.parser.plugin.pluggable.Operator;
import lipstone.joshua.parser.tokenizer.ConsCell;
import lipstone.joshua.parser.tokenizer.ConsType;

/**
 * Represents an N x M rectangular matrix.
 * 
 * @author Joshua Lipstone
 */
public final class Matrix implements ParserObject {
	private final ConsCell[][] matrix;
	
	@FunctionalInterface
	interface InitializerFunction {
		public void initialize(Matrix m);
	}
	
	public static final InitializerFunction DEFAULT_INITIALIZER = m -> {
		for (int r = 0; r < m.matrix.length; r++)
			for (int c = 0; c < m.matrix[r].length; c++)
				m.matrix[r][c] = new ConsCell(BigDec.ZERO, ConsType.NUMBER);
	};
	
	/**
	 * Generates a rows x cols {@link Matrix} with all values set to 0.
	 * 
	 * @param rows
	 *            the number of rows in the {@link Matrix}
	 * @param cols
	 *            the number of columns in the {@link Matrix}
	 */
	public Matrix(int rows, int cols) {
		this(rows, cols, DEFAULT_INITIALIZER);
	}
	
	/**
	 * Generates a rows x cols {@link Matrix} with all values set to 0.
	 * 
	 * @param rows
	 *            the number of rows in the {@link Matrix}
	 * @param cols
	 *            the number of columns in the {@link Matrix}
	 * @param initializer
	 *            a function that takes the {@link Matrix} that is being constructed as an argument and initializes
	 *            <i>every</i> value in the matrix.
	 */
	public Matrix(int rows, int cols, InitializerFunction initializer) {
		matrix = new ConsCell[rows][cols];
		if (initializer != null)
			initializer.initialize(this);
		else
			DEFAULT_INITIALIZER.initialize(this);
	}
	
	/**
	 * Initializes a {@link Matrix} that is backed a shallow copy of the provided 2D
	 * {@link lipstone.joshua.parser.tokenizer.ConsCell Token} array.
	 * 
	 * @param mat
	 *            a pre-arranged array of which a shallow copy will be made to back a new {@link Matrix}
	 */
	public Matrix(final ConsCell[][] mat) {
		this(mat.length, mat[0].length, m -> {
			for (int r = 0; r < mat.length; r++)
				for (int c = 0; c < mat[r].length; c++)
					m.matrix[r][c] = mat[r][c];
		});
	}
	
	/**
	 * Creates a shallow copy of the provided {@link Matrix} and associates it with the given
	 * {@link lipstone.joshua.parser.Parser Parser}.<br>
	 * The new {@link Matrix} has a different backing array, but the pointers within that array point to the same
	 * {@link lipstone.joshua.parser.tokenizer.ConsCell Tokens} as the pointers in the original {@link Matrix}.
	 * 
	 * @param m
	 *            the {@link Matrix} to copy
	 */
	public Matrix(Matrix m) {
		matrix = new ConsCell[m.getRows()][m.getCols()];
		for (int r = 0; r < m.getRows(); r++)
			for (int c = 0; c < m.getCols(); c++)
				matrix[r][c] = m.matrix[r][c];
	}
	
	/**
	 * Creates a new {@link Matrix} from the given input {@link lipstone.joshua.parser.tokenizer.ConsCell Token} tree.
	 * 
	 * @param input
	 *            a {@link lipstone.joshua.parser.tokenizer.ConsCell Token} that is the root of a comma-separated list of rows.
	 * @param caller
	 *            the {@link lipstone.joshua.parser.Parser Parser} that is creating this {@link Matrix}
	 * @throws MatrixSyntaxException
	 *             if the input {@link lipstone.joshua.parser.tokenizer.ConsCell Token} tree does not represent a valid
	 *             {@link Matrix}
	 */
	public Matrix(ConsCell input, Parser caller) throws MatrixSyntaxException {
		if (input.getCarType() == ConsType.MATRIX)
			input = (ConsCell) input.getCar();
		List<ConsCell> rows = input.splitOnSeparator();
		if (rows.size() < 1 || !(rows.get(0).getCar() instanceof List) || ((List<?>) rows.get(0).getCar()).size() < 1 || !(((List<?>) rows.get(0).getCar()).get(0) instanceof ConsCell))
			throw new MatrixSyntaxException(input, caller.getLastPlugin());
		@SuppressWarnings("unchecked")
		int numCols = ((List<ConsCell>) rows.get(0).getCar()).size();
		matrix = new ConsCell[rows.size()][numCols];
		for (int i = 0; i < rows.size(); i++) {
			if (!(rows.get(i).getCar() instanceof List))
				throw new MatrixSyntaxException(input, caller.getLastPlugin());
			@SuppressWarnings("unchecked")
			List<ConsCell> cols = (List<ConsCell>) rows.get(i).getCar();
			if (cols.size() != numCols)
				throw new MatrixSyntaxException(input, caller.getLastPlugin());
			for (int j = 0; j < numCols; j++)
				matrix[i][j] = cols.get(j);
		}
	}
	
	public Matrix(String input, Parser parser) throws MatrixSyntaxException, SyntaxException {
		this(parser.tokenize(input), parser);
	}
	
	public ConsCell get(int row, int col, Parser caller) throws MatrixCoordinateOutOfBoundsException {
		if (row >= matrix.length || col >= matrix[row].length)
			throw new MatrixCoordinateOutOfBoundsException(row, col, this, caller.getLastPlugin());
		return matrix[row][col];
	}
	
	public BigDec determinant(Parser caller) throws ParserException {
		if (!isSquare())
			throw new UndefinedResultException("The determinant operation is only valid for square matrices.", caller.getLastPlugin());
		if (getCols() == 1)
			return (BigDec) caller.evaluate(matrix[0][0]).getCar();
		
		String determinant = "0";
		for (int i = 0; i < getCols(); i++) {
			ConsCell[][] mat = new ConsCell[getRows() - 1][getCols() - 1];
			int skipped = 0;
			for (int a = 0; a < getCols(); a++) {
				if (a == i)
					skipped = 1;
				else
					for (int b = 1; b < getRows(); b++)
						mat[b - 1][a - skipped] = matrix[b][a];
			}
			if (i % 2 != 0)
				determinant += '-';
			determinant = determinant + "+(" + matrix[0][i] + ")*(" + new Matrix(mat).determinant(caller) + ")";
		}
		return (BigDec) caller.evaluate(caller.preProcess(caller.tokenize(determinant))).getCar();
	}
	
	//Basic operations for scalars, simpler to use a sort of switch than write the method for each.
	public Matrix multiply(BigDec scalar, Parser caller) throws ParserException {
		return scalarOp(scalar, caller.operators.get("*"), caller);
	}
	
	public Matrix divide(BigDec scalar, Parser caller) throws ParserException {
		return scalarOp(scalar, caller.operators.get("/"), caller);
	}
	
	public Matrix scalarOp(BigDec scalar, Operator op, Parser caller) throws UnbalancedDescendersException, ParserException {
		if (!op.equals("*") && !op.equals("/"))
			throw new UndefinedOperationException(op + " is not defined for a matrix and a scalar.", caller.getLastPlugin());
		Matrix m = new Matrix(this);
		for (int r = 0; r < m.getRows(); r++)
			for (int c = 0; c < m.matrix[r].length; c++) {
				m.matrix[r][c] = matrix[r][c].clone();
				m.matrix[r][c].append(new ConsCell(op, ConsType.OPERATOR)).append(new ConsCell(scalar, ConsType.NUMBER)); //Insert the addition or subtraction as appropriate
				m.matrix[r][c] = caller.evaluate(m.matrix[r][c]);
			}
		return m;
	}
	
	//Same as above, but for two matrices
	public Matrix add(Matrix m2, Parser caller) throws ParserException {
		return matrixOp(m2, caller.operators.get("+"), caller);
	}
	
	public Matrix substract(Matrix m2, Parser caller) throws ParserException {
		return matrixOp(m2, caller.operators.get("-"), caller);
	}
	
	public Matrix multiply(Matrix m2, Parser caller) throws UnbalancedDescendersException, ParserException {
		if (getCols() != m2.getRows())
			throw new UndefinedResultException("Matrix multiplication requires that the number of columns in the first matrix is equal to the number of rows in the second.", caller.getLastPlugin());
		Matrix m = new Matrix(getRows(), m2.getCols());
		for (int r = 0; r < m.getRows(); r++) {
			for (int c = 0; c < m.getCols(); c++) {
				String entry = "0";
				for (int i = 0, a = 0; i < m2.getRows() && a < getCols(); i++, a++)
					entry = entry + "+(" + matrix[r][a] + ")*(" + m2.matrix[i][c] + ")";
				m.matrix[r][c] = caller.tokenize(entry);
			}
		}
		return m.compress(caller);
	}
	
	/**
	 * Divides this {@link Matrix} with the given one.
	 * 
	 * @param divisor
	 *            an invertible {@link Matrix} with which to divide this one
	 * @param caller
	 *            the {@link lipstone.joshua.parser.Parser Parser} from which the current processing call originated
	 * @return the dividend
	 * @throws UndefinedResultException
	 *             if the divisor is not invertible
	 * @throws ParserException
	 *             to allow any other exceptions caused while evaluating this {@link Matrix} to propagate back to the calling
	 *             function.
	 */
	public Matrix divide(Matrix divisor, Parser caller) throws UndefinedResultException, ParserException {
		if (!divisor.isInvertible(caller) || getCols() != divisor.getRows())
			throw new UndefinedResultException("Matrix division is only applicable when the divisor is an invertible matrix" +
					"and the number of columns of the dividend equals the number of rows in the divisor.", caller.getLastPlugin());
		return multiply(divisor.invert(caller), caller);
	}
	
	public Matrix matrixOp(Matrix other, Operator op, Parser caller) throws SyntaxException, UnbalancedDescendersException, ParserException {
		if (op.equals("/"))
			return multiply(other, caller);
		else if (op.equals("/"))
			return divide(other, caller);
		else if (!op.equals("+") && !op.equals("-"))
			throw new UndefinedOperationException(op + " is not defined for two matrices.", caller.getLastPlugin());
		if (getRows() != other.getRows() || getCols() != other.getCols())
			throw new UndefinedOperationException("Matrix " + (op.equals("+") ? "addition" : "subtraction") + " requires that the rows and the columns of the two matrices be equal.", caller.getLastPlugin());
		Matrix m = new Matrix(getRows(), getCols());
		for (int r = 0; r < m.getRows(); r++)
			for (int c = 0; c < m.getCols(); c++) {
				m.matrix[r][c] = matrix[r][c].clone();
				m.matrix[r][c].append(new ConsCell(op, ConsType.OPERATOR)).append(other.matrix[r][c]); //Insert the addition or subtraction as appropriate
				m.matrix[r][c] = caller.evaluate(m.matrix[r][c]);
			}
		return m.compress(caller);
	}
	
	/**
	 * @param caller
	 *            the {@link lipstone.joshua.parser.Parser Parser} from which the current processing call originated
	 * @return a inverted copy of this {@link Matrix}
	 * @throws UndefinedResultException
	 *             if this {@link Matrix} is not invertible
	 * @throws ParserException
	 *             to allow any other exceptions caused while evaluating this {@link Matrix} to propagate back to the calling
	 *             function.
	 */
	public Matrix invert(Parser caller) throws UndefinedResultException, ParserException {
		if (!isInvertible(caller))
			throw new UndefinedResultException("The matrix is not invertible.", caller.getLastPlugin());
		Matrix output = new Matrix(this);
		output = output.multiply(BigDec.MINUSONE, caller);
		for (int r = 0; r < getRows(); r++)
			output.matrix[r][r] = matrix[getRows() - 1 - r][getCols() - 1 - r];
		return output.divide(determinant(caller), caller);
	}
	
	/**
	 * @param caller
	 *            the {@link lipstone.joshua.parser.Parser Parser} from which the current processing call originated
	 * @return a copy of this {@link Matrix} where all of the individual values have been evaluated.
	 * @throws ParserException
	 *             to allow any exceptions caused while evaluating this {@link Matrix} to propagate back to the calling
	 *             function.
	 */
	public Matrix compress(Parser caller) throws ParserException {
		Matrix m = new Matrix(getRows(), getCols());
		for (int r = 0; r < getRows(); r++)
			for (int c = 0; c < getCols(); c++)
				m.matrix[r][c] = caller.evaluate(caller.preProcess(matrix[r][c]));
		return m;
	}
	
	/**
	 * @return a String representation of this matrix where the rows an colums are layed out in a NxM grid.
	 */
	public String printMatrix() {
		String output = "";
		for (int r = 0; r < matrix.length; r++) {
			for (int c = 0; c < matrix[r].length; c++)
				output = output + " " + matrix[r][c].toString();
			output = output + "\n";
		}
		return output;
	}
	
	/**
	 * @param caller
	 *            the {@link lipstone.joshua.parser.Parser Parser} from which the current processing call originated
	 * @return true if this matrix is invertible, (this is a square matrix and its determinant is not equal to 0)
	 * @throws ParserException
	 *             to allow exceptions thrown while evaluating the matrix's determinant to propagate back to the calling
	 *             function
	 */
	public boolean isInvertible(Parser caller) throws ParserException {
		return isSquare() && determinant(caller).neq(BigDec.ZERO);
	}
	
	/**
	 * @return true if this is a square matrix (the number of rows equals the number of columns)
	 */
	public boolean isSquare() {
		return matrix.length == matrix[0].length;
	}
	
	/**
	 * @return the number of rows in this {@link Matrix}
	 */
	public int getRows() {
		return matrix.length;
	}
	
	/**
	 * @return the number of columns in this {@link Matrix}
	 */
	public int getCols() {
		return matrix[0].length;
	}
	
	/**
	 * Generates a n x n identity matrix and returns it.<br>
	 * It does not store the matrix for later use, so getIdentityMatrix(n, parser) != getIdentityMatrix(n, parser)
	 * 
	 * @param n
	 *            the dimension of the {@link Matrix}
	 * @param parser
	 *            the parser that this {@link Matrix} should be linked to.
	 * @return a n x n identity matrix
	 */
	public static Matrix getIdentityMatrix(int n, Parser parser) {
		Matrix I = new Matrix(n, n);
		for (int i = 0; i < n; i++)
			I.matrix[i][i] = new ConsCell(BigDec.ONE, ConsType.NUMBER);
		return I;
	}
	
	@Override
	public String toString() {
		String string = "{";
		for (int r = 0; r < matrix.length; r++) {
			string = string + "[";
			for (int c = 0; c < matrix[r].length; c++)
				string = string + matrix[r][c] + ",";
			string = string.substring(0, string.length() - 1) + "],";
		}
		string = string.substring(0, string.length() - 1) + "}";
		return string;
	}

	@Override
	public String getType() {
		return "Matrix";
	}
}