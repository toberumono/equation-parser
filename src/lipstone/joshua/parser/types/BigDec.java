package lipstone.joshua.parser.types;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apfloat.Apcomplex;
import org.apfloat.ApcomplexMath;
import org.apfloat.Apfloat;

import toberumono.json.JSONSerializable;
import toberumono.structures.tuples.Pair;

import lipstone.joshua.parser.Parser;
import lipstone.joshua.parser.exceptions.UndefinedResultException;
import lipstone.joshua.parser.tokenizer.ConsCell;
import lipstone.joshua.parser.tokenizer.ConsType;

public class BigDec extends Number implements ParserObject, Comparable<BigDec>, JSONSerializable {
	private static final Pattern complexNumber = Pattern.compile("([+-])?(([0-9]+(\\.[0-9]*)?|\\.[0-9]+)(\\*?i)?|i|infinity)(([+-])(([0-9]+(\\.[0-9]*)?|\\.[0-9]+)(\\*?i)?|i|infinity))?");
	
	/**
	 * The default precision used by <tt>Apcomplex</tt> for inexact operations (30 digits)
	 */
	public static final long PRECISION = 30L;
	public static final BigDec ZERO = new BigDec(BigDecimal.ZERO);
	public static final BigDec ONE = new BigDec(BigDecimal.ONE);
	public static final BigDec MINUSONE = new BigDec(BigDecimal.ZERO.subtract(BigDecimal.ONE));
	public static final BigDec INFINITY = new BigDec(Infinity.Positive);
	public static final BigDec MINUS_INFINITY = new BigDec(Infinity.Negative);
	public static final BigDec PI = new BigDec(Math.PI);
	public static final BigDec E = new BigDec(Math.E);
	public static final BigDec I = new BigDec(0, 1);
	
	private static final Apfloat ApZero = new Apfloat(0, PRECISION);
	private static final BigDec[] units = {new BigDec(1, 0), new BigDec(0, 1), new BigDec(-1, 0), new BigDec(0, -1)};
	
	private final Infinity infinity;
	private Apcomplex complex = null;
	private final BigDecimal real, imaginary;
	
	public BigDec(Infinity infinity) {
		real = BigDecimal.ZERO;
		imaginary = BigDecimal.ZERO;
		this.infinity = infinity;
	}
	
	public BigDec(Apcomplex number) {
		complex = number;
		real = new BigDecimal(number.real().toString(true));
		imaginary = new BigDecimal(number.imag().toString(true));
		infinity = Infinity.Not;
	}
	
	public BigDec(Apfloat real) {
		complex = new Apcomplex(real, ApZero);
		this.real = new BigDecimal(real.toString(true));
		imaginary = BigDecimal.ZERO;
		infinity = Infinity.Not;
	}
	
	public BigDec(Apfloat real, Apfloat complex) {
		this.complex = new Apcomplex(real, complex);
		this.real = new BigDecimal(real.toString(true));
		imaginary = new BigDecimal(complex.toString(true));
		infinity = Infinity.Not;
	}
	
	public BigDec(BigDecimal real) {
		this.real = real;
		imaginary = BigDecimal.ZERO;
		infinity = Infinity.Not;
	}
	
	public BigDec(BigDecimal real, BigDecimal complex) {
		this.real = real;
		imaginary = complex;
		infinity = Infinity.Not;
	}
	
	public BigDec(BigDec number) {
		infinity = number.infinity;
		complex = number.complex;
		real = number.real;
		imaginary = number.imaginary;
	}
	
	public BigDec(double real) {
		this(real, 0.0);
	}
	
	public BigDec(double real, double complex) {
		if (real == Double.POSITIVE_INFINITY || complex == Double.POSITIVE_INFINITY) {
			infinity = Infinity.Positive;
			this.real = BigDecimal.ZERO;
			this.imaginary = BigDecimal.ZERO;
		}
		else if (real == Double.NEGATIVE_INFINITY || complex == Double.NEGATIVE_INFINITY) {
			infinity = Infinity.Negative;
			this.real = BigDecimal.ZERO;
			this.imaginary = BigDecimal.ZERO;
		}
		else {
			infinity = Infinity.Not;
			this.real = new BigDecimal(real);
			imaginary = new BigDecimal(complex);
		}
	}
	
	public BigDec(int real) {
		this(real, 0);
	}
	
	public BigDec(int real, int complex) {
		infinity = Infinity.Not; //ints cannot equal Double.POSITIVE_INFINITY or Double.NEGATIVE_INFINITY
		this.real = new BigDecimal(real);
		this.imaginary = new BigDecimal(complex);
	}
	
	public BigDec(long real) {
		this(real, 0l);
	}
	
	public BigDec(long real, long complex) {
		infinity = Infinity.Not; //longs cannot equal Double.POSITIVE_INFINITY or Double.NEGATIVE_INFINITY
		this.real = new BigDecimal(real);
		this.imaginary = new BigDecimal(complex);
	}
	
	public BigDec(String number) {
		Matcher m = complexNumber.matcher(number.replaceAll(" ", "").toLowerCase());
		if (!m.matches()) {
			if (number.equals("i")) {
				infinity = Infinity.Not;
				real = BigDecimal.ZERO;
				imaginary = BigDecimal.ZERO;
				return;
			}
			throw new NumberFormatException(number + " is not a valid complex number.");
		}
		BigDecimal real = BigDecimal.ZERO, imaginary = BigDecimal.ZERO;
		Apfloat r = ApZero, i = ApZero;
		int sign = 1;
		if ("-".equals(m.group(1)))
			sign = -1;
		if (m.group(2).equals("infinity")) {
			infinity = sign == 1 ? Infinity.Positive : Infinity.Negative;
			this.real = BigDecimal.ZERO;
			this.imaginary = BigDecimal.ZERO;
			return;
		}
		if (m.group(2).equals("i"))
			imaginary = new BigDecimal(sign);
		else if (m.group(5) != null)
			imaginary = sign == -1 ? new BigDecimal("-" + m.group(3)) : new BigDecimal(m.group(3));
		else
			real = sign == -1 ? new BigDecimal("-" + m.group(3)) : new BigDecimal(m.group(3));
		if (m.group(6) != null) {
			sign = 1;
			if ("-".equals(m.group(7)))
				sign = -1;
			if (m.group(8).equals("infinity")) {
				infinity = sign == 1 ? Infinity.Positive : Infinity.Negative;
				this.real = BigDecimal.ZERO;
				this.imaginary = BigDecimal.ZERO;
				return;
			}
			if (m.group(8).equals("i"))
				imaginary = new BigDecimal(sign);
			else if (m.group(11) != null)
				imaginary = sign == -1 ? new BigDecimal("-" + m.group(9)) : new BigDecimal(m.group(9));
			else
				real = sign == -1 ? new BigDecimal("-" + m.group(9)) : new BigDecimal(m.group(9));
		}
		infinity = Infinity.Not;
		this.real = real;
		this.imaginary = imaginary;
		complex = new Apcomplex(r, i);
	}
	
	/**
	 * @return the core number-handling object that this BigDec encapsulates
	 * @see org.apfloat.Apcomplex
	 */
	public Apcomplex getInternal() {
		return complex == null ? (complex = new Apcomplex(new Apfloat(real.toPlainString(), PRECISION), new Apfloat(imaginary.toPlainString(), PRECISION))) : complex;
	}
	
	/**
	 * @return a {@link BigDec} representing the real component of this {@link BigDec}
	 */
	public BigDec real() {
		return new BigDec(real);
	}
	
	/**
	 * @return a {@link BigDec} representing the complex component of this {@link BigDec}
	 */
	public BigDec complex() {
		return new BigDec(complex);
	}
	
	/**
	 * Converts this number into an int. It drops any decimal and/or complex component without rounding.<br>
	 * Infinities are converted to {@link Integer#MAX_VALUE} for positive infinity and {@link Integer#MIN_VALUE} for negative
	 * infinity.<br>
	 * Ergo, this conversion function can be very lossy, so use it sparingly.
	 * 
	 * @return this number as an int
	 */
	@Override
	public int intValue() {
		return infinity == Infinity.Not ? real.intValue() : (infinity == Infinity.Positive ? Integer.MAX_VALUE : Integer.MIN_VALUE);
	}
	
	/**
	 * Converts this number into a double. It drops any complex component without rounding.<br>
	 * Infinities are converted to {@link Double#POSITIVE_INFINITY} for positive infinity and
	 * {@link Double#NEGATIVE_INFINITY} for negative infinity.<br>
	 * Ergo, this conversion function can be very lossy, so use it sparingly.
	 * 
	 * @return a double representation of this number.
	 */
	@Override
	public double doubleValue() {
		return infinity == Infinity.Not ? real.doubleValue() : infinity.doubleValue();
	}
	
	/**
	 * Converts this number into a long. It drops any decimal and/or complex component without rounding.<br>
	 * Infinities are converted to {@link Long#MAX_VALUE} for positive infinity and {@link Long#MIN_VALUE} for negative
	 * infinity.<br>
	 * Ergo, this conversion function can be very lossy, so use it sparingly.
	 * 
	 * @return a long representation of this number.
	 */
	@Override
	public long longValue() {
		return infinity == Infinity.Not ? real.longValue() : (infinity == Infinity.Positive ? Long.MAX_VALUE : Long.MIN_VALUE);
	}
	
	/**
	 * Gets this {@link BigDec}'s infinity information
	 * 
	 * @return {@link Infinity#Positive} if this is positive infinity, {@link Infinity#Not} if it is a finite number, or
	 *         {@link Infinity#Negative} if it is negative infinity
	 */
	public Infinity getInfinity() {
		return infinity;
	}
	
	/**
	 * Adds two BigDec objects. Equivalent to this + number.
	 * 
	 * @param number
	 *            the BigDec to add
	 * @return this + number
	 * @throws UndefinedResultException
	 *             when this BigDec and the passed BigDec represent different-signed infinities
	 */
	public BigDec add(BigDec number) throws UndefinedResultException {
		if (infinity != Infinity.Not) {
			if (number.infinity != Infinity.Not && number.infinity != infinity)
				throw new UndefinedResultException("Cannot add different signed infinities.", null);
			return infinity == Infinity.Negative ? MINUS_INFINITY : INFINITY;
		}
		if (number.infinity != Infinity.Not)
			return number.infinity.value();
		return new BigDec(real.add(number.real), imaginary.add(number.imaginary));
	}
	
	/**
	 * Subtracts two BigDec objects. Equivalent to this - number.
	 * 
	 * @param number
	 *            the BigDec to subtract
	 * @return this - number
	 * @throws UndefinedResultException
	 *             when this BigDec and the passed BigDec represent same-signed infinities
	 */
	public BigDec subtract(BigDec number) throws UndefinedResultException {
		if (infinity != Infinity.Not) {
			if (number.infinity != Infinity.Not && infinity != number.infinity.negate())
				throw new UndefinedResultException("Cannot add different signed infinities.", null);
			return infinity.value();
		}
		if (number.infinity != Infinity.Not)
			return number.infinity.negate().value();
		return new BigDec(real.subtract(number.real), imaginary.subtract(number.imaginary));
	}
	
	/**
	 * Multiplies two BigDec objects. Equivalent to this * number.
	 * 
	 * @param number
	 *            the BigDec to multiply by
	 * @return this * number
	 */
	public BigDec multiply(BigDec number) {
		if (infinity != Infinity.Not)
			return number.infinity != Infinity.Not ? infinity.value() : infinity.multiply(number.infinity).value();
		if (number.infinity != Infinity.Not)
			return number.infinity.value();
		return new BigDec(real.multiply(number.real).subtract(imaginary.multiply(number.imaginary)), imaginary.multiply(number.real).add(real.multiply(number.imaginary)));
	}
	
	/**
	 * Divides this BigDec by number. Equivalent to this / number.
	 * 
	 * @param number
	 *            the BigDec to divide by
	 * @return this / number
	 * @throws UndefinedResultException
	 *             when both this {@link BigDec} and the passed {@link BigDec} represent infinities
	 */
	public BigDec divide(BigDec number) throws UndefinedResultException {
		if (infinity != Infinity.Not) {
			if (number.infinity != Infinity.Not)
				throw new UndefinedResultException("Cannot divide infinity by infinity.", null);
			return infinity.value();
		}
		if (number.infinity != Infinity.Not)
			return ZERO;
		return imaginary.compareTo(BigDecimal.ZERO) == 0 && number.imaginary.compareTo(BigDecimal.ZERO) == 0 ?
				new BigDec(real.divide(number.real, MathContext.DECIMAL128), BigDecimal.ZERO) : new BigDec(getInternal().divide(number.getInternal()));
	}
	
	/**
	 * The modulus operation. Equivalent to this % number
	 * 
	 * @param number
	 *            the BigDec to mod by
	 * @return this % number
	 * @throws UndefinedResultException
	 *             when the passed {@link BigDec} equals 0 or both this {@link BigDec} and the passed {@link BigDec}
	 *             represent infinities
	 */
	public BigDec mod(BigDec number) throws UndefinedResultException {
		if (infinity != Infinity.Not)
			return INFINITY;
		if (number.eq(ZERO))
			throw new UndefinedResultException("Cannot take the modulus of 0.", null);
		if (!isComplex() && !number.isComplex())
			return new BigDec(real.remainder(number.real).abs(), BigDecimal.ZERO);
		return gt(number) ? complexMod(this, number) : complexMod(number, this);
	}
	
	private BigDec complexMod(BigDec b, BigDec a) throws UndefinedResultException {
		return b.subtract(getQ(b, a, ZERO, ZERO, -1).multiply(a));
	}
	
	//0 -> +1, 1 -> +i, 2 -> -1, 3 -> -i
	private BigDec getQ(BigDec b, BigDec a, BigDec q, BigDec lastR, int direction) throws UndefinedResultException {
		BigDecimal norm = norm(b);
		//q, r
		@SuppressWarnings("unchecked")
		Pair<BigDec, BigDec>[] r = (Pair<BigDec, BigDec>[]) Array.newInstance(new Pair<BigDec, BigDec>(null, null).getClass(), 4);
		if (direction >= 0) {
			direction = (direction + 2) % 4;
			r[direction] = new Pair<>(q, lastR);
		}
		int lowest = 0;
		for (int i = 0; i < 4; i++) {
			if (i != direction) {
				BigDec tempQ = q.add(units[i]), mult = a.multiply(tempQ);
				if (norm(mult).compareTo(norm) <= 0)
					r[i] = new Pair<>(tempQ, b.subtract(mult));
				else {
					r[i] = null;
					if (lowest == i)
						lowest++;
					continue;
				}
			}
			if (i > 0 && norm(r[i].getY()).compareTo(norm(r[lowest].getY())) <= 0)
				lowest = i;
		}
		if (lowest == direction)
			return q;
		return getQ(b, a, r[lowest].getX(), r[lowest].getY(), lowest);
	}
	
	private static final BigDecimal norm(BigDec number) {
		return number.real.multiply(number.real).add(number.imaginary.multiply(number.imaginary));
	}
	
	/**
	 * Determines if this number evenly divides the given number. (If the result of the division would be a Gaussian Integer)
	 * 
	 * @param number
	 *            another Gaussian Integer
	 * @return true if this Gaussian Integer evenly divides the other Gaussian Integer
	 * @throws UndefinedResultException
	 *             if either this or the other number is not a Gaussian Integer
	 */
	public boolean divides(BigDec number) throws UndefinedResultException {
		return !eq(ZERO) && (number.eq(ZERO) || number.mod(this).eq(ZERO));
	}
	
	/**
	 * The remainder operation. This is NOT the modulus operation.
	 * 
	 * @param number
	 *            the BigDec to divide by
	 * @return the signed remainder of the division operation
	 * @throws UndefinedResultException
	 *             when the passed {@link BigDec} equals 0
	 */
	public BigDec remainder(BigDec number) throws UndefinedResultException {
		if (number.eq(ZERO))
			throw new UndefinedResultException("The remainder when dividing by zero is undefined.", null);
		if (!isComplex() && !number.isComplex())
			return new BigDec(real.remainder(number.real));
		return gt(number) ? complexMod(this, number) : complexMod(number, this);
	}
	
	/**
	 * Equivalent of Math.pow(base, exp) where base is this object
	 * 
	 * @param exp
	 *            the exponent as a BigDec
	 * @return a BigDec object with a value equal to this^exp to the accuracy defined by context
	 * @throws UndefinedResultException
	 *             when both this and exp are infinities
	 */
	public BigDec pow(BigDec exp) throws UndefinedResultException {
		return imaginary.compareTo(BigDecimal.ZERO) == 0 && exp.imaginary.compareTo(BigDecimal.ZERO) == 0 ? pow(exp, MathContext.DECIMAL128) : new BigDec(ApcomplexMath.pow(getInternal(),
				exp.getInternal()));
	}
	
	/**
	 * Equivalent of Math.pow(base, exp) where base is this object
	 * 
	 * @param exp
	 *            the exponent as a BigDec, unlike the BigDecimal.pow(BigDecimal exp) this can be a decimal value
	 * @param context
	 *            a MathContext object containing the accuracy for this exponent calculation
	 * @return a BigDec object with a value equal to this^exp to the accuracy defined by context
	 * @throws UndefinedResultException
	 *             when both this and exp are infinitives
	 */
	public BigDec pow(BigDec exp, MathContext context) throws UndefinedResultException {
		if (infinity != Infinity.Not) {
			if (exp.infinity != Infinity.Not)
				throw new UndefinedResultException(null);
			return infinity.value();
		}
		else if (exp.infinity == Infinity.Positive)
			return INFINITY;
		else if (exp.infinity == Infinity.Negative)
			return ZERO;
		BigDec result = ZERO;
		double multiplier = 1, n2 = exp.doubleValue();
		if (this.real.signum() < 0 && (int) (n2) != n2 && exp.signum() < 1 && ((int) 1 / n2) % 2 != 0) {
			multiplier = -1;
			try {
				result = new BigDec(multiplier * Math.pow(new BigDec(multiplier).multiply(this).doubleValue(), n2));
			}
			catch (NumberFormatException e) {
				result = ZERO;
			}
		}
		else {
			try {
				result = new BigDec(Math.pow(this.real.doubleValue(), n2));
			}
			catch (NumberFormatException e) {
				result = ZERO;
			}
		}
		if (result.eq(ZERO) && this.neq(ZERO)) {
			//###############################################################################################################################################//
			//I got this code for exponents from Gene Marin via http://stackoverflow.com/questions/3579779/how-to-do-a-fractional-power-on-bigdecimal-in-java//
			//###############################################################################################################################################//
			int signOf2 = exp.signum();
			
			MathContext mc;
			mc = new MathContext(this.real.precision() * exp.real.precision());
			try {
				// Perform X^(A+B)=X^A*X^B (B = remainder)
				double dn1 = this.real.doubleValue();
				exp = exp.multiply(new BigDec(signOf2)); // exp is now positive
				BigDec remainderOf2 = exp.remainder(ONE);
				BigDec n2IntPart = exp.subtract(remainderOf2);
				// Calculate big part of the power using context -
				// bigger range and performance but lower accuracy
				BigDec intPow = new BigDec(this.real.pow(n2IntPart.real.intValueExact(), mc));
				BigDec doublePow = new BigDec(Math.pow(dn1, remainderOf2.doubleValue()));
				result = intPow.multiply(doublePow);
			}
			catch (Exception e) {}
			// Fix negative power
			if (signOf2 == -1 && result.neq(ZERO))
				result = new BigDec(ONE.real.divide(result.real, mc).stripTrailingZeros());
		}
		return result;
	}
	
	/**
	 * equal to
	 * 
	 * @param number
	 *            the value to be compared to
	 * @return returns true if the real and imaginary components are equal, false otherwise
	 */
	public boolean eq(BigDec number) {
		return infinity == number.infinity && real.compareTo(number.real) == 0 && imaginary.compareTo(number.imaginary) == 0;
	}
	
	/**
	 * not equal to
	 * 
	 * @param number
	 *            the value to be compared to
	 * @return returns the inverse of {@link #eq(BigDec) eq(number)}.
	 */
	public boolean neq(BigDec number) {
		return !eq(number);
	}
	
	/**
	 * greater than
	 * 
	 * @param number
	 *            the value to be compared to
	 * @return returns true if this is greater than number
	 */
	public boolean gt(BigDec number) {
		if (infinity == Infinity.Not && number.infinity == Infinity.Not)
			return imaginary.compareTo(BigDecimal.ZERO) == 0 ? real.compareTo(number.real) > 0 : norm(this).compareTo(norm(number)) > 0;
		return infinity.ordinal() > number.infinity.ordinal();
	}
	
	/**
	 * less than
	 * 
	 * @param number
	 *            the value to be compared to
	 * @return returns true if this is less than number
	 */
	public boolean lt(BigDec number) {
		if (infinity == Infinity.Not && number.infinity == Infinity.Not)
			return imaginary.compareTo(BigDecimal.ZERO) == 0 ? real.compareTo(number.real) < 0 : norm(this).compareTo(norm(number)) < 0;
		return infinity.ordinal() < number.infinity.ordinal();
	}
	
	/**
	 * greater than or equal to
	 * 
	 * @param number
	 *            the value to be compared to
	 * @return returns true if this is greater than or equal to number
	 */
	public boolean gteq(BigDec number) {
		if (infinity == Infinity.Not && number.infinity == Infinity.Not)
			return imaginary.compareTo(BigDecimal.ZERO) == 0 ? real.compareTo(number.real) >= 0 : norm(this).compareTo(norm(number)) >= 0;
		return infinity.ordinal() >= number.infinity.ordinal();
	}
	
	/**
	 * less than or equal to
	 * 
	 * @param number
	 *            the value to be compared to
	 * @return returns true if this is less than or equal to number
	 */
	public boolean lteq(BigDec number) {
		if (infinity == Infinity.Not && number.infinity == Infinity.Not)
			return imaginary.compareTo(BigDecimal.ZERO) == 0 ? real.compareTo(number.real) <= 0 : norm(this).compareTo(norm(number)) <= 0;
		return infinity.ordinal() <= number.infinity.ordinal();
	}
	
	/**
	 * Returns a BigDec whose value is the absolute value of this BigDec
	 * 
	 * @return the absolute value of this BigDec
	 */
	public BigDec abs() {
		return imaginary.compareTo(BigDecimal.ZERO) == 0 ? new BigDec(real.multiply(real.signum() == -1 ? MINUSONE.real : ONE.real)) : real.compareTo(BigDecimal.ZERO) == 0 ?
				new BigDec(real, imaginary.multiply(imaginary.signum() == -1 ? MINUSONE.real : ONE.real)) : new BigDec(ApcomplexMath.abs(getInternal()));
	}
	
	/**
	 * Functional equivalent of ++, the stored value by one, but doesn't save to this object. So, the use is num = num.pp(),
	 * not num.pp().
	 * 
	 * @return this BigDec with the stored value incremented by 1
	 */
	public BigDec pp() {
		return new BigDec(real.add(ONE.real), imaginary);
	}
	
	/**
	 * Functional equivalent of --, decrements the stored value by one, but doesn't save to this object. So, the use is num =
	 * num.mm(), not num.mm().
	 * 
	 * @return this BigDec with the stored value decremented by 1
	 */
	public BigDec mm() {
		return new BigDec(real.add(MINUSONE.real), imaginary);
	}
	
	/**
	 * Determines the sign of this number.<br>
	 * The sign is defined as -1 when real {@literal <} 0 or real == 0 and imaginary {@literal <} 0, 0 when real == imaginary
	 * == 0, and 1 when real {@literal >} 0 or real == 0 and imaginary {@literal >} 0
	 * 
	 * @return the sign of this number
	 */
	public int signum() {
		return real.compareTo(BigDecimal.ZERO) != 0 ? real.signum() : imaginary.signum();
	}
	
	/**
	 * Determines if this number is an integer.
	 * 
	 * @return true if this number's complex component is zero and its real component's a decimal component is also zero.
	 */
	public boolean isInt() {
		return imaginary.signum() == 0 && (this.real.signum() == 0 || this.real.scale() <= 0 || this.real.stripTrailingZeros().scale() <= 0);
	}
	
	/**
	 * Determines if this number is a complex number.
	 * 
	 * @return true if this number is not infinite and its complex component is not equal to zero.
	 */
	public boolean isComplex() {
		return imaginary.compareTo(BigDecimal.ZERO) != 0;
	}
	
	/**
	 * Computes the greatest common divisor of this number and <tt>other</tt>
	 * 
	 * @param other
	 *            another integer
	 * @return the gcd of this number and <tt>other</tt>
	 * @throws UndefinedResultException
	 *             if either this number or <tt>other</tt> is not an integer
	 */
	public BigDec gcd(BigDec other) throws UndefinedResultException {
		if (isComplex() && other.isComplex())
			return gt(other) ? complexGCD(this, other) : complexGCD(other, this);
		if (!isInt() || !other.isInt())
			throw new UndefinedResultException("The gcd operation is only valid over the integers.", null);
		BigDec a = this, r = null;
		while ((r = other.mod(a)).neq(ZERO)) {
			a = other;
			other = r;
		}
		return other;
	}
	
	private BigDec complexGCD(BigDec b, BigDec a) throws UndefinedResultException {
		BigDec q = getQ(b, a, ZERO, ZERO, -1), mod = b.subtract(q.multiply(a));
		while (!mod.equals(ZERO)) {
			b = a;
			a = mod;
			q = getQ(b, a, ZERO, ZERO, -1);
			mod = b.subtract(q.multiply(a));
		}
		return new BigDec(a);
	}
	
	/**
	 * Computes the least common multiple of this number and <tt>other</tt>
	 * 
	 * @param other
	 *            another integer
	 * @return the lcm of this number and <tt>other</tt>
	 * @throws UndefinedResultException
	 *             if either this number or <tt>other</tt> is not an integer
	 */
	public BigDec lcm(BigDec other) throws UndefinedResultException {
		if (!isInt() || !other.isInt())
			throw new UndefinedResultException("The lcm operation is only valid over the integers.", null);
		return multiply(other).divide(gcd(other));
	}
	
	/**
	 * Converts this {@link BigDec} into a fraction.<br>
	 * Does not work for infinities and complex numbers.
	 * 
	 * @param caller
	 *            the {@link lipstone.joshua.parser.Parser Parser} from which the current processing call originated
	 * @return a fractional representation of this {@link BigDec}
	 * @throws UndefinedResultException
	 *             if this {@link BigDec} is an infinity or is a complex number
	 */
	public ConsCell toFraction(Parser caller) throws UndefinedResultException {
		if (infinity != Infinity.Not)
			throw new UndefinedResultException("Cannot convert infinity to fractional form.", null);
		BigDec numerator = this, denominator = ONE, ten = new BigDec(10);
		//TODO handle complex numbers
		while (!numerator.isInt()) {
			numerator = numerator.multiply(ten);
			denominator = denominator.multiply(ten);
		}
		BigDec gcd = numerator.gcd(denominator);
		numerator = numerator.divide(gcd);
		denominator = denominator.divide(gcd);
		return new ConsCell(numerator, ConsType.NUMBER, new ConsCell(caller.operators.get("/"), ConsType.OPERATOR, new ConsCell(denominator, ConsType.NUMBER)));
	}
	
	@Override
	public String toString() {
		return imaginary.signum() == 0 ? real.stripTrailingZeros().toPlainString() : real.stripTrailingZeros().toPlainString() +
				(imaginary.signum() == -1 ? imaginary.stripTrailingZeros().toPlainString() : "+" + imaginary.stripTrailingZeros().toPlainString());
	}
	
	@Override
	public int compareTo(BigDec o) {
		return gt(o) ? 1 : lt(o) ? -1 : 0;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof BigDec))
			return false;
		return eq((BigDec) o);
	}
	
	@Override
	public String toJSONString() {
		return toString().trim().toLowerCase();
	}
	
	@Override
	public String getType() {
		return "BigDec";
	}
	
	@Override
	public float floatValue() {
		return 0;
	}
}
