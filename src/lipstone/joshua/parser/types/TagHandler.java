package lipstone.joshua.parser.types;

import lipstone.joshua.parser.Parser;

/**
 * Represents a function that constructs a {@link ParserObject}
 * 
 * @author Joshua Lipstone
 */
@FunctionalInterface
public interface TagHandler {
	
	/**
	 * Constructs a {@link ParserObject} from the given input and pairs it with the given
	 * {@link lipstone.joshua.parser.Parser Parser}<br>
	 * 
	 * @param input
	 *            the raw, untokenized input from which to construct the {@link ParserObject}
	 * @param parser
	 *            the {@link lipstone.joshua.parser.Parser Parser} to pair the {@link ParserObject} with
	 * @return a newly constructed {@link ParserObject}
	 */
	public ParserObject handle(String input, Parser parser);
}
