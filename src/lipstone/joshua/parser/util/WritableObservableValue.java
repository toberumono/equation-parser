package lipstone.joshua.parser.util;

import java.util.ArrayList;
import java.util.List;

import com.sun.javafx.binding.ExpressionHelper;

import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValueBase;
import javafx.beans.value.WritableObjectValue;

/**
 * A writable implementation of {@link javafx.beans.value.ObservableValue ObservableValue}.
 * 
 * @author Joshua Lipstone
 * @param <T>
 *            the type that this {@link WritableObservableValue} wraps
 */
public class WritableObservableValue<T> extends ObservableValueBase<T> implements WritableObjectValue<T>, ListenerTracker {
	private final List<Object> listeners = new ArrayList<>();
	private final ValueKernel value;
	
	private class ValueKernel {
		T value = null;
		ExpressionHelper<T> helper;
	}
	
	public WritableObservableValue(T initial) {
		value = new ValueKernel();
		value.value = initial;
	}
	
	/**
	 * Creates an instance of {@link WritableObservableValue} whose value is linked to <tt>base</tt>'s value via pointers.
	 * 
	 * @param base
	 *            the {@link WritableObservableValue} to link
	 */
	public WritableObservableValue(WritableObservableValue<T> base) {
		this.value = base.value;
	}
	
	@Override
	public T get() {
		return getValue();
	}
	
	@Override
	public void set(T value) {
		setValue(value);
	}
	
	@Override
	public void addListener(InvalidationListener listener) {
		value.helper = ExpressionHelper.addListener(value.helper, this, listener);
		listeners.add(listener);
	}
	
	@Override
	public void addListener(ChangeListener<? super T> listener) {
		value.helper = ExpressionHelper.addListener(value.helper, this, listener);
		listeners.add(listener);
	}
	
	@Override
	public void removeListener(InvalidationListener listener) {
		value.helper = ExpressionHelper.removeListener(value.helper, listener);
		listeners.remove(listener);
	}
	
	@Override
	public void removeListener(ChangeListener<? super T> listener) {
		value.helper = ExpressionHelper.removeListener(value.helper, listener);
		listeners.remove(listener);
	}
	
	@Override
	public void clearTrackedListeners() {
		while (listeners.size() > 0) {
			Object listener = listeners.get(0);
			if (listener instanceof InvalidationListener)
				removeListener((InvalidationListener) listener);
			else if (listener instanceof ChangeListener) {
				@SuppressWarnings("unchecked")
				ChangeListener<? super T> listen = (ChangeListener<? super T>) listener;
				removeListener(listen);
			}
		}
	}
	
	@Override
	public T getValue() {
		return value.value;
	}
	
	@Override
	public void setValue(T value) {
		this.value.value = value;
		fireValueChangedEvent();
	}

    @Override
	protected void fireValueChangedEvent() {
        ExpressionHelper.fireValueChangedEvent(value.helper);
    }
}
