package lipstone.joshua.parser.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import toberumono.json.JSONObject;
import toberumono.json.JSONSystem;
import toberumono.json.exceptions.JSONException;

import lipstone.joshua.parser.Parser;
import lipstone.joshua.parser.exceptions.ParserException;

import javafx.collections.ModifiableObservableListBase;

/**
 * This provides a means of storing a history of operations performed through one instance of the program and saving/loading
 * them to/from a json file. This class uses an List of {@link Equation Equations} to store
 * history in REVERSE order.<br>
 * <br>
 * The History JSON file has the following format:
 * <ul style="list-style-type: none">
 * <li>&#123;</li>
 * <li>
 * <ul style="list-style-type: none">
 * <li>"history" : [</li>
 * <li>
 * <ul style="list-style-type: none">
 * <li>&#123;</li>
 * <li>
 * <ul style="list-style-type: none">
 * <li>"Equation" : {@literal <the equation>} as a String,</li>
 * <li>"Answer" : {@literal <the answer>} as a String,</li>
 * <li>"Error" : {@literal <the error>} as a JSONObject if there is one, otherwise null</li>
 * </ul>
 * </li>
 * <li>&#125;,...</li>
 * </ul>
 * </li>
 * <li>]</li>
 * </ul>
 * &#125;
 * </ul>
 * 
 * @author Joshua Lipstone
 */
public class History extends ModifiableObservableListBase<Equation> {
	private final List<Equation> backing;
	private final List<JSONObject> json;
	private final Path fileLocation;
	private final Parser parser;
	private final Logger logger;
	private int writesDisabled; //Used for operations that load a large amount of data into the array in rapid succession
	
	public History(Path location, Parser parser) {
		this.parser = parser;
		logger = Logger.getLogger("toberumono.parser.util.History");
		fileLocation = Files.isDirectory(location) ? location.resolve("history.json") : location;
		logger.log(Level.INFO, "Using " + fileLocation.toString() + " as the history file.");
		backing = new ArrayList<>();
		json = new ArrayList<>();
		writesDisabled = 0;
		loadFromJSON();
	}
	
	private void loadFromJSON() {
		try {
			writesDisabled++;
			JSONObject historyFile = (JSONObject) JSONSystem.loadJSON(fileLocation);
			if (!historyFile.containsKey("history") || !(historyFile.get("history").value() instanceof List)) //If the JSON file does not contain the history list
				return;
			@SuppressWarnings("unchecked")
			List<JSONObject> hist = (List<JSONObject>) historyFile.get("history");
			for (JSONObject item : hist)
				super.add(new Equation(item, parser));
		}
		catch (IOException | JSONException | ParserException e) {
			logger.log(Level.WARNING, "Failed to load the history file data.", e);
		}
		finally {
			writesDisabled--;
		}
	}
	
	public void writeJSON() {
		if (writesDisabled > 0)
			return;
		JSONObject history = new JSONObject();
		history.put("history", JSONSystem.wrap(json));
		try {
			JSONSystem.writeJSON(history, fileLocation);
		}
		catch (IOException e) {
			logger.log(Level.SEVERE, "Failed to save the history file data.", e);
		}
	}
	
	public synchronized void appendEquation(Equation e) {
		super.add(0, e);
	}
	
	public synchronized int indexOf(Equation e) {
		return backing.indexOf(e);
	}
	
	public synchronized void swap(int index1, int index2) {
		try {
			writesDisabled++;
			super.set(index1, super.set(index2, backing.get(index1)));
		}
		finally {
			writesDisabled--;
		}
		writeJSON();
	}
	
	@Override
	public String toString() {
		String output = "";
		for (Equation eqn : backing)
			output = output + "\n\n" + eqn.getInput() + " -> " + eqn.getResult();
		return output.length() > 0 ? output.substring(2) : output;
	}
	
	/**
	 * @return the location that the history is being written to
	 */
	public Path getFileLocation() {
		return fileLocation;
	}
	
	/**
	 * @return an unmodifiable {@link List} backed by this
	 *         {@link History History's} internal list
	 */
	public List<Equation> getList() {
		return Collections.unmodifiableList(backing);
	}
	
	@Override
	protected synchronized void doAdd(int index, Equation element) {
		backing.add(index, element);
		json.add(index, JSONSystem.wrap(element));
		writeJSON();
	}
	
	@Override
	protected synchronized Equation doSet(int index, Equation element) {
		Equation old = backing.set(index, element);
		json.set(index, JSONSystem.wrap(element));
		writeJSON();
		return old;
	}
	
	@Override
	protected synchronized Equation doRemove(int index) {
		Equation removed = backing.remove(index);
		json.remove(index);
		writeJSON();
		return removed;
	}
	
	@Override
	public synchronized Equation get(int index) {
		return backing.get(index);
	}
	
	@Override
	public synchronized int size() {
		return backing.size();
	}
	
	@Override
	public synchronized void clear() {
		try {
			writesDisabled++;
			super.clear();
		}
		finally {
			writesDisabled--;
		}
	}
}