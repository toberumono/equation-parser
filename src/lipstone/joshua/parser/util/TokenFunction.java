package lipstone.joshua.parser.util;

import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.tokenizer.ConsCell;

/**
 * An interface for short operations on two {@link lipstone.joshua.parser.tokenizer.ConsCell Tokens}
 * 
 * @author Joshua Lipstone
 */
@FunctionalInterface
public interface TokenFunction {
	public ConsCell apply(ConsCell a, ConsCell b) throws ParserException;
}
