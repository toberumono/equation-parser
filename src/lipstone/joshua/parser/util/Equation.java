package lipstone.joshua.parser.util;

import toberumono.json.JSONObject;
import toberumono.json.JSONRepresentable;
import toberumono.json.JSONSystem;
import toberumono.json.JSONType;

import lipstone.joshua.parser.Parser;
import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.tokenizer.ConsCell;

/**
 * Stores the input, result, and error relevant to a fully parsed and evaluated input.
 * 
 * @author Joshua Lipstone
 */
public class Equation implements JSONRepresentable {
	private final ConsCell input, result;
	private final ParserException error;
	
	/**
	 * Stores a shallow copy of all of the parameters.
	 * 
	 * @param input
	 *            the input equation
	 * @param result
	 *            the result from evaluating the input
	 * @param error
	 *            the error if one occured while evaluating the given input, otherwise null
	 */
	public Equation(ConsCell input, ConsCell result, ParserException error) {
		this.input = input.clone();
		this.result = result.clone();
		this.error = error == null ? null : error.clone();
	}
	
	/**
	 * Creates a new {@link Equation} object based on the provided source.
	 * 
	 * @param input
	 *            a valid input that can be tokenized via {@link lipstone.joshua.parser.tokenizer.Tokenizer#lex(String)}
	 * @param result
	 *            the result from evaluating the given input
	 * @param error
	 *            the error that occured while evaluating the given input if one occured, otherwise null
	 * @param parser
	 *            the {@link lipstone.joshua.parser.Parser Parser} that this {@link Equation} will be paired with
	 * @throws ParserException
	 *             if an error occurs while loading this {@link Equation}
	 */
	public Equation(String input, String result, ParserException error, Parser parser) throws ParserException {
		this.input = input.length() > 0 ? parser.tokenize(input) : new ConsCell();
		this.result = result.length() > 0 ? parser.tokenize(result) : new ConsCell();
		this.error = error == null ? null : error.clone();
	}
	
	/**
	 * Creates a shallow copy of the given {@link Equation}
	 * 
	 * @param equation
	 *            the {@link Equation} to copy
	 */
	public Equation(Equation equation) {
		this.input = equation.input;
		result = equation.result;
		error = equation.error;
	}
	
	/**
	 * Uses the equation, answer, and error data in the given {@link JSONObject} to construct an {@link Equation}.
	 * 
	 * @param base
	 *            the {@link JSONObject}
	 * @param parser
	 *            the {@link lipstone.joshua.parser.Parser Parser} that this {@link Equation} will be linked to. This is also
	 *            used in reconstructing the error data.
	 * @throws ParserException
	 *             if an error occurs while loading this {@link Equation}
	 */
	public Equation(JSONObject base, Parser parser) throws ParserException {
		this((String) base.get("Equation").value(), (String) base.get("Answer").value(),
				base.get("Error").type() == JSONType.OBJECT ? new ParserException((JSONObject) base.get("Error"), parser) : null, parser);
	}
	
	/**
	 * @return the input that this {@link Equation} represents
	 */
	public ConsCell getInput() {
		return input;
	}
	
	/**
	 * @return the result of evaluating the input for this {@link Equation}
	 */
	public ConsCell getResult() {
		return result;
	}
	
	/**
	 * @return the {@link lipstone.joshua.parser.exceptions.ParserException} that represents the error in evaluating this
	 *         equation if there was one, otherwise null.
	 */
	public ParserException getError() {
		return error;
	}
	
	/**
	 * Stores the input, result, matrix array, and error in a {@link JSONObject} and returns it.
	 * 
	 * @return a {@link JSONObject} that represents this {@link Equation}.
	 */
	@Override
	public JSONObject toJSONObject() {
		JSONObject output = new JSONObject();
		output.put("Equation", JSONSystem.wrap(input));
		output.put("Answer", JSONSystem.wrap(result));
		output.put("Error", JSONSystem.wrap(error));
		return output;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Equation))
			return false;
		return input.equals(((Equation) o).input) && result.equals(((Equation) o).result);
	}
	
	/**
	 * @return true if the equation and answer {@link lipstone.joshua.parser.tokenizer.ConsCell Tokens'}
	 *         {lipstone.joshua.parser.tokenizer.Token#isNull() isNull()} method return true
	 */
	public boolean isEmpty() {
		return input.isNull() && result.isNull();
	}
	
	/**
	 * @return the result of evaluating this equation. If there was an error, it returns the error text instead.
	 */
	public String resultString() {
		return error != null ? error.getMessage() : result.toString();
	}
	
	/**
	 * Constructs and returns a String of the form, "input {@literal ->} result", where the result is from
	 * {@link #resultString()}
	 * 
	 * @see #resultString()
	 */
	@Override
	public String toString() {
		return input.toString() + " -> " + (error != null ? error.getMessage() : result.toString());
	}
}
