package lipstone.joshua.parser.util;

import lipstone.joshua.parser.exceptions.UndefinedResultException;
import lipstone.joshua.parser.types.BigDec;

@FunctionalInterface
public interface MathFunction {
	public BigDec calculate(BigDec input1, BigDec input2) throws UndefinedResultException;
}
