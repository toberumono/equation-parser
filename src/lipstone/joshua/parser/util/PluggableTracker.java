package lipstone.joshua.parser.util;

import lipstone.joshua.parser.exceptions.PluginConflictException;

/**
 * Indicates that the implementing object tracks {@link lipstone.joshua.parser.plugin.pluggable.Pluggable Pluggable} items
 * placed in it.
 * 
 * @author Joshua Lipstone
 */
public interface PluggableTracker extends Tracker {
	
	/**
	 * Clears all of the {@link lipstone.joshua.parser.plugin.pluggable.Pluggable Pluggables} tracked by the implementing
	 * object.
	 * 
	 * @throws PluginConflictException
	 *             if a {@link lipstone.joshua.parser.plugin.pluggable.Pluggable Pluggable} item could not be correctly removed
	 */
	public void clearTrackedPluggables() throws PluginConflictException;
}
