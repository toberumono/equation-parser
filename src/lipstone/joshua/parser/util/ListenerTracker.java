package lipstone.joshua.parser.util;

/**
 * Indicates that the implementing object tracks listeners placed on it.
 * 
 * @author Joshua Lipstone
 */
public interface ListenerTracker extends Tracker {
	
	/**
	 * Clears all of the listeners tracked by the implementing object.
	 */
	public void clearTrackedListeners();
}
