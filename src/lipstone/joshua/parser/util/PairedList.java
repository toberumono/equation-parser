package lipstone.joshua.parser.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PairedList<T, V> implements Iterable<T> {
	private List<T> keys;
	private List<V> values;
	
	/**
	 * Constructs a new paired list
	 */
	public PairedList() {
		keys = new ArrayList<>();
		values = new ArrayList<>();
	}
	
	/**
	 * If key is already in the list, it overwrites the value associated with key. Otherwise, it adds it to the list.
	 * 
	 * @param key
	 *            the key to set the value of
	 * @param value
	 *            the value to set the key to
	 */
	public void put(T key, V value) {
		int index = keys.indexOf(key);
		if (index < 0) {
			keys.add(key);
			values.add(value);
			return;
		}
		values.set(index, value);
	}
	
	/**
	 * @param key
	 *            the key to get the value of
	 * @return the value associated with the key if the key is in the map, otherwise null
	 */
	public V get(T key) {
		int index = keys.indexOf(key);
		if (index < 0)
			return null;
		return values.get(index);
	}
	
	/**
	 * @param key
	 *            the key to remove
	 * @return the value mapped to the key or null if the key was not in the list
	 */
	public V remove(T key) {
		int index = keys.indexOf(key);
		if (index < 0)
			return null;
		keys.remove(index);
		return values.remove(index);
	}
	
	/**
	 * @param key
	 *            the key to look for
	 * @return true if the given key is in the map
	 */
	public boolean containsKey(T key) {
		return keys.indexOf(key) >= 0;
	}
	
	/**
	 * @return the size of this list, which is equal to the number of keys within the list.
	 */
	public int size() {
		return keys.size();
	}
	
	/**
	 * @return the keys in this map. Please note that these keys are completely separate from the map
	 */
	public List<T> getKeys() {
		return new ArrayList<>(keys);
	}
	
	/**
	 * @return the values in this map. Please note that these values are completely separate from the map
	 */
	public List<V> getValues() {
		return new ArrayList<>(values);
	}
	
	@Override
	public String toString() {
		String output = "";
		for (T key : keys)
			output = output + "<" + key.toString() + ", " + get(key).toString() + ">, ";
		if (output.endsWith(", "))
			output = output.substring(0, output.length() - 2);
		return output;
	}

	@Override
	public Iterator<T> iterator() {
		return keys.iterator();
	}
}
