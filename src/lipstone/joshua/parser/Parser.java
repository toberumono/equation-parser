package lipstone.joshua.parser;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

import toberumono.json.JSONSystem;
import toberumono.structures.tuples.Pair;

import lipstone.joshua.parser.cas.CAS;
import lipstone.joshua.parser.cas.Solver;
import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.exceptions.PluginConflictException;
import lipstone.joshua.parser.exceptions.PluginInstantiationException;
import lipstone.joshua.parser.exceptions.SyntaxException;
import lipstone.joshua.parser.exceptions.UnbalancedDescendersException;
import lipstone.joshua.parser.plugin.ParserPermissions;
import lipstone.joshua.parser.plugin.ParserSettingsPlugin;
import lipstone.joshua.parser.plugin.mapping.ObservableParserPluginMap;
import lipstone.joshua.parser.plugin.mapping.ObservablePluggableCollectionIterator;
import lipstone.joshua.parser.plugin.mapping.ObservablePluggableMap;
import lipstone.joshua.parser.plugin.mapping.ObservablePluggableSourcedMap;
import lipstone.joshua.parser.plugin.pluggable.Action;
import lipstone.joshua.parser.plugin.pluggable.AngleType;
import lipstone.joshua.parser.plugin.pluggable.Command;
import lipstone.joshua.parser.plugin.pluggable.Filter;
import lipstone.joshua.parser.plugin.pluggable.Keyword;
import lipstone.joshua.parser.plugin.pluggable.Operation;
import lipstone.joshua.parser.plugin.pluggable.Operator;
import lipstone.joshua.parser.plugin.pluggable.Pluggable;
import lipstone.joshua.parser.plugin.pluggable.ScrubbingFunction;
import lipstone.joshua.parser.tokenizer.ConsCell;
import lipstone.joshua.parser.tokenizer.ConsType;
import lipstone.joshua.parser.tokenizer.Tokenizer;
import lipstone.joshua.parser.tokenizer.TokenizerDescender;
import lipstone.joshua.parser.tokenizer.TokenizerRule;
import lipstone.joshua.parser.types.BigDec;
import lipstone.joshua.parser.util.ListenerTracker;
import lipstone.joshua.parser.util.PluggableTracker;
import lipstone.joshua.parser.util.Tracker;
import lipstone.joshua.parser.util.WritableObservableValue;
import lipstone.joshua.pluginLoader.Plugin;
import lipstone.joshua.pluginLoader.PluginUser;

import static lipstone.joshua.parser.plugin.pluggable.Operator.noOp;
import static lipstone.joshua.parser.tokenizer.ConsType.*;

/**
 * The core evaluation system for this library
 * 
 * @author Joshua Lipstone
 */
public class Parser implements Evaluator, PluginUser<ParserPlugin> {
	public final ObservablePluggableSourcedMap<Operation> operations;
	public final ObservablePluggableSourcedMap<Operator> operators;
	public final ObservablePluggableSourcedMap<Keyword> keywords;
	public final ObservablePluggableSourcedMap<Command> commands;
	public final ObservablePluggableSourcedMap<Filter> preprocessingFilters;
	public final ObservablePluggableSourcedMap<Filter> postprocessingFilters;
	public final ObservablePluggableSourcedMap<AngleType> angleTypes;
	public final ObservablePluggableSourcedMap<TokenizerRule> tokenizerRules;
	public final ObservablePluggableSourcedMap<TokenizerDescender> tokenizerDescenders;
	public final ObservablePluggableSourcedMap<Solver> casSolvers;
	public final ObservableParserPluginMap plugins;
	private final List<ParserPlugin> settingsPlugins;
	private final HashMap<String, String> abbreviations;
	private final HashMap<String, String> tokenizeablePluggableRegistry;
	private final List<ListenerTracker> listenerTrackers = new ArrayList<>();
	private final List<PluggableTracker> pluggableTrackers = new ArrayList<>();
	private final Logger logger;
	
	static {
		JSONSystem.setNumberHandlers(BigDec.class, BigDec::new, BigDec::toString);
	}
	
	static class ParserPluginWrapper {
		public ParserPlugin wrapped;
	}
	
	private final ParserPluginWrapper lastPlugin;
	
	private final ParserCore core;
	private final ParserPermissions permissions;
	
	/**
	 * The system of angle measure currently in use by this {@link Parser}
	 */
	public final WritableObservableValue<AngleType> activeAngleType;
	private final List<String> vars;
	
	public final CAS cas;
	public final Tokenizer tokenizer;
	
	public final String CREDITS = "Joshua Lipstone: Original idea, programming, debugging\nJacob Spear: The cool math functions, efficiency, debugging";
	
	class ParserWritableObservableValue<T> extends WritableObservableValue<T> {
		public ParserWritableObservableValue(T initial) {
			super(initial);
			addTrackableField(this);
		}
		
		public ParserWritableObservableValue(WritableObservableValue<T> base) {
			super(base);
			addTrackableField(this);
		}
	}
	
	class ParserObservablePluggableSourcedMap<T extends Pluggable> extends ObservablePluggableSourcedMap<T> {
		public ParserObservablePluggableSourcedMap(Map<String, T> backing, ParserPermissions permissions) {
			super(backing, permissions);
			addTrackableField(this);
		}
		
		public ParserObservablePluggableSourcedMap(ParserObservablePluggableSourcedMap<T> base, ParserPermissions permissions) {
			super(base, permissions);
			addTrackableField(this);
		}
	}
	
	class ObservableConsCellizeableSourcedMap<T extends Pluggable> extends ParserObservablePluggableSourcedMap<T> {
		protected final String typeName;
		
		public ObservableConsCellizeableSourcedMap(Map<String, T> backing, ParserPermissions permissions, String typeName) {
			super(backing, permissions);
			this.typeName = typeName;
		}
		
		public ObservableConsCellizeableSourcedMap(ObservableConsCellizeableSourcedMap<T> base, ParserPermissions permissions) {
			super(base, permissions);
			typeName = base.typeName;
		}
		
		@Override
		public T tryPut(String key, T action) throws PluginConflictException {
			if (tokenizeablePluggableRegistry.containsKey(key) && !containsKey(key))
				throw new PluginConflictException(key + " is already loaded into this Parser as a " + tokenizeablePluggableRegistry.get(key) +
						".  It must be removed from the " + tokenizeablePluggableRegistry.get(key) + "s map before it can be added to this one.", permissions.getPlugin());
			T output = super.tryPut(key, action);
			tokenizeablePluggableRegistry.put(key, typeName);
			return output;
		}
		
		@Override
		protected void doRemove(String key, T value) throws PluginConflictException {
			super.doRemove(key, value);
			tokenizeablePluggableRegistry.remove(key);
		}
	}
	
	class ObservableActionSourcedMap<T extends Action> extends ObservableConsCellizeableSourcedMap<T> {
		
		public ObservableActionSourcedMap(Map<String, T> backing, ParserPermissions permissions, String typeName) {
			super(backing, permissions, typeName);
		}
		
		public ObservableActionSourcedMap(ObservableActionSourcedMap<T> base, ParserPermissions permissions) {
			super(base, permissions);
		}
		
		@Override
		public T tryGet(Object key) throws PluginConflictException {
			T output = super.tryGet(key);
			return output == null && abbreviations.containsKey(key) ? super.tryGet(abbreviations.get(key)) : output;
		}
		
		@Override
		public T tryPut(String key, T action) throws PluginConflictException {
			for (String alt : action.getAlternateNames()) {
				String test = abbreviations.get(alt);
				if (test != null && !test.equals(key))
					throw new PluginConflictException("The abbreviation, \"" + alt + "\" is already mapped to " + test +
							" and must be removed before it can be used to point to a different value.", permissions.getPlugin());
			}
			T output = super.tryPut(key, action);
			for (String name : action.getAlternateNames()) {
				abbreviations.put(name, key);
				tokenizeablePluggableRegistry.put(key, typeName);
			}
			return output;
		}
		
		@Override
		protected void doRemove(String key, T value) throws PluginConflictException {
			super.doRemove(key, value);
			for (String name : value.getAlternateNames()) {
				abbreviations.remove(name);
				tokenizeablePluggableRegistry.remove(name);
			}
		}
	}
	
	class ObservableAngleTypeSourcedMap extends ParserObservablePluggableSourcedMap<AngleType> {
		
		public ObservableAngleTypeSourcedMap(Map<String, AngleType> backing, ParserPermissions permissions) {
			super(backing, permissions);
		}
		
		public ObservableAngleTypeSourcedMap(ObservablePluggableMap<AngleType> base, ParserPermissions permissions) {
			super(base, permissions);
		}
		
		@Override
		protected void doPut(String key, AngleType value) {
			super.doPut(key, value);
			if (activeAngleType.get().equals(key) || activeAngleType.get().equals(AngleType.NONE))
				activeAngleType.set(value);
		}
		
		@Override
		protected void doRemove(String key, AngleType value) throws PluginConflictException {
			super.doRemove(key, value);
			if (activeAngleType.get().equals(key))
				activeAngleType.set(AngleType.NONE);
		}
	}
	
	class InternalObservableParserPluginMap extends ObservableParserPluginMap {
		
		public InternalObservableParserPluginMap(Map<String, ParserPlugin> backing, ParserPermissions permissions) {
			super(backing, permissions);
		}
		
		public InternalObservableParserPluginMap(InternalObservableParserPluginMap base, ParserPermissions permissions) {
			super(base, permissions);
		}
		
		@Override
		public ParserPlugin add(Class<? extends ParserPlugin> plugin) throws PluginInstantiationException, PluginConflictException {
			ParserPlugin p;
			try {
				p = plugin.newInstance();
			}
			catch (InstantiationException | IllegalAccessException | IllegalArgumentException | SecurityException e) {
				throw new PluginInstantiationException("Failed to load " + plugin.getAnnotation(Plugin.class).id(), null);
			}
			return add(p);
		}
		
		@Override
		public ParserPlugin tryPut(String key, ParserPlugin value) throws PluginConflictException {
			ParserPlugin previous = lastPlugin.wrapped;
			try {
				lastPlugin.wrapped = value;
				ParserPlugin output = super.tryPut(key, value);
				if (value.getClass().getAnnotation(ParserSettingsPlugin.class) != null)
					settingsPlugins.add(value);
				ParserPermissions permissions = ParserPermissions.makeStandardPluginPermissions(value, Parser.this.permissions);
				Parser.this.permissions.loadPlugin(value, permissions);
				Parser loading = new Parser(Parser.this, permissions);
				value.loadPlugin(loading, Parser.this.permissions.getPlugin());
				linkedMaps.put(key, loading.plugins);
				logger.log(Level.INFO, "Loaded " + value.getID());
				return output;
			}
			finally {
				lastPlugin.wrapped = previous;
			}
		}
		
		@Override
		protected void doRemove(String key, ParserPlugin value) throws PluginConflictException {
			super.doRemove(key, value);
			Parser.this.permissions.unloadPlugin(value);
			for (ListenerTracker tracker : listenerTrackers)
				tracker.clearTrackedListeners();
			for (PluggableTracker tracker : pluggableTrackers)
				tracker.clearTrackedPluggables();
			value.unloadPlugin();
			if (value.getClass().getAnnotation(ParserSettingsPlugin.class) != null)
				settingsPlugins.remove(value);
		}
		
		@Override
		public ObservablePluggableCollectionIterator<Class<ParserPlugin>> pluginClassIterator() {
			return new ObservablePluggableCollectionIterator<Class<ParserPlugin>>() {
				ObservablePluggableCollectionIterator<Entry<String, ParserPlugin>> entryIterator = InternalObservableParserPluginMap.this.entrySet().iterator();
				
				@Override
				public boolean hasNext() {
					return entryIterator.hasNext();
				}
				
				@Override
				public Class<ParserPlugin> next() {
					@SuppressWarnings("unchecked")
					Class<ParserPlugin> ret = (Class<ParserPlugin>) entryIterator.next().getValue().getClass();
					return ret;
				}
				
				@Override
				public void tryRemove() throws PluginConflictException {
					entryIterator.tryRemove();
				}
			};
		}
	}
	
	/**
	 * Constructs a new {@link Parser} object with an automatically determined baseLocation, and initializes the File IO
	 * systems.
	 * 
	 * @param core
	 *            the {@link ParserCore} that is using this {@link Parser}
	 */
	Parser(ParserCore core, ParserPermissions permissions) {
		this.core = core;
		this.permissions = permissions;
		logger = Logger.getLogger("toberumono.parser.Parser");
		lastPlugin = new ParserPluginWrapper();
		abbreviations = new HashMap<>();
		tokenizeablePluggableRegistry = new HashMap<>();
		activeAngleType = new ParserWritableObservableValue<>(AngleType.NONE);
		operations = new ObservableActionSourcedMap<>(new HashMap<>(), permissions, "Operation");
		operators = new ObservableConsCellizeableSourcedMap<>(new HashMap<>(), permissions, "Operator");
		keywords = new ObservableActionSourcedMap<>(new HashMap<>(), permissions, "Keyword");
		commands = new ObservableActionSourcedMap<>(new HashMap<>(), permissions, "Command");
		preprocessingFilters = new ParserObservablePluggableSourcedMap<>(new HashMap<>(), permissions);
		postprocessingFilters = new ParserObservablePluggableSourcedMap<>(new HashMap<>(), permissions);
		angleTypes = new ObservableAngleTypeSourcedMap(new HashMap<>(), permissions);
		plugins = new InternalObservableParserPluginMap(new HashMap<>(), permissions);
		casSolvers = new ParserObservablePluggableSourcedMap<>(new HashMap<>(), permissions);
		tokenizer = new Tokenizer(this, core.internalPlugin);
		Pair<Map<String, TokenizerRule>, Map<String, TokenizerDescender>> tokenizerMaps = tokenizer.getMaps();
		tokenizerRules = new ParserObservablePluggableSourcedMap<>(tokenizerMaps.getX(), permissions);
		tokenizerDescenders = new ParserObservablePluggableSourcedMap<>(tokenizerMaps.getY(), permissions);
		settingsPlugins = new ArrayList<>();
		vars = new ArrayList<>();
		cas = new CAS(this);
	}
	
	private Parser(Parser source, ParserPermissions permissions) {
		core = source.core;
		this.permissions = permissions;
		logger = Logger.getLogger("toberumono.parser.Parser." + permissions.getPlugin().getID());
		lastPlugin = source.lastPlugin;
		activeAngleType = new ParserWritableObservableValue<>(source.activeAngleType);
		tokenizeablePluggableRegistry = source.tokenizeablePluggableRegistry;
		abbreviations = source.abbreviations;
		operations = new ObservableActionSourcedMap<>((ObservableActionSourcedMap<Operation>) source.operations, permissions);
		operators = new ObservableConsCellizeableSourcedMap<>((ObservableConsCellizeableSourcedMap<Operator>) source.operators, permissions);
		keywords = new ObservableActionSourcedMap<>((ObservableActionSourcedMap<Keyword>) source.keywords, permissions);
		commands = new ObservableActionSourcedMap<>((ObservableActionSourcedMap<Command>) source.commands, permissions);
		preprocessingFilters = new ParserObservablePluggableSourcedMap<>(source.preprocessingFilters, permissions);
		postprocessingFilters = new ParserObservablePluggableSourcedMap<>(source.postprocessingFilters, permissions);
		tokenizerRules = new ParserObservablePluggableSourcedMap<>(source.tokenizerRules, permissions);
		tokenizerDescenders = new ParserObservablePluggableSourcedMap<>(source.tokenizerDescenders, permissions);
		casSolvers = new ParserObservablePluggableSourcedMap<>(source.casSolvers, permissions);
		angleTypes = new ObservableAngleTypeSourcedMap(source.angleTypes, permissions);
		plugins = new InternalObservableParserPluginMap(source.plugins, permissions);
		listenerTrackers.addAll(Arrays.asList(new ListenerTracker[]{activeAngleType, operations, operators, keywords, commands, preprocessingFilters,
				postprocessingFilters, angleTypes, plugins, casSolvers, tokenizerRules, tokenizerDescenders}));
		pluggableTrackers.addAll(Arrays.asList(new PluggableTracker[]{operations, operators, keywords, commands, preprocessingFilters,
				postprocessingFilters, angleTypes, casSolvers, tokenizerRules, tokenizerDescenders}));
		settingsPlugins = source.settingsPlugins;
		vars = source.vars;
		cas = source.cas;
		tokenizer = source.tokenizer;
	}
	
	/**
	 * This is <i>not</i> the default plugin location. To get the default plugin location, use:
	 * <code>getBaseLocation() + "/plugins/"</code>
	 * 
	 * @return the location that this <tt>PluginUser</tt> is running from.
	 */
	@Override
	public Path getBaseLocation() {
		return core.getBaseLocation();
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	//Plugin handling stuff goes here
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Adds a field that implements either {@link ListenerTracker} or {@link PluggableTracker}
	 * 
	 * @param tracker
	 *            the field to track
	 */
	public void addTrackableField(Tracker tracker) {
		if (tracker instanceof ListenerTracker && !listenerTrackers.contains(tracker))
			listenerTrackers.add((ListenerTracker) tracker);
		if (tracker instanceof PluggableTracker && !pluggableTrackers.contains(tracker))
			pluggableTrackers.add((PluggableTracker) tracker);
	}
	
	/**
	 * Removes all loaded plugins from the {@link Parser}
	 * 
	 * @throws PluginConflictException
	 *             if the plugin tries to remove operations, keywords, or commands that another plugin registered
	 */
	public void clearPlugins() throws PluginConflictException {
		plugins.tryClear();
		logger.log(Level.INFO, "Cleared all plugins.");
	}
	
	/**
	 * Checks if this {@link Parser} contains the specified plugin by ID
	 * 
	 * @param plugin
	 *            the plugin to check for
	 * @return whether this {@link Parser} contains the specified plugin in its plugin registry
	 */
	public boolean containsPlugin(ParserPlugin plugin) {
		return plugins.containsValue(plugin);
	}
	
	/**
	 * Use this method to gain access to the {@link ObservablePluggableSourcedMap map} of {@link Operation Operations}, which
	 * has methods for adding and removing {@link Operation Operations}, as well as listening for changes (additions,
	 * removals, or replacements) in the {@link Operation Operations} {@link ObservablePluggableSourcedMap map}.
	 * 
	 * @return the {@link ObservablePluggableSourcedMap map} of {@link Operation Operations} loaded into this {@link Parser}
	 */
	@Deprecated
	public ObservablePluggableSourcedMap<Operation> getOperations() {
		return operations;
	}
	
	/**
	 * Use this method to gain access to the {@link ObservablePluggableSourcedMap map} of {@link Operator Operators}, which
	 * has methods for adding and removing {@link Operator Operators}, as well as listening for changes (additions, removals,
	 * or replacements) in the {@link Operator Operators} {@link ObservablePluggableSourcedMap map}.
	 * 
	 * @return the {@link ObservablePluggableSourcedMap map} of {@link Operator Operators} loaded into this {@link Parser}
	 */
	@Deprecated
	public ObservablePluggableSourcedMap<Operator> getOperators() {
		return operators;
	}
	
	/**
	 * Use this method to gain access to the {@link ObservablePluggableSourcedMap map} of {@link Command Commands}, which has
	 * methods for adding and removing {@link Command Commands}, as well as listening for changes (additions, removals, or
	 * replacements) in the {@link Command Commands} {@link ObservablePluggableSourcedMap map}.
	 * 
	 * @return the {@link ObservablePluggableSourcedMap map} of {@link Command Commands} loaded into this {@link Parser}
	 */
	@Deprecated
	public ObservablePluggableSourcedMap<Command> getCommands() {
		return commands;
	}
	
	/**
	 * Use this method to gain access to the {@link ObservablePluggableSourcedMap map} of {@link Keyword Keywords}, which has
	 * methods for adding and removing {@link Keyword Keywords}, as well as listening for changes (additions, removals, or
	 * replacements) in the {@link Keyword Keywords} {@link ObservablePluggableSourcedMap map}.
	 * 
	 * @return the {@link ObservablePluggableSourcedMap map} of {@link Keyword Keywords} loaded into this {@link Parser}
	 */
	@Deprecated
	public ObservablePluggableSourcedMap<Keyword> getKeywords() {
		return keywords;
	}
	
	/**
	 * Use this method to gain access to the {@link ObservablePluggableSourcedMap map} of {@link Filter PreprocessingFilters}
	 * , which has methods for adding and removing {@link Filter PreprocessingFilters}, as well as listening for changes
	 * (additions, removals, or replacements) in the {@link Filter PreprocessingFilters} {@link ObservablePluggableSourcedMap
	 * map}.
	 * 
	 * @return the {@link ObservablePluggableSourcedMap map} of {@link Filter PreprocessingFilters} loaded into this
	 *         {@link Parser}
	 */
	@Deprecated
	public ObservablePluggableSourcedMap<Filter> getPreprocessingFilters() {
		return preprocessingFilters;
	}
	
	/**
	 * Use this method to gain access to the {@link ObservablePluggableSourcedMap map} of {@link Filter
	 * PostprocessingFilters}, which has methods for adding and removing {@link Filter PostprocessingFilters}, as well as
	 * listening for changes (additions, removals, or replacements) in the {@link Filter PostprocessingFilters}
	 * {@link ObservablePluggableSourcedMap map}.
	 * 
	 * @return the {@link ObservablePluggableSourcedMap map} of {@link Filter PostprocessingFilters} loaded into this
	 *         {@link Parser}
	 */
	@Deprecated
	public ObservablePluggableSourcedMap<Filter> getPostprocessingFilters() {
		return postprocessingFilters;
	}
	
	/**
	 * Use this method to gain access to the {@link ObservablePluggableSourcedMap map} of {@link AngleType AngleTypes}, which
	 * has methods for adding and removing {@link AngleType AngleTypes}, as well as listening for changes (additions,
	 * removals, or replacements) in the {@link AngleType AngleTypes} {@link ObservablePluggableSourcedMap map}.
	 * 
	 * @return the {@link ObservablePluggableSourcedMap map} of {@link AngleType AngleTypes} loaded into this {@link Parser}
	 */
	@Deprecated
	public ObservablePluggableSourcedMap<AngleType> getAngleTypes() {
		return angleTypes;
	}
	
	private final <T extends Pluggable> T loadPluggable(T item, String type, String propertyName, ObservablePluggableSourcedMap<T> map, ParserPlugin plugin) throws PluginConflictException {
		return map.add(item);
	}
	
	private final <T extends Pluggable> T unloadPluggable(String name, String type, String propertyName, ObservablePluggableSourcedMap<T> map, ParserPlugin plugin) throws PluginConflictException {
		return map.tryRemove(name);
	}
	
	/**
	 * Adds a Preprocessing filter to this {@link Parser}
	 * 
	 * @param filter
	 *            the {@link Filter} to add
	 * @param plugin
	 *            the {@link ParserPlugin} adding the {@link Filter}
	 * @return if there was an {@link Filter} under the same name previously mapped, it is returned, otherwise null
	 * @throws PluginConflictException
	 *             when a {@link Filter} of the same name already exists and does not belong to the plugin adding it
	 */
	@Deprecated
	public synchronized Filter addPreprocessingFilter(Filter filter, ParserPlugin plugin) throws PluginConflictException {
		return loadPluggable(filter, "preprocessing filter", "PreprocessingFilter", preprocessingFilters, plugin);
	}
	
	/**
	 * Removes a Preprocessing filter from this {@link Parser} by name
	 * 
	 * @param name
	 *            the name of the {@link Filter} to remove
	 * @param plugin
	 *            the {@link ParserPlugin} removing this {@link Filter}
	 * @return the {@link Filter} that was removed
	 * @throws PluginConflictException
	 *             when the {@link Filter} does not belong to the {@link ParserPlugin} removing it
	 */
	@Deprecated
	public synchronized Filter removePreprocessingFilter(String name, ParserPlugin plugin) throws PluginConflictException {
		return unloadPluggable(name, "preprocessing filter", "PreprocessingFilter", preprocessingFilters, plugin);
	}
	
	/**
	 * Adds a Postprocessing filter to this {@link Parser}
	 * 
	 * @param filter
	 *            the {@link Filter} to add
	 * @param plugin
	 *            the {@link ParserPlugin} adding the {@link Filter}
	 * @return if there was an {@link Filter} under the same name previously mapped, it is returned, otherwise null
	 * @throws PluginConflictException
	 *             when a {@link Filter} of the same name already exists and does not belong to the {@link ParserPlugin}
	 *             adding it
	 */
	@Deprecated
	public synchronized Filter addPostprocessingFilter(Filter filter, ParserPlugin plugin) throws PluginConflictException {
		return loadPluggable(filter, "postprocessing filter", "PostprocessingFilter", postprocessingFilters, plugin);
	}
	
	/**
	 * Removes a Postprocessing filter from this {@link Parser} by name
	 * 
	 * @param name
	 *            the name of the {@link Filter} to remove
	 * @param plugin
	 *            the {@link ParserPlugin} removing the {@link Filter}
	 * @return the {@link Filter} that was removed
	 * @throws PluginConflictException
	 *             when the {@link Filter} does not belong to the {@link ParserPlugin} removing it
	 */
	@Deprecated
	public synchronized Filter removePostprocessingFilter(String name, ParserPlugin plugin) throws PluginConflictException {
		return unloadPluggable(name, "postprocessing filter", "PostprocessingFilter", postprocessingFilters, plugin);
	}
	
	/**
	 * Adds the specified {@link Operation} to this {@link Parser}.<br>
	 * Note: A plugin cannot override an already-loaded plugin's {@link Operation} - it will throw a
	 * {@link PluginConflictException} instead.
	 * 
	 * @param operation
	 *            the {@link Operation} to add in object form
	 * @param plugin
	 *            the plugin adding the {@link Operation}
	 * @return if there was an {@link Operation} under the same name previously mapped, it is returned, otherwise null
	 * @throws PluginConflictException
	 *             when the {@link Operation} to be added already exists and does not belong to the plugin being mapped
	 */
	@Deprecated
	public synchronized Operation addOperation(Operation operation, ParserPlugin plugin) throws PluginConflictException {
		return loadPluggable(operation, "operation", "Operation", operations, plugin);
	}
	
	/**
	 * Removes an {@link Operation} from this {@link Parser}.
	 * 
	 * @param name
	 *            the name of the {@link Operation} to be removed
	 * @param plugin
	 *            the plugin removing the {@link Operation}
	 * @return the {@link Operation} that was removed, or null if there wasn't one
	 * @throws PluginConflictException
	 *             when the {@link Operation} is not owned by the plugin trying to remove it
	 */
	@Deprecated
	public synchronized Operation removeOperation(String name, ParserPlugin plugin) throws PluginConflictException {
		return unloadPluggable(name, "operation", "Operation", operations, plugin);
	}
	
	/**
	 * Adds the specified {@link Keyword} to this {@link Parser}.<br>
	 * Note: A plugin cannot override an already-loaded plugin's {@link Keyword} - it will throw a
	 * {@link PluginConflictException} instead.
	 * 
	 * @param keyword
	 *            the {@link Keyword} to add in object form
	 * @param plugin
	 *            the plugin adding the {@link Keyword}
	 * @return if there was a {@link Keyword} under the same name previously mapped, it is returned, otherwise null
	 * @throws PluginConflictException
	 *             when the {@link Keyword} to be added already exists and does not belong to the plugin being mapped
	 */
	@Deprecated
	public synchronized Keyword addKeyword(Keyword keyword, ParserPlugin plugin) throws PluginConflictException {
		return loadPluggable(keyword, "keyword", "Keyword", keywords, plugin);
	}
	
	/**
	 * Removes a {@link Keyword} from this {@link Parser}.
	 * 
	 * @param name
	 *            the name of the {@link Keyword} to be removed
	 * @param plugin
	 *            the plugin removing the {@link Keyword}
	 * @return the {@link Keyword} that was removed, or null if there wasn't one
	 * @throws PluginConflictException
	 *             when the {@link Keyword} is not owned by the plugin trying to remove it
	 */
	@Deprecated
	public synchronized Keyword removeKeyword(String name, ParserPlugin plugin) throws PluginConflictException {
		return unloadPluggable(name, "keyword", "Keyword", keywords, plugin);
	}
	
	/**
	 * Adds the specified {@link Command} to this {@link Parser}.<br>
	 * Note: A plugin cannot override an already-loaded plugin's {@link Command} - it will throw a
	 * {@link PluginConflictException} instead.
	 * 
	 * @param command
	 *            the {@link Command} to add in object form
	 * @param plugin
	 *            the plugin adding the {@link Command}
	 * @return if there was a {@link Command} under the same name previously mapped, it is returned, otherwise null
	 * @throws PluginConflictException
	 *             when the {@link Command} to be added already exists and does not belong to the plugin being mapped
	 */
	@Deprecated
	public synchronized Command addCommand(Command command, ParserPlugin plugin) throws PluginConflictException {
		return loadPluggable(command, "command", "Command", commands, plugin);
	}
	
	/**
	 * Removes a {@link Command} from this {@link Parser}.
	 * 
	 * @param name
	 *            the name of the {@link Command} to be removed
	 * @param plugin
	 *            the plugin removing the {@link Command}
	 * @return the {@link Command} that was removed, or null if there wasn't one
	 * @throws PluginConflictException
	 *             when the {@link Command} is not owned by the plugin trying to remove it
	 */
	@Deprecated
	public synchronized Command removeCommand(String name, ParserPlugin plugin) throws PluginConflictException {
		return unloadPluggable(name, "command", "Command", commands, plugin);
	}
	
	/**
	 * Adds the specified {@link Operator} to this {@link Parser}.<br>
	 * Note: A plugin cannot override an already-loaded plugin's {@link lipstone.joshua.parser.plugin.pluggable.Operator
	 * Operator} - it will throw a {@link PluginConflictException} instead.
	 * 
	 * @param operator
	 *            the {@link Operator} to add in object form
	 * @param plugin
	 *            the plugin adding the {@link Operator}
	 * @return if there was an {@link Operator} under the same name previously mapped, it is returned, otherwise null
	 * @throws PluginConflictException
	 *             when the {@link Operator} to be added already exists and does not belong to the plugin being mapped
	 */
	@Deprecated
	public synchronized Operator addOperator(Operator operator, ParserPlugin plugin) throws PluginConflictException {
		return loadPluggable(operator, "operator", "Operator", operators, plugin);
	}
	
	/**
	 * Removes an {@link Operator} from this {@link Parser}.
	 * 
	 * @param name
	 *            the name of the {@link Operator} to be removed
	 * @param plugin
	 *            the plugin removing the {@link Operator}
	 * @return the {@link Operator} that was removed, or null if there wasn't one
	 * @throws PluginConflictException
	 *             when the {@link Operator} is not owned by the plugin trying to remove it
	 */
	@Deprecated
	public synchronized Operator removeOperator(String name, ParserPlugin plugin) throws PluginConflictException {
		return unloadPluggable(name, "operator", "Operator", operators, plugin);
	}
	
	/**
	 * Adds the specified {@link AngleType} to this {@link Parser}.<br>
	 * Note: A plugin cannot override an already-loaded plugin's {@link AngleType} - it will throw a
	 * {@link PluginConflictException} instead.
	 * 
	 * @param angleType
	 *            the {@link AngleType} to add in object form
	 * @param plugin
	 *            the plugin adding the {@link AngleType}
	 * @return if there was an {@link AngleType} under the same name previously mapped, it is returned, otherwise null
	 * @throws PluginConflictException
	 *             when the {@link AngleType} to be added already exists and does not belong to the plugin being mapped
	 */
	@Deprecated
	public synchronized AngleType addAngleType(AngleType angleType, ParserPlugin plugin) throws PluginConflictException {
		return loadPluggable(angleType, "angle type", "AngleType", angleTypes, plugin);
	}
	
	/**
	 * Removes an {@link AngleType} from this {@link Parser}.
	 * 
	 * @param name
	 *            the name of the {@link AngleType} to be removed
	 * @param plugin
	 *            the plugin removing the {@link AngleType}
	 * @return the {@link AngleType} that was removed, or null if there wasn't one
	 * @throws PluginConflictException
	 *             when the {@link AngleType} is not owned by the plugin trying to remove it
	 */
	@Deprecated
	public synchronized AngleType removeAngleType(String name, ParserPlugin plugin) throws PluginConflictException {
		return unloadPluggable(name, "angle type", "AngleType", angleTypes, plugin);
	}
	
	/**
	 * Adds a {@link TokenizerRule} to this {@link Parser}
	 * 
	 * @param name
	 *            the name of the {@link TokenizerRule rule} to add
	 * @param rule
	 *            the {@link TokenizerRule} to add
	 * @param plugin
	 *            the {@link ParserPlugin} adding this {@link TokenizerRule rule}
	 * @return if there was an {@link TokenizerRule} under the same name previously mapped, it is returned, otherwise null
	 * @throws PluginConflictException
	 *             when the {@link TokenizerRule} to be added already exists and does not belong to the plugin being mapped
	 */
	@Deprecated
	public synchronized TokenizerRule addTokenizerRule(String name, TokenizerRule rule, ParserPlugin plugin) throws PluginConflictException {
		return loadPluggable(rule, "rule", "TokenizerRule", tokenizerRules, plugin);
	}
	
	/**
	 * Removes a {@link TokenizerRule} from this {@link Parser}.
	 * 
	 * @param name
	 *            the name of the {@link TokenizerRule} to be removed
	 * @param plugin
	 *            the plugin removing the {@link TokenizerRule}
	 * @return the {@link TokenizerRule} that was removed
	 * @throws PluginConflictException
	 *             when the {@link TokenizerRule} is not owned by the plugin trying to remove it
	 */
	@Deprecated
	public synchronized TokenizerRule removeTokenizerRule(String name, ParserPlugin plugin) throws PluginConflictException {
		return unloadPluggable(name, "rule", "TokenizerRule", tokenizerRules, plugin);
	}
	
	/**
	 * Adds a {@link TokenizerDescender} to this {@link Parser}
	 * 
	 * @param name
	 *            the name of the {@link TokenizerDescender descender} to add
	 * @param descender
	 *            the {@link TokenizerDescender} to add
	 * @param plugin
	 *            the {@link ParserPlugin} adding this {@link TokenizerDescender descender}
	 * @return if there was an {@link TokenizerDescender} under the same name previously mapped, it is returned, otherwise
	 *         null
	 * @throws PluginConflictException
	 *             when the {@link TokenizerDescender} to be added already exists and does not belong to the plugin being
	 *             mapped
	 */
	@Deprecated
	public synchronized TokenizerDescender addTokenizerDescender(String name, TokenizerDescender descender, ParserPlugin plugin) throws PluginConflictException {
		return loadPluggable(descender, "descender", "TokenizerDescender", tokenizerDescenders, plugin);
	}
	
	/**
	 * Removes a {@link TokenizerDescender} from this {@link Parser}.
	 * 
	 * @param name
	 *            the name of the {@link TokenizerDescender} to be removed
	 * @param plugin
	 *            the plugin removing the {@link TokenizerDescender}
	 * @return the {@link TokenizerDescender} that was removed
	 * @throws PluginConflictException
	 *             when the {@link TokenizerDescender} is not owned by the plugin trying to remove it
	 */
	@Deprecated
	public synchronized TokenizerDescender removeTokenizerDescender(String name, ParserPlugin plugin) throws PluginConflictException {
		return unloadPluggable(name, "descender", "TokenizerDescender", tokenizerDescenders, plugin);
	}
	
	@Override
	public synchronized void loadPlugin(Class<? extends ParserPlugin> plugin) throws PluginInstantiationException, PluginConflictException {
		ParserPlugin p;
		try {
			p = plugin.newInstance();
		}
		catch (InstantiationException | IllegalAccessException | IllegalArgumentException | SecurityException e) {
			e.printStackTrace();
			throw new PluginInstantiationException("Failed to load " + plugin.getAnnotation(Plugin.class).id(), null);
		}
		ParserPlugin previous = lastPlugin.wrapped;
		try {
			lastPlugin.wrapped = p;
			plugins.add(p);
			logger.log(Level.INFO, "Loaded " + p.getID());
		}
		finally {
			lastPlugin.wrapped = previous;
		}
	}
	
	/**
	 * Unloads a ParserPlugin from the plugins registry
	 * 
	 * @throws PluginConflictException
	 *             if the plugin tries to remove operations, keywords, or commands that another plugin registered
	 */
	@Override
	public synchronized void unloadPlugin(String pluginID) throws PluginConflictException {
		ParserPlugin previous = lastPlugin.wrapped;
		try {
			lastPlugin.wrapped = plugins.get(pluginID);
			plugins.tryRemove(pluginID);
			logger.log(Level.INFO, "Unloaded " + pluginID);
		}
		finally {
			lastPlugin.wrapped = previous;
		}
	}
	
	//TODO Make better fail-safes
	/**
	 * Reloads all plugin data from the plugin registry
	 */
	@Override
	public synchronized void reloadPlugins() {
		List<Class<ParserPlugin>> plugins = new ArrayList<>(), failed = new ArrayList<>();
		String internalPluginID = InternalPlugin.class.getAnnotation(Plugin.class).id();
		for (ObservablePluggableCollectionIterator<Class<ParserPlugin>> i = this.plugins.pluginClassIterator(); i.hasNext();) {
			Class<ParserPlugin> plugin = i.next();
			if (plugin.getAnnotation(Plugin.class).id().equals(internalPluginID))
				continue;
			plugins.add(plugin);
			try {
				i.tryRemove();
			}
			catch (PluginConflictException e) {
				failed.add(plugins.remove(plugins.size() - 1));
				break;
			}
		}
		if (failed.size() == 0)
			try {
				this.plugins.tryRemove(internalPluginID);
				this.plugins.add(InternalPlugin.class);
			}
			catch (PluginConflictException | PluginInstantiationException e) {
				e.printStackTrace();
				logger.log(Level.SEVERE, "Unable to refresh plugins.", e);
				return;
			}
			
		for (Class<ParserPlugin> plugin : plugins)
			try {
				this.plugins.add(plugin);
			}
			catch (PluginInstantiationException | PluginConflictException e) {
				failed.add(plugin);
			}
		if (failed.size() == 0)
			logger.log(Level.INFO, "Successfully refreshed the plugins.");
		else {
			StringBuilder error = new StringBuilder();
			for (Class<ParserPlugin> id : failed)
				error = error.append(id.getAnnotation(Plugin.class).id()).append(", ");
			logger.log(Level.SEVERE, "Failed to refresh: " + (error.length() > 2 ? error.toString().substring(0, error.length() - 2) : error.toString()));
		}
	}
	
	@Override
	@Deprecated
	public ObservablePluggableSourcedMap<ParserPlugin> getPlugins() {
		return plugins;
	}
	
	/**
	 * Gets a given {@link Operation} by name or abbreviation.
	 * 
	 * @param operation
	 *            the name of or abbreviation for an {@link Operation} as a {@link String}
	 * @return the {@link Operation} if it exists, otherwise null
	 */
	@Deprecated
	public Operation getOperation(String operation) {
		return operations.get(operation);
	}
	
	/**
	 * Gets a given {@link Command} by name or abbreviation.
	 * 
	 * @param command
	 *            the name of or abbreviation for a {@link Command} as a {@link String}
	 * @return the {@link Command} if it exists, otherwise null
	 */
	@Deprecated
	public Command getCommand(String command) {
		return commands.get(command);
	}
	
	/**
	 * Gets a given {@link Keyword} by name or abbreviation.
	 * 
	 * @param keyword
	 *            the name of or abbreviation for a {@link Keyword} as a {@link String}
	 * @return the {@link Keyword} if it exists, otherwise null
	 */
	@Deprecated
	public Keyword getKeyword(String keyword) {
		return keywords.get(keyword);
	}
	
	/**
	 * @return an unmodifiable {@link List} of the names of all the Commands, Keywords, and Operations in this {@link Parser}
	 */
	public Set<String> getAllNames() {
		return Collections.unmodifiableSet(tokenizeablePluggableRegistry.keySet());
	}
	
	/**
	 * @return a HashMap{@literal <String, String>} of all of the abbreviations for Commands, Keywords, and Operations in
	 *         this {@link Parser}
	 */
	public HashMap<String, String> getAbbreviations() {
		return new HashMap<>(abbreviations);
	}
	
	/**
	 * Gets a given {@link Operator} by name.
	 * 
	 * @param operator
	 *            the name of an {@link Operator} as a {@link String}
	 * @return the {@link Operator} if it exists, otherwise null
	 */
	@Deprecated
	public Operator getOperator(String operator) {
		Operator output = operators.get(operator);
		return output == null && abbreviations.containsKey(operator) ? operators.get(abbreviations.get(operator)) : output;
	}
	
	/**
	 * Gets a given {@link AngleType} by name.
	 * 
	 * @param angleType
	 *            the name of an {@link AngleType} as a {@link String}
	 * @return the {@link Operator} if it exists, otherwise null
	 */
	@Deprecated
	public AngleType getAngleType(String angleType) {
		return angleTypes.get(angleType);
	}
	
	/**
	 * @param check
	 *            the <tt>String</tt> to check
	 * @return true if <tt>check</tt> is the name of an operation, otherwise false
	 */
	public boolean isOperation(String check) {
		return operations.containsKey(check);
	}
	
	/**
	 * Gets the last called plugin. This is used for error handling when the thrower is null.
	 * 
	 * @return the lastPlugin
	 */
	public ParserPlugin getLastPlugin() {
		return lastPlugin.wrapped;
	}
	
	/**
	 * Returns the {@link ParserPlugin} with the given ID or null if no such plugin is loaded.
	 * 
	 * @param id
	 *            the ID of the plugin to retrieve
	 * @return the {@link ParserPlugin} with the given ID or null if no such plugin is loaded
	 */
	public ParserPlugin getPlugin(String id) {
		return plugins.get(id);
	}
	
	public List<ParserPlugin> getSettingsPlugins() {
		return Collections.unmodifiableList(settingsPlugins);
	}
	
	/**
	 * @return the {@link AngleType} that this {@link Parser} is currently using
	 */
	@Deprecated
	public AngleType getActiveAngleType() {
		return activeAngleType.get();
	}
	
	/**
	 * @param angleType
	 *            the new {@link AngleType} for this parser to use
	 * @return the AngleType that was previously in use
	 */
	@Deprecated
	public AngleType setActiveAngleType(AngleType angleType) {
		AngleType old = this.activeAngleType.get();
		if (!angleTypes.containsKey(angleType.getName()))
			try {
				angleTypes.add(angleType);
			}
			catch (PluginConflictException e) {
				logger.log(Level.WARNING, e.getMessage());
				e.printStackTrace();
			}
		this.activeAngleType.set(angleType);
		return old;
	}
	
	/**
	 * Adds an {@link AngleType} to this {@link Parser}.
	 * 
	 * @param angleType
	 *            the {@link AngleType} to add
	 * @return the {@link AngleType} that this overrode, if it did, otherwise null
	 */
	@Deprecated
	public AngleType addAngleType(AngleType angleType) {
		return angleTypes.put(angleType.getName(), angleType);
	}
	
	/**
	 * Removes an {@link AngleType} from this {@link Parser} by name.
	 * 
	 * @param angleType
	 *            the name of the {@link AngleType} to remove
	 * @return the {@link AngleType} that was removed, otherwise null
	 */
	@Deprecated
	public AngleType removeAngleType(String angleType) {
		return angleTypes.remove(angleType);
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	//Parsing stuff goes here
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Calls the preprocessing filters added by plugins on the given input.<br>
	 * Provided that all of the filters are implemented correctly, this does not modify the input {@link ConsCell} tree.
	 * 
	 * @param input
	 *            the {@link ConsCell} tree to scrub
	 * @return a scrubbed copy of the given {@link ConsCell} tree
	 * @throws ParserException
	 *             to allow exceptions to propagate back to the calling function
	 */
	public ConsCell preProcess(ConsCell input) throws ParserException {
		if (!input.checkLength(3))
			return condenseOperators(input);
		ConsCell output = new ConsCell(), head = output;
		for (Iterator<ConsCell> i = input.iterator(), j = input.getNextConsCell().iterator(); i.hasNext();) {
			ConsCell current = i.next(), next = j.hasNext() ? j.next() : new ConsCell();
			if (current.getCar() instanceof ConsCell) {
				current = new ConsCell(preProcess((ConsCell) current.getCar()), current.getCarType());
				if (cas.canDropParentheses(head, current, next))
					current = (ConsCell) current.getCar();
			}
			if (head.getCarType() == OPERATOR && current.getCarType() == OPERATOR) {
				if (head.getCar().equals("^")) {
					StringBuilder chain = new StringBuilder();
					Operator first = operators.get("+");
					do {
						Operator op = (Operator) current.getCar();
						chain.append(op.getName());
						if (first.equals("+") && op.equals("+")) {/*No need to change first*/}
						else if (first.equals("+") && op.equals("-"))
							first = op;
						else if (first.equals("-") && op.equals("+")) {/*No need to change first*/}
						else if (first.equals("-") && op.equals("-"))
							first = operators.get("-");
						else
							throw new SyntaxException("Cannot have an ^ followed by " + chain.toString() + ".", getLastPlugin());
						if (!i.hasNext())
							throw new SyntaxException("Cannot have an ^ followed by " + chain.toString() + ".", getLastPlugin());
						current = i.next();
						if (j.hasNext())
							next = j.next();
					} while (current.getCarType() == OPERATOR);
					if (first.equals("+")) {/*Nothing to do here - current already equals the token after the operator chain*/}
					else if (first.equals("-"))
						current = new ConsCell(new ConsCell(first, OPERATOR, current, TOKEN), PARENTHESES); //The exponent is more than one token long, so we wrap it.
					else
						throw new SyntaxException("Cannot have an ^ followed by " + chain.toString() + ".", getLastPlugin());
				}
				else {
					if (head.getCar().equals("+") && current.getCar().equals("+"))
						current = current.getNextConsCell();
					else if (head.getCar().equals("+") && current.getCar().equals("-")) {
						head.replaceCar(current.getCar(), OPERATOR);
						current = current.getNextConsCell();
					}
					else if (head.getCar().equals("-") && current.getCar().equals("+"))
						current = current.getNextConsCell();
					else if (head.getCar().equals("-") && current.getCar().equals("-")) {
						head.replaceCar(operators.get("+"), OPERATOR);
						current = current.getNextConsCell();
					}
				}
			}
			if (!current.isNull())
				head = head.append(current);
		}
		for (Filter scrubber : preprocessingFilters.values())
			output = scrubber.evaluate(output);
		return output;
	}
	
	/**
	 * Calls the postprocessing filters added by plugins on the given input.<br>
	 * Provided that all of the filters are implemented correctly, this does not modify the input {@link ConsCell} tree.
	 * 
	 * @param input
	 *            the {@link ConsCell} tree to scrub
	 * @return a scrubbed copy of the given {@link ConsCell} tree
	 * @throws ParserException
	 *             to allow exceptions to propagate back to the calling function
	 */
	public ConsCell postProcess(ConsCell input) throws ParserException {
		for (Filter scrubber : postprocessingFilters.values())
			input = scrubber.evaluate(input);
		return input;
	}
	
	@Override
	public ConsCell evaluate(ConsCell input) throws ParserException {
		List<String> initVars = new ArrayList<>(vars);
		if (!input.containsType(CAS))
			input = evaluateInner(input);
		else
			input = cas.solve(input);
		vars.clear();
		vars.addAll(initVars); //TODO speed this up.  Possibly use a kernel object?
		return input;
	}
	
	private ConsCell evaluateInner(ConsCell input) throws ParserException {
		if (input.getCarType() == COMMAND)
			return ((Command) input.getCar()).evaluate(input.getNextConsCell());
		input = seekActions(input);
		input = processEquation(input);
		return input;
	}
	
	//Finds and evaluates all Operations and Keywords in the input
	private ConsCell seekActions(ConsCell input) throws ParserException {
		ConsCell output = new ConsCell(), head = output;
		//Each case in this loop needs to end such that current is set to the last used / inspected token.
		for (ConsCell current = input; !current.isNull(); current = current.getNextConsCell()) {
			if (current.getCarType() == OPERATION) {
				Operation operation = (Operation) current.getCar();
				ConsCell argument = new ConsCell(), arg = argument;
				current = current.getNextConsCell();
				//First run of this loop checks for things like leading minus signs
				while (current.getCarType() == OPERATION || current.getCarType() == OPERATOR) {
					arg = arg.append(current.singular());
					current = current.getNextConsCell();
				}
				arg = arg.append(current.singular());
				head = head.append(operation.evaluate(argument.getCarType() == PARENTHESES ? (ConsCell) argument.getCar() : argument));
			}
			else if (current.getCarType() == KEYWORD)
				head = head.append(((Keyword) current.getCar()).evaluate(null));
			else
				//If no special processing was required, check for descenders and append the token.
				head = head.append(checkDescenders(current, this::evaluateInner));
		}
		return output;
	}
	
	private ConsCell processEquation(ConsCell input) throws ParserException {
		if (!input.checkLength(2)) //If the input has a length of 0 or 1, return a copy of it
			return input.singular();
		Stack<ConsCell> previous = new Stack<>(), next = new Stack<>();
		Stack<Operator> left = new Stack<>(), right = new Stack<>();
		boolean fromLeft = true, leftOverride = false, rightOverride = false;
		ConsCell head = input;
		//If the input starts with an operator, push the operator and its default left-hand input into the left and previous stacks respectively and then step the head forward by one
		if (head.getCarType() == OPERATOR) {
			left.push((Operator) head.getCar());
			previous.push(((Operator) head.getCar()).getLeftInitial());
			head = head.getNextConsCell();
		}
		else {
			left.push(noOp);
			previous.push(new ConsCell());
		}
		if (!input.checkLength(3)) {
			right.push(noOp);
			next.push(new ConsCell());
		}
		else {
			right.push((Operator) head.getNextConsCell().getCar());
			next.push(head.getNextConsCell().getNextConsCell());
		}
		//TODO reduce duplicate if statements
		for (;;) {
			Operator leftOp = left.peek(), rightOp = right.peek();
			int leftPrecedence = leftOp.getPrecedence(), rightPrecedence = rightOp.getPrecedence();
			boolean leftPrecedent =
					leftOp != noOp && (rightOp == noOp || leftPrecedence > rightPrecedence || (leftPrecedence == rightPrecedence && (!leftOp.isRightAssociative() && !leftOp.isLocked())));
			rightOverride = (rightOverride || (rightOp != noOp && leftOp.isLocked())) && leftPrecedent && fromLeft;
			leftOverride = (leftOverride || (leftOp != noOp && rightOp.isLocked())) && !leftPrecedent && !fromLeft;
			if (!leftOp.isLocked() && fromLeft && leftPrecedent) { //<previous> <left> <current>
				ConsCell p = previous.pop();
				Operator l = left.pop(); //We're about to process <previous> and <left>, so we remove them from the queue
				if (previous.size() == 0) {
					Operator op = (Operator) p.getPreviousConsCell().getCar();
					left.push(op == null ? noOp : op);
					previous.push(p.getPreviousConsCell().getPreviousConsCell());
				}
				if (!leftOp.isApplicable(p, head)) {
					left.push(l.lock());
					previous.push(p);
				}
				else {
					head = leftOp.evaluate(p, head);
					if (rightOp.isLocked())
						right.push(right.pop().unlock());
					if (left.peek().isLocked())
						left.push(left.pop().unlock());
				}
			}
			else if (!rightOp.isLocked() && !fromLeft && !leftPrecedent) { //<current> <right> <next>
				ConsCell n = next.pop(); //We're about to process <right> and <next>, so we remove them from the queue
				Operator r = right.pop();
				if (next.size() == 0) {
					Operator op = (Operator) n.getNextConsCell().getCar();
					right.push(op == null ? noOp : op);
					next.push(n.getNextConsCell().getNextConsCell());
				}
				if (!rightOp.isApplicable(head, n)) {
					right.push(r.lock());
					next.push(n);
				}
				else {
					head = rightOp.evaluate(head, n);
					if (leftOp.isLocked())
						left.push(left.pop().unlock());
					if (right.peek().isLocked())
						right.push(right.pop().unlock());
				}
			}
			else if (leftOverride) {
				if (left.peek() == noOp)
					break;
				head = moveLeft(previous, left, head, right, next);
				fromLeft = false; //Moving from right -> left
				if (right.peek() == noOp)
					break;
				right.push(right.pop().lock());
			}
			else if (rightOverride) {
				if (right.peek() == noOp)
					break;
				head = moveRight(previous, left, head, right, next);
				fromLeft = true; //Moving from left -> right
				if (left.peek() == noOp)
					break;
				left.push(left.pop().lock());
			}
			else if (!rightOp.isLocked() && !leftPrecedent) {
				if (right.peek() == noOp)
					break;
				head = moveRight(previous, left, head, right, next);
				fromLeft = true; //Moving from left -> right
			}
			else if (!leftOp.isLocked() && leftPrecedent) {
				if (left.peek() == noOp)
					break;
				head = moveLeft(previous, left, head, right, next);
				fromLeft = false; //Moving from right -> left
			}
			else
				break;
		}
		ConsCell output = new ConsCell(), out = output;
		while (left.size() > 1)
			output = new ConsCell(previous.peek().getCar(), previous.pop().getCarType(), new ConsCell(left.pop().unlock(), OPERATOR, output, TOKEN));
		out = out.append(head.singular());
		while (right.size() > 1)
			out = out.append(new ConsCell(right.pop().unlock(), OPERATOR)).append(new ConsCell(next.peek().getCar(), next.pop().getCarType()));
		return output;
	}
	
	private ConsCell moveRight(Stack<ConsCell> previous, Stack<Operator> left, ConsCell head, Stack<Operator> right, Stack<ConsCell> next) {
		if (right.size() == 1) { //The size can never equal zero, so 1 is the minimum
			left.push(right.pop());
			ConsCell temp = next.pop();
			previous.push(head);
			Operator op = (Operator) temp.getNextConsCell().getCar();
			right.push(op == null ? noOp : op);
			next.push(temp.getNextConsCell().getNextConsCell());
			return temp;
		}
		else { //Move the head right
			previous.push(head);
			left.push(right.pop());
			return next.pop();
		}
	}
	
	private ConsCell moveLeft(Stack<ConsCell> previous, Stack<Operator> left, ConsCell head, Stack<Operator> right, Stack<ConsCell> next) {
		if (left.size() == 1) { //The size can never equal zero, so 1 is the minimum
			right.push(left.pop());
			ConsCell temp = previous.pop();
			Operator op = (Operator) temp.getPreviousConsCell().getCar();
			left.push(op == null ? noOp : op);
			next.push(head);
			previous.push(temp.getPreviousConsCell().getPreviousConsCell());
			return temp; //New head
		}
		else { //Move the head left
			next.push(head);
			right.push(left.pop());
			return previous.pop(); //New head
		}
	}
	
	/**
	 * Determines if the given {@link ConsCell} represents a descent to another level of the token tree. If it does, it
	 * applies the provided {@link ScrubbingFunction} to that level and encapsulates the result in a token of the appropriate
	 * descent type before returning it. Otherwise, it returns a new {@link ConsCell} containing a pointer to just the given
	 * {@link ConsCell}'s car and carType (see {@link ConsCell#singular()}
	 * 
	 * @param token
	 *            the {@link ConsCell} to check and scrub as necessary
	 * @param scrubber
	 *            the function with which to scrub the {@link ConsCell}
	 * @return scrubs the token if needed and then returns an appropriately wrapped result or shallow copy of the
	 *         {@link ConsCell} for appending to the output chain of the calling {@link ScrubbingFunction}
	 * @throws ParserException
	 *             to allow exceptions to propagate back to the calling function
	 * @see ConsCell#singular()
	 */
	public ConsCell checkDescenders(ConsCell token, ScrubbingFunction scrubber) throws ParserException {
		if (token.getCar() instanceof ConsCell && token.getCarType() != RETURN) {
			//TODO fix removeExcessParentheses
			ConsCell res = new ConsCell(scrubber.scrub((ConsCell) token.getCar()), token.getCarType());
			return !((ConsCell) res.getCar()).hasNextConsCell() ? (ConsCell) res.getCar() : res;
		}
		return token.singular();
	}
	
	/**
	 * Condenses ++/+-/-+/-- operator pairs into +/-/-/+.
	 * 
	 * @param input
	 *            the {@link ConsCell} tree to process
	 * @return the condensed {@link ConsCell} tree
	 */
	public ConsCell condenseOperators(ConsCell input) {
		ConsCell output = input.getCarType() == PARENTHESES ? new ConsCell(condenseOperators((ConsCell) input.getCar()), PARENTHESES) : input.singular(), head = output;
		for (Iterator<ConsCell> i = input.getNextConsCell().iterator(); i.hasNext();) {
			ConsCell current = i.next();
			if (head.getCarType() != OPERATOR || current.getCarType() != OPERATOR)
				head = head.append(current.getCarType() == PARENTHESES ? new ConsCell(condenseOperators((ConsCell) current.getCar()), PARENTHESES) : current);
			else if (head.getCar().equals("+") && current.getCar().equals("+"))
				continue;
			else if (head.getCar().equals("+") && current.getCar().equals("-"))
				head.replaceCar(current.getCar(), OPERATOR);
			else if (head.getCar().equals("-") && current.getCar().equals("+"))
				continue;
			else if (head.getCar().equals("-") && current.getCar().equals("-"))
				head.replaceCar(operators.get("+"), OPERATOR);
			else
				head = head.append(current);
		}
		return output;
	}
	
	/**
	 * Seeks for duplicate operations in the {@link ConsCell} tree and fixes them.
	 * 
	 * @param input
	 *            the {@link ConsCell} tree to fix
	 * @return the fixed {@link ConsCell} tree
	 */
	public ConsCell fixOperatorPairs(ConsCell input) {
		ConsCell output = input.singular(), head = output;
		for (ConsCell next = input.getNextConsCell(); !head.isNull() && !next.isNull(); head = head.append(next.singular()), next = next.getNextConsCell()) {
			if (head.getCarType() == PARENTHESES)
				head.replaceCar(fixOperatorPairs((ConsCell) head.getCar()), PARENTHESES);
			if (head.getCarType() == OPERATOR && next.getCarType() == OPERATOR) {
				Operator op1 = (Operator) head.getCar(), op2 = (Operator) next.getCar();
				if (op1 == op2 && (op1.equals("+") || op1.equals("-")))
					head.replaceCar(operators.get("+"), OPERATOR);
				else if (op1.equals("+") && op2.equals("-") || op1.equals("-") && op2.equals("+"))
					head.replaceCar(operators.get("-"), OPERATOR);
				else
					continue;
				next = next.getNextConsCell(); //Skip the next token so that it isn't appended to the output
			}
		}
		if (head.getCarType() == PARENTHESES) //Recursion - just in case it ends with parentheses
			head.replaceCar(fixOperatorPairs((ConsCell) head.getCar()), PARENTHESES);
		return output;
	}
	
	/**
	 * Converts the given input String into a {@link ConsCell} tree
	 * 
	 * @param input
	 *            the String to convert
	 * @return a {@link ConsCell} tree representing the given String
	 * @throws SyntaxException
	 *             if the String contains syntax errors
	 */
	public ConsCell tokenize(String input) throws SyntaxException {
		try {
			return tokenizer.lex(input);
		}
		catch (Exception e) {
			SyntaxException ex = new SyntaxException(e.getMessage(), getLastPlugin());
			ex.setStackTrace(e.getStackTrace());
			throw ex;
		}
	}
	
	/**
	 * @return the Computer Algebra System associated with this {@link Parser}
	 */
	@Deprecated
	public CAS getCAS() {
		return cas;
	}
	
	/**
	 * Convenience method for {@link #getEndIndex(String, int, char, char)} that passes parentheses as the open and close
	 * symbols
	 * 
	 * @param input
	 *            the {@link String} containing the parentheses to be checked
	 * @param start
	 *            index of the opening parenthesis to be checked
	 * @return index of the closing parenthesis
	 * @throws UnbalancedDescendersException
	 *             if a descender token is found that does not have a corresponding close token
	 * @see #getEndIndex(String, int, char, char)
	 */
	public static int getEndIndex(String input, int start) throws UnbalancedDescendersException {
		return getEndIndex(input, start, '(', ')');
	}
	
	/**
	 * Finds and returns the index of the close symbol that closes the open symbol at the start index in the given input
	 * {@link String}.<br>
	 * This method should be used for descent tokens that are one character long (it is significantly faster than the
	 * {@link String} version)
	 * 
	 * @param input
	 *            the {@link String} containing the descender to be checked
	 * @param start
	 *            index of the opening open symbol
	 * @param open
	 *            the symbol that opens the block
	 * @param close
	 *            the symbol that closes the block
	 * @return index of the close symbol that closes the block opened at start
	 * @throws UnbalancedDescendersException
	 *             if a descender token is found that does not have a corresponding close token
	 * @see #getEndIndex(String, int, String, String)
	 */
	public static int getEndIndex(String input, int start, char open, char close) throws UnbalancedDescendersException {
		for (int i = start, level = 0; i < input.length(); i++) {
			if (input.charAt(i) == open)
				level++;
			if (input.charAt(i) == close)
				level--;
			if (level == 0)
				return i;
		}
		throw new UnbalancedDescendersException(start, input, null);
	}
	
	/**
	 * Finds and returns the index of the close symbol that closes the open symbol at the start index in the given input
	 * {@link String}.<br>
	 * NOTE: if both open and close have a length of one, {@link #getEndIndex(String, int, char, char)} should be used.
	 * 
	 * @param input
	 *            the {@link String} containing the descender to be checked
	 * @param start
	 *            index of the opening open symbol
	 * @param open
	 *            the symbol that opens the block
	 * @param close
	 *            the symbol that closes the block
	 * @return index of the close symbol that closes the block opened at start
	 * @throws UnbalancedDescendersException
	 *             if a descender token is found that does not have a corresponding close token
	 */
	public static int getEndIndex(String input, int start, String open, String close) throws UnbalancedDescendersException {
		//If open and close both have a length of one, we can forward to the faster char-base method
		if (open.length() == 1 && close.length() == 1)
			return getEndIndex(input, start, open.charAt(0), close.charAt(0));
			
		//Calculate the iteration limit based on the length of the longer of the two symbols
		int limit = input.length() - (open.length() >= close.length() ? open.length() : close.length()) + 1;
		for (int i = start, level = 0; i < limit; i++) {
			if (input.startsWith(open, i))
				level++;
			if (input.startsWith(close, i))
				level--;
			if (level == 0)
				return i;
		}
		throw new UnbalancedDescendersException(start, input, null);
	}
	
	/**
	 * Determines the variables in the given {@link ConsCell}.<br>
	 * This blindly treats all {@link ConsType#IDENTIFIER} as variables; a more fine-tuned approach is generally preferable.
	 * 
	 * @param cell
	 *            the {@link ConsCell} to process
	 * @return the variables in <tt>cell</tt>
	 */
	public static Set<String> getVariables(ConsCell cell) {
		Set<String> out = new LinkedHashSet<>();
		for (; !cell.isNull(); cell = cell.getNextConsCell())
			if (cell.getCarType() == IDENTIFIER)
				out.add((String) cell.getCar());
			else if (cell.getCar() instanceof ConsCell)
				out.addAll(getVariables((ConsCell) cell.getCar()));
		return out;
	}
	
	/**
	 * Determines if the given {@link ConsCell} tree contains an occurrence of any of the given variables.
	 * 
	 * @param token
	 *            the {@link ConsCell} tree to check
	 * @param vars
	 *            the variables to check for
	 * @return true if there is at least one instance of at least one of the given variables in the given {@link ConsCell}
	 *         tree
	 */
	public boolean containsVariables(ConsCell token, String... vars) {
		for (; !token.isNull(); token = token.getNextConsCell()) {
			if (token.getCarType() == IDENTIFIER) {
				String car = (String) token.getCar();
				for (String var : vars)
					if (car.equals(var))
						return true;
			}
			if (token.getCar() instanceof ConsCell && containsVariables((ConsCell) token.getCar(), vars))
				return true;
		}
		return false;
	}
	
	/**
	 * Determines if the given {@link ConsCell} tree contains an occurrence of any of the given variables.<br>
	 * Forwards to: {@link #containsVariables(ConsCell, String...)}
	 * 
	 * @param token
	 *            the {@link ConsCell} tree to check
	 * @param vars
	 *            the variables to check for
	 * @return true if there is at least one instance of at least one of the given variables in the given {@link ConsCell}
	 *         tree
	 * @see #containsVariables(ConsCell, String...)
	 */
	public boolean containsVariables(ConsCell token, Collection<String> vars) {
		return containsVariables(token, vars.toArray(new String[vars.size()]));
	}
}
