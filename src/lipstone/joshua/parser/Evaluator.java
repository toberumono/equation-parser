package lipstone.joshua.parser;

import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.tokenizer.ConsCell;

/**
 * Represents a function that is used to evaluate a {@link lipstone.joshua.parser.tokenizer.ConsCell Token} tree
 * 
 * @author Joshua Lipstone
 */
@FunctionalInterface
public interface Evaluator {
	
	/**
	 * Evaluates the given {@link lipstone.joshua.parser.tokenizer.ConsCell Token} tree without modifying the original.
	 * 
	 * @param input
	 *            the {@link lipstone.joshua.parser.tokenizer.ConsCell Token} tree to evaluate
	 * @return an evaluated copy of the given {@link lipstone.joshua.parser.tokenizer.ConsCell Token} tree
	 * @throws ParserException
	 *             to allow exceptions to propagate back to the calling function
	 */
	public ConsCell evaluate(ConsCell input) throws ParserException;
}
