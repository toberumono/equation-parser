package lipstone.joshua.parser;

/**
 * A simple functional interface that is designed to take a {@link Parser} as an argument and return another function
 * represented by the Functional Interface type <b>{@literal <T>}</b>
 * 
 * @author Joshua Lipstone
 * @param <T>
 *            the Functional Interface type to return.
 */
@FunctionalInterface
public interface ParserCurrier<T> {
	
	public T curry(Parser parser);
}
