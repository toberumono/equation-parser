package lipstone.joshua.parser.exceptions;

import lipstone.joshua.parser.ParserPlugin;

/**
 * Thrown when an operation does not exist or is undefined for the given operands.
 * 
 * @author Joshua Lipstone
 */
public class UndefinedOperationException extends ParserException {
	
	public UndefinedOperationException(String message, ParserPlugin thrower) {
		super(message, thrower);
	}
	
}
