package lipstone.joshua.parser.exceptions;

import toberumono.json.JSONObject;
import toberumono.json.JSONRepresentable;
import toberumono.json.JSONSystem;

import lipstone.joshua.parser.Parser;
import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.pluginLoader.PluginException;

/**
 * The root class for all exceptions thrown by this library
 * 
 * @author Joshua Lipstone
 */
public class ParserException extends PluginException implements Cloneable, JSONRepresentable {
	static final long serialVersionUID = 2L;
	
	/**
	 * @param thrower
	 *            the plugin that threw this exception
	 */
	public ParserException(ParserPlugin thrower) {
		super(thrower);
	}
	
	/**
	 * @param message
	 *            a message describing the error and/or its cause
	 * @param thrower
	 *            the plugin that threw this exception
	 */
	public ParserException(String message, ParserPlugin thrower) {
		super(message, thrower);
	}
	
	/**
	 * Uses the message and throwerID data in the given {@link JSONObject} to construct a {@link ParserException}
	 * 
	 * @param base
	 *            a {@link JSONObject} containing 'Message' and 'ThrowerID'
	 * @param parser
	 *            Used to reconstruct the thrower from the ThrowerID
	 */
	public ParserException(JSONObject base, Parser parser) {
		this((String) base.get("Message").value(), base.get("ThrowerID").value() == null ? null : parser.getPlugin((String) base.get("ThrowerID").value()));
	}
	
	/**
	 * @return the plugin that threw this error
	 */
	@Override
	public ParserPlugin getThrower() {
		return (ParserPlugin) super.getThrower();
	}
	
	@Override
	public ParserException clone() {
		ParserException clone = new ParserException(getMessage(), getThrower());
		clone.setStackTrace(this.getStackTrace());
		return clone;
	}
	
	/**
	 * Stores the message and throwerID in a {@link JSONObject} and returns it.
	 * 
	 * @return a {@link JSONObject} that represents this {@link ParserException}.
	 */
	@Override
	public JSONObject toJSONObject() {
		JSONObject output = new JSONObject();
		output.put("Message", JSONSystem.wrap(getMessage()));
		output.put("ThrowerID", JSONSystem.wrap(getThrower() == null ? getThrower() : getThrower().getID()));
		return output;
	}
}
