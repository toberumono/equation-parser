package lipstone.joshua.parser.exceptions;

import lipstone.joshua.parser.ParserPlugin;

/**
 * Thrown when a descender token is found does not have a matching close token.
 * 
 * @author Joshua Lipstone
 */
public class UnbalancedDescendersException extends SyntaxException {
	private final int index;
	private final String equation;
	
	/**
	 * Construct a new {@link UnbalancedDescendersException} caused by a descender at index, with a custom message and the
	 * plugin that threw the exception
	 * 
	 * @param index
	 *            the index of the descender that caused this error
	 * @param message
	 *            a message describing the error and/or its cause
	 * @param equation
	 *            the equation that contains the unbalanaced descender
	 * @param thrower
	 *            the plugin that threw this exception
	 */
	public UnbalancedDescendersException(int index, String message, String equation, ParserPlugin thrower) {
		super(message, thrower);
		this.index = index;
		this.equation = equation;
	}
	
	/**
	 * Convenience constructor - uses an default message
	 * 
	 * @param index
	 *            the index of the descender that caused this error
	 * @param equation
	 *            the equation that contains the unbalanaced descender
	 * @param thrower
	 *            the plugin that threw this exception
	 * @see #UnbalancedDescendersException(int, String, String, ParserPlugin)
	 */
	public UnbalancedDescendersException(int index, String equation, ParserPlugin thrower) {
		super(equation + " contains unbalanced descender at index " + (index + 1) + ".", thrower);
		this.index = index;
		this.equation = equation;
	}
	
	/**
	 * Get the index of the descender that caused this error
	 * 
	 * @return the index of the descender that caused the error
	 */
	public int getIndex() {
		return index;
	}
	
	/**
	 * Get the equation containing the descender that caused this error
	 * 
	 * @return the equation containing the descender that caused the error
	 */
	public String getEquation() {
		return equation;
	}
}
