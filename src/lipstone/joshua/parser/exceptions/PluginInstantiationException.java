package lipstone.joshua.parser.exceptions;

import lipstone.joshua.parser.ParserPlugin;

/**
 * Thrown when a plugin does not have a valid 0-argument constructor.
 * 
 * @author Joshua Lipstone
 */
public class PluginInstantiationException extends ParserException {
	
	/**
	 * @param thrower
	 *            the plugin that threw this exception
	 */
	public PluginInstantiationException(ParserPlugin thrower) {
		super(thrower);
	}
	
	/**
	 * @param message
	 *            a message describing this error
	 * @param thrower
	 *            the plugin that threw this exception
	 */
	public PluginInstantiationException(String message, ParserPlugin thrower) {
		super(message, thrower);
	}
	
}
