package lipstone.joshua.parser.exceptions;

import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.types.Matrix;

/**
 * Thrown when a function attempts to access an invalid cell in a {@link lipstone.joshua.parser.types.Matrix Matrix}.
 * 
 * @author Joshua Lipstone
 */
public class MatrixCoordinateOutOfBoundsException extends ParserException {
	
	public MatrixCoordinateOutOfBoundsException(ParserPlugin thrower) {
		super("The requested coordinates were out of bounds.", thrower);
	}
	
	public MatrixCoordinateOutOfBoundsException(String message, ParserPlugin thrower) {
		super(message, thrower);
	}
	
	public MatrixCoordinateOutOfBoundsException(int row, int col, Matrix m, ParserPlugin thrower) {
		super("(" + row + ", " + col + ") is not in the matrix.  The maximum bound for the matrix is (" + m.getRows() + ", " + m.getCols() + ").", thrower);
	}
	
}
