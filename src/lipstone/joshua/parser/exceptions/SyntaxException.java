package lipstone.joshua.parser.exceptions;

import lipstone.joshua.parser.ParserPlugin;

/**
 * Thrown when a parsed input is syntactically incorrect.
 * 
 * @author Joshua Lipstone
 */
public class SyntaxException extends ParserException {
	
	/**
	 * Constructs a new SyntaxException
	 * 
	 * @param message
	 *            The input being tokenized at the time of the error
	 * @param thrower
	 *            The last called plugin
	 */
	public SyntaxException(String message, ParserPlugin thrower) {
		super(message, thrower);
	}
}
