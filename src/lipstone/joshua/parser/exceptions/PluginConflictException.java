package lipstone.joshua.parser.exceptions;

import lipstone.joshua.parser.ParserPlugin;

/**
 * Thrown when a {@link lipstone.joshua.parser.ParserPlugin plugin} attempts to load data into its paired
 * {@link lipstone.joshua.parser.Parser Parser} that conflicts with another {@link lipstone.joshua.parser.ParserPlugin 
 * plugin's} data.
 * 
 * @author Joshua Lipstone
 */
public class PluginConflictException extends ParserException {
	
	/**
	 * @param thrower
	 *            the plugin that threw this exception
	 */
	public PluginConflictException(ParserPlugin thrower) {
		super(thrower);
	}
	
	/**
	 * @param message
	 *            a message describing this error
	 * @param thrower
	 *            the plugin that threw this exception
	 */
	public PluginConflictException(String message, ParserPlugin thrower) {
		super(message, thrower);
	}
	
}
