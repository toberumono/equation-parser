package lipstone.joshua.parser.exceptions;

import lipstone.joshua.parser.ParserPlugin;

/**
 * Thrown when a math operation has an undefined result
 * 
 * @author Joshua Lipstone
 */
public class UndefinedResultException extends ParserException {
	
	public UndefinedResultException(ParserPlugin thrower) {
		super("Undefined result.", thrower);
	}
	
	public UndefinedResultException(String message, ParserPlugin thrower) {
		super(message, thrower);
	}
	
}
