package lipstone.joshua.parser.exceptions;

import lipstone.joshua.parser.ParserPlugin;
import lipstone.joshua.parser.tokenizer.ConsCell;

/**
 * Thrown when a function attempts to extract a {@link lipstone.joshua.parser.types.Matrix} from an incorrectly formatted
 * input.
 * 
 * @author Joshua Lipstone
 */
public class MatrixSyntaxException extends SyntaxException {
	
	public MatrixSyntaxException(ConsCell matrixData, ParserPlugin thrower) {
		super("The syntax for an N x M matrix is as follows: {row1,row2,...rowN} where each row is an array of length M, and N and M are both greater than 0.\n" +
				matrixData.toString() + " does not follow that criteria.", thrower);
	}
	
}
