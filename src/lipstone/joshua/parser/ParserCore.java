package lipstone.joshua.parser;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.ForkJoinPool;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;

import lipstone.joshua.parser.exceptions.ParserException;
import lipstone.joshua.parser.plugin.ParserPermissions;
import lipstone.joshua.parser.tokenizer.ConsCell;
import lipstone.joshua.parser.tokenizer.ConsType;
import lipstone.joshua.parser.types.BigDec;
import lipstone.joshua.parser.util.Equation;
import lipstone.joshua.parser.util.History;
import lipstone.joshua.parser.util.WritableObservableValue;
import lipstone.joshua.pluginLoader.Plugin;
import lipstone.joshua.pluginLoader.PluginException;

/**
 * The main class for this library. Programs using this library should create an instance of this class and call the
 * {@link #parse(String)} method.
 * 
 * @author Joshua Lipstone
 */
public class ParserCore {
	
	private final Parser parser;
	private final History history;
	private final Logger logger;
	
	private PluginController pc;
	final InternalPlugin internalPlugin;
	
	private final Path baseLocation;
	
	/**
	 * Stores whether execution history will be stored across runs.<br>
	 * This defaults to <tt>true</tt>. If set to <tt>false</tt> the history.json file will be deleted when the program exits.
	 * <br>
	 * If this is set to <tt>true</tt> while {@link #dataPersistence} is <tt>false</tt>, {@link #dataPersistence} will be set
	 * to <tt>true</tt>.
	 */
	public final WritableObservableValue<Boolean> saveHistory = new WritableObservableValue<>(true);
	
	/**
	 * Stores whether data (history, logs, and the plugin data folder) will be stored across runs.<br>
	 * This defaults to <tt>true</tt>. If set to <tt>false</tt>, the plugin data folder, history.json file, and log.txt file
	 * will be deleted when the program exits.<br>
	 * If this is set to <tt>false</tt> while {@link #saveHistory} is <tt>true</tt>, {@link #saveHistory} will be set to
	 * <tt>false</tt>.
	 */
	public final WritableObservableValue<Boolean> dataPersistence = new WritableObservableValue<>(true);
	
	/**
	 * Use this for all threading operations.
	 */
	private static final ForkJoinPool fjPool = new ForkJoinPool();
	
	static {
		System.getProperties().setProperty("java.util.logging.SimpleFormatter.format", "%1$tc %2$s%n%4$s: %5$s%6$s%n");
	}
	
	public ParserCore() {
		this(FileSystems.getDefault().getPath(getPath()).getParent());
	}
	
	public ParserCore(Path baseLocation) {
		try {
			Logger.getLogger("toberumono.parser").addHandler(new StreamHandler(Files.newOutputStream(baseLocation.resolve("parser.log")), new SimpleFormatter()));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		this.logger = Logger.getLogger("toberumono.parser.ParserCore");
		this.baseLocation = baseLocation;
		if (!Files.exists(getBaseLocation()))
			try {
				Files.createDirectories(getBaseLocation());
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		setUpDirectories(getBaseLocation(), "data", "plugins");
		parser = new Parser(this, ParserPermissions.makeRootPermissions());
		String id = InternalPlugin.class.getAnnotation(Plugin.class).id();
		try {
			parser.plugins.add(InternalPlugin.class);
		}
		catch (PluginException e) {
			logger.log(Level.SEVERE, "Failed to load the internal plugin", e);
			e.printStackTrace();
		}
		finally {
			internalPlugin = (InternalPlugin) parser.getPlugin(id);
		}
		pc = new PluginController(parser);
		history = new History(baseLocation, parser);
		linkPersistenceFields();
	}
	
	private final void setUpDirectories(Path parent, String... directories) {
		for (String directory : directories) {
			Path file = parent.resolve(directory);
			if (!Files.isDirectory(file)) //Determine if the directory exists
				try {
					Files.createDirectories(file);
				}
				catch (IOException e) {
					logger.log(Level.SEVERE, "Could not create the " + directory + " directory in " + parent.toString(), e);
				}
		}
	}
	
	/**
	 * Links the history and data persistence fields and adds the shutdown hook that wipes the history file if
	 * {@link #saveHistory} is {@code false}; and the log file and the data folder if {@link #dataPersistence} is
	 * {@code false}.
	 */
	private final void linkPersistenceFields() {
		parser.addTrackableField(dataPersistence);
		parser.addTrackableField(saveHistory);
		dataPersistence.addListener((observable, oldValue, newValue) -> {
			if (!newValue)
				saveHistory.set(newValue);
		});
		saveHistory.addListener((observable, oldValue, newValue) -> {
			if (newValue)
				dataPersistence.set(newValue);
		});
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				if (!saveHistory.get())
					try {
						recursiveErase(history.getFileLocation());
					}
					catch (IOException e) {
						logger.log(Level.SEVERE, "Could not erase the history file.", e);
					}
				if (!dataPersistence.get()) {
					try {
						recursiveErase(getBaseLocation().resolve("data" + FileSystems.getDefault().getSeparator()));
					}
					catch (IOException e) {
						logger.log(Level.SEVERE, "Could not erase the data folder.", e);
					}
					try {
						recursiveErase(getBaseLocation().resolve("parser.log"));
					}
					catch (IOException e) {}
				}
			}
		});
	}
	
	/**
	 * Completely erases the given file from the computer. If the {@link java.nio.file.Path Path} points to a directory, this
	 * erases all of the files in the directory and then erases the directory itself.
	 * 
	 * @param file
	 *            the file or directory to erase
	 * @throws IOException
	 *             tosses the exception back to the calling function
	 */
	private static final void recursiveErase(Path file) throws IOException {
		if (!Files.exists(file))
			return;
		FileVisitor<Path> fv = new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}
			
			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
				Files.delete(file); //try to delete the file anyway, even if its attributes could not be read, since delete-only access is theoretically possible
				return FileVisitResult.CONTINUE;
			}
			
			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				if (exc == null) {
					Files.delete(dir);
					return FileVisitResult.CONTINUE;
				}
				else
					throw exc; //directory iteration failed; propagate exception
			}
		};
		Files.walkFileTree(file, fv);
	}
	
	private static final String getPath() {
		try {
			String path = ParserCore.class.getProtectionDomain().getCodeSource().getLocation().getPath(); //Gets the path to the .jar file containing this class
			String decodedPath = URLDecoder.decode(path, "UTF-8");
			return decodedPath;
		}
		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * The recommended starting method for all of the math capabilities of this program.<br>
	 * <b>Note:</b> only external calls should be made to this function. <b>No plugins should call this method</b>
	 * 
	 * @param input
	 *            the equation to be parsed, as a String
	 * @return the output from the parsed String. If there is an error, the String returned is a description of the error
	 */
	public Equation parse(String input) {
		ConsCell result = new ConsCell(), equation = result;
		Equation output = null;
		ParserException error = null;
		try {
			//TODO Possibly need to handle threading here
			equation = parser.preProcess(parser.tokenize(input));
			if (!equation.checkLength(1)) //Return 0 for empty inputs
				return new Equation(equation, new ConsCell(BigDec.ZERO, ConsType.NUMBER), null);
			result = parser.postProcess(parser.evaluate(equation));
			result = parser.cas.simplifyTokenTree(result);
			logger.log(Level.INFO, "Successfully parsed " + equation + " -> " + result);
		}
		catch (ParserException e) {
			error = e;
			if (e.getThrower() == null && parser.getLastPlugin() != null) { //If the plugin isn't specified in the error, set it to the last called plugin
				try {
					ParserException pe = e.getClass().getConstructor(String.class, ParserPlugin.class).newInstance(e.getMessage(), e.getThrower());
					pe.setStackTrace(e.getStackTrace());
					error = pe;
				}
				catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e1) {
					System.err.println("Could not append the last-called plugin data to the exception.");
				}
			}
			logger.log(Level.SEVERE, "Failed to parse: " + input, error);
		}
		finally {
			output = new Equation(equation, result, error);
			history.appendEquation(output);
		}
		return output;
	}
	
	/**
	 * @return the history management system associated with this {@link ParserCore}
	 */
	public History getHistory() {
		return history;
	}
	
	/**
	 * @return the {@link lipstone.joshua.parser.PluginController PluginController} associated with this {@link ParserCore}
	 */
	public PluginController getPluginController() {
		return pc;
	}
	
	/**
	 * @return the {@link Parser} associated with this {@link ParserCore}
	 */
	public Parser getParser() {
		return parser;
	}
	
	/**
	 * This is <i>not</i> the default plugin location. To get the default plugin location, use:
	 * <code>getBaseLocation() + "/plugins/"</code>
	 * 
	 * @return the location that this {@link ParserCore} is running from.
	 */
	public Path getBaseLocation() {
		return baseLocation;
	}
	
	/**
	 * @return the {@link java.util.concurrent.ForkJoinPool ForkJoinPool} used by this library for threaded operations
	 */
	public static ForkJoinPool getFJPool() {
		return fjPool;
	}
	
	/**
	 * @return whether this <tt>Parser</tt> is currently saving an input history and whether the history.xml file will be
	 *         saved (true) or deleted (false) when the program closes
	 * @see #setSaveHistory(boolean saveHistory)
	 */
	@Deprecated
	public final boolean isSavingHistory() {
		return saveHistory.get();
	}
	
	/**
	 * The saveHistory flag determines whether this <tt>Parser</tt> should record each input and whether the history.xml file
	 * should be saved (true) or deleted (false) when the program is closed. It defaults to true.
	 * 
	 * @param saveHistory
	 *            the new value for the saveHistory flag
	 * @see #isSavingHistory()
	 */
	@Deprecated
	public final void setSaveHistory(boolean saveHistory) {
		this.saveHistory.set(saveHistory);
	}
	
	/**
	 * @return whether any data (plugin, history, or logs) is going to persist across runs
	 * @see #setDataPersistence(boolean dataPersistence)
	 */
	@Deprecated
	public final boolean isDataPersisting() {
		return dataPersistence.get();
	}
	
	/**
	 * The dataPersistence flag determines whether all the data associated with this parser other than the plugins folder
	 * will be saved (true) or deleted (false) when the program is closed. It defaults to true.
	 * 
	 * @param dataPersistence
	 *            whether any data (plugin, history, or logs) should persist across runs
	 * @see #isDataPersisting()
	 */
	@Deprecated
	public final void setDataPersistence(boolean dataPersistence) {
		this.dataPersistence.set(dataPersistence);
	}
	
	@Override
	public String toString() {
		return "ParserCore";
	}
	
}
