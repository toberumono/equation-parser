#include <jni.h>
#include <stdlib.h>
#include "lipstone_joshua_parser_cas_CAS.h"

JNIEXPORT jlongArray JNICALL Java_lipstone_joshua_parser_cas_CAS_generatePrimes(JNIEnv *env, jclass jclass, jlong n) {
	jlong i = 2l, j = 0l, lenArr = 100l;
	long int * prime = (long int *) malloc(n * sizeof(long int));
	long int * primes = (long int *) malloc(lenArr * sizeof(long int));

	for (; i < n; i++) {
		if (!prime[i]) { //If this number has not been toggled by the sieve
			if (j == lenArr) {
				lenArr *= 2;
				primes = (long int *) realloc(primes, lenArr * sizeof(long int));
			}
			primes[j++] = i;
			for (jint k = i + i; k < n; k += i)
				prime[k] = 1;
		}
	}
	jlongArray result = env->NewLongArray(j);
	env->SetLongArrayRegion(result, 0, j, primes);
	//free(primes);
	return result;
}